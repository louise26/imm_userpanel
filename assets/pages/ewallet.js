
!function ($) {
    "use strict";

    var Ewallet = function () {


    };
    
    Ewallet.prototype.withdraw = function (url) {
      			
      				$('[name=withdrawalForm]').submit(function(){
      								  $('#btn_withdraw').attr('disabled',true);
                					 $('#btn_withdraw').html('<span class="fa fa-spin fa-spinner"> </span> Sending request .. ');
      						$.ajax({
									type: "POST",
									url: url + '/wallet/withdraw',
									data: 	{  
									                'imm_token'     :   $('#token').val(),
  													't_code' 		: 	$('[name=t_code]').val(),
  													'payment'		: 	$('[name=type]').val(),
  													'first_name'	: 	$('[name=first_name]').val(),
  													'last_name'		: 	$('[name=last_name]').val(),
  													'acc_name'		: 	$('[name=acc_name]').val(),
  													'ac_no' 		: 	$('[name=acc_no]').val(),
  													'bank_nm' 		: 	$('[name=bank_nm]').val(),
  													'branch_nm' 	: 	$('[name=branch_nm]').val(),
  													'swift_code' 	: 	$('[name=swift_code]').val(),
  													'bitcoin' 		: 	$('[name=bitcoin]').val(),
  													'ripple' 		: 	$('[name=ripple]').val(),
  													'ethereum' 		: 	$('[name=ethereum]').val(),
  													'bankwire' 		: 	$('[name=bankwire]').val(),
  													'amount' 		: 	$('[name=amount]').val(),
  													'service_charge': 	$('[name=trans_charge]').val(),
  													'net_amount' 	: 	$('[name=net_amount]').val(),
  													'password' 		: 	$('[name=password]').val(),
  													'description' 	: 	$('[name=description]').val(),
  													'balance' 		: 	$('[name=balance]').val()
											},
									cache: false,
									success: function(data){

												var obj = JSON.parse(data);
													//console.log(obj.title);
													$('#withdrawalModal').modal('hide');
												   swal(obj.title, obj.msg, obj.status);
												    $('#btn_withdraw').attr('disabled',false);
                            $('#btn_withdraw').html('<span>Submit</span')

                             						$('#direct-income').DataTable().destroy();

                             						$('#direct-income').DataTable({
										                    "ajax": url + '/wallet/withdraw-history',    
										                     dom: "Bfrtip",
											                    buttons: [{
											                        extend: "copy",
											                        className: "btn-success"
											                    }, {
											                        extend: "csv"
											                    }, {
											                        extend: "excel"
											                    }, {
											                        extend: "pdf"
											                    }, {
											                        extend: "print"
											                    }],
											                    "language": {
											                             "loadingRecords": "Please wait - retrieving data..."
											                          },
											                    responsive: !0
										                });

                             				$('[name=amount]').val('');
                             				$('[name=password]').val('');

									}
								});
      					event.preventDefault();
      				});
     					
      },
        Ewallet.prototype.transferFund = function (url) {
            
      
              
      },
      Ewallet.prototype.calc = function (){

      		var net  = 0,total=0;

            $('[name=amount]').focusout(function() {

                    net     =  parseFloat($(this).val() * (5/100));
                    total   =  parseFloat($(this).val() - (net))
                      
                    $('[name=trans_charge]').val(net.toFixed(2));
                    $('[name=net_amount]').val(total);          
              });
            
           
      },
      Ewallet.prototype.paymentOption = function(){


      			    $('#bank').hide();
                $('#eth').hide();
                $('#btc').hide();
                $('#xrp').hide();

                $('[name=payment]').click(function(){
                    if($(this).val()=='Bankwire'){
                             $('#bank').show();
                             $('[name=type]').val('Bankwire');
                             $('#eth').hide();
                             $('#btc').hide();
                             $('#xrp').hide();
                        }
                    else if($(this).val()=='Bitcoin') {
                             $('#bank').hide();
                             $('#eth').hide();
                             $('#btc').show();
                             $('#xrp').hide();
                             $('[name=type]').val('Bitcoin');
                    }
                    else if($(this).val()=='Ethereum') {
                             $('#bank').hide();
                             $('#eth').show();
                             $('#btc').hide();
                             $('#xrp').hide();
                             $('[name=type]').val('Ethereum');
                    }
                    else if($(this).val()=='Ripple') {
                             $('#bank').hide();
                             $('#eth').hide();
                             $('#btc').hide();
                             $('#xrp').show();
                             $('[name=type]').val('Ripple');
                    }
                    else {

                    }
                });
      },
      Ewallet.prototype.history = function (url) {

      			 $('#direct-income').DataTable({
                    "ajax": url + '/wallet/withdraw-history',    
                     dom: "Bfrtip",
	                    buttons: [{
	                        extend: "copy",
	                        className: "btn-success"
	                    }, {
	                        extend: "csv"
	                    }, {
	                        extend: "excel"
	                    }, {
	                        extend: "pdf"
	                    }, {
	                        extend: "print"
	                    }],
	                    "language": {
	                             "loadingRecords": "Please wait - retrieving data..."
	                          },
	                    responsive: !0
                });
      },
       
     Ewallet.prototype.init = function () {
      		  		
      		  		this.paymentOption();
      		  		this.calc(); 
      		  		this.history($('[name=url]').val());
      		  		this.withdraw($('[name=url]').val()); 	
                //this.transferFund($('[name=trans_url]').val()); 	  		
      }

    $.Ewallet = new Ewallet, $.Ewallet.Constructor = Ewallet

}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Ewallet.init();
    }(window.jQuery);

