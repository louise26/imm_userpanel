
!function ($) {
    "use strict";

    var Dash_board = function () {


    };
    //ewallet balance,rwallet balance,total referral,total_downline
    Dash_board.prototype.getDatas = function (url,user_id,imms_token) {
      		
     				$.ajax({
									type: "POST",
									url: url +'/userinfo',
									data: 	{ 
									                 imm_token : imms_token,
													user_id : user_id
											},
									cache: false,
								success: function(result){

												//console.log(result);
									}
								});	
      },

       //creates Bar chart
        Dash_board.prototype.createBarChart = function (element, data, xkey, ykeys, labels, lineColors) {
            Morris.Bar({
                element: element,
                data: data,
                xkey: xkey,
                ykeys: ykeys,
                labels: labels,
                gridLineColor: '#3d4956',
                barSizeRatio: 0.4,
                resize: true,
                hideHover: 'auto',
                barColors: lineColors
            });
        },
      //Total Paid and Unpaid Downlines
     Dash_board.prototype.getDownlinePayments = function (url,user_id,imms_token) {
      		
     				$.ajax({
									type: "POST",
									url: url +'/downlinepayments',
									data: 	{   
									    
									                  imm_token : imms_token,
													user_id : user_id
											},
									cache: false,
								success: function(result){
											var obj = JSON.parse(result);

													$('#paid').html('<b>'+obj.paid_downline+'</b>');
													$('#unpaid').html('<b>'+obj.unpaid_downline+'</b>');
												//console.log(result);
									}
								});
      },
      Dash_board.prototype.getEarnings = function (url,user_id,imms_token) {
      		                  
      		                  var ref = 0;
      		                  var l   = 0;
      		                  var p   = 0;
      		                  var r   = 0;
     				$.ajax({
									type: "POST",
									url: url +'/earnings',
									data: 	{ 
									                  imm_token : imms_token,
													user_id : user_id
											},
									cache: false,
								success: function(result){

											//console.log(result);
												var obj = JSON.parse(result);
											       console.log(obj);
											 
													 ref = (parseFloat(obj.referral).toFixed(2)) ;
													 l   = (parseFloat(obj.level).toFixed(2)); 
													 p   = (parseFloat(obj.profit).toFixed(2)) ;
													 r   = (parseFloat(obj.royalty).toFixed(2))  ;
											
												
												var $barData = [
											                {y: 'REFERRAL INCOME', a: ref},
											                {y: 'LEVEL INCOME', a: l},
											                {y: 'PROFIT SHARE  INCOME', a: p},
											                {y: 'ROYALTY INCOME', a: r},
											               
											            ];
											 Morris.Bar({
											                element: document.getElementById('morris-bar-example'),
											                data: $barData,
											                xkey: 'y',
											                ykeys: ['a'],
											                labels: ['Total'],
											                gridLineColor: '#3d4956',
											                barSizeRatio: 0.4,
											                resize: true,
											                hideHover: 'auto',
											                barColors: ['#04a2b3', '#00a3ff']
											            });
										     $('#msg2').hide();	            
											          
									}
								});
      },
      Dash_board.prototype.getTotalIncome = function (url,user_id,imms_token) {
      		
     				$.ajax({
									type: "POST",
									url: url +'/totalincome',
									data: 	{ 
									                  imm_token : imms_token,
													user_id : user_id
											},
									cache: false,
								success: function(result){
												var obj = JSON.parse(result);
												//var obj1 = JSON.parse(obj);
												//Object.keys(options)
												var tot = (parseFloat(obj.total_income[0].total).toFixed(2));
											//console.log(parseFloat(obj.total_income[0].total).toFixed(2));
											//console.log(tot);
											$('#total_income').html('<b>'+tot+'</b>');
									}
								});
      },
      

      Dash_board.prototype.init = function () {

      	$('#paid').html('<span class="fa fa-spin fa-spinner"></span>');
      	$('#unpaid').html('<span class="fa fa-spin fa-spinner"></span>');
      	$('#total_income').html('<span class="fa fa-spin fa-spinner"></span>');
   				
      		  this.getTotalIncome($('#base_url').val(),$('#user_id').val(),$('#token').val());

      		  this.getDownlinePayments($('#base_url').val(),$('#user_id').val(),$('#token').val());

      		  this.getEarnings($('#base_url').val(),$('#user_id').val(),$('#token').val());
      		   

      		  //this.getDatas($('#base_url').val(),$('#user_id').val());
      		   //creating bar chart
            var $barData = [
                {y: '2009', a: 100, b: 90},
                {y: '2010', a: 75, b: 65},
                {y: '2011', a: 50, b: 40},
                {y: '2012', a: 75, b: 65},
                {y: '2013', a: 50, b: 40},
                {y: '2014', a: 75, b: 65},
                {y: '2015', a: 100, b: 90},
                {y: '2016', a: 90, b: 75}
            ];
           // this.createBarChart(document.getElementById('morris-bar-example'), $barData, 'y', ['a', 'b'], ['Series A', 'Series B'], ['#04a2b3', '#00a3ff']);

      }
    $.Dash_board = new Dash_board, $.Dash_board.Constructor = Dash_board
}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Dash_board.init();
    }(window.jQuery);

