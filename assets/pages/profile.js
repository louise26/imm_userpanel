
!function ($) {
    "use strict";

    var Profile = function () {


    };
    
    Profile.prototype.getInfo = function (url,user_id,imms_token) {
      					
     				$.ajax({
									type: "POST",
									url: url +'/account/userinfo',
									data: 	{ 
									                imm_token : imms_token,
													user_id : user_id
											},
									cache: false,
								success: function(result){


												var obj = JSON.parse(result);
												//console.log(obj.user_info[0]);

												$('#fname').val(obj.user_info[0].first_name);
												$('#lname').val(obj.user_info[0].last_name);
												$('#email').val(obj.user_info[0].email);
												$('#passwd').val(obj.user_info[0].passwd);
												$('#oldpasswd').val(obj.user_info[0].passwd);
												$('#confirmpasswd').val(obj.user_info[0].passwd);
												$('#oldtranspasswd').val(obj.user_info[0].t_code);
												$('#transpasswd').val(obj.user_info[0].t_code);
												$('#ctranspasswd').val(obj.user_info[0].t_code);
												$('#address').val(obj.user_info[0].address);
												$('#country').val(obj.user_info[0].country);
												$('#state').val(obj.user_info[0].state);
												$('#city').val(obj.user_info[0].city);
												$('#zipcode').val(obj.user_info[0].zipcode);
												$('#countrycode').val(obj.user_info[0].zipcode);
												

												$('#contact').val(obj.user_info[0].telephone);
												$('#birthdate').val(obj.user_info[0].dob);
												$('#status').val(obj.user_info[0].merried_status);

												$('#bankForm #account').val(obj.user_info[0].acc_name);
												$('#bankForm #acc_number').val(obj.user_info[0].ac_no);
												$('#bankForm #bank_name').val(obj.user_info[0].bank_nm);
												$('#bankForm #branch_name').val(obj.user_info[0].branch_nm);
												$('#bankForm #swift_code').val(obj.user_info[0].swift_code);


												$('#walletForm #btc').val(obj.user_info[0].bitcoin);
												$('#walletForm #eth').val(obj.user_info[0].ethereum);
												$('#walletForm #etc').val(obj.user_info[0].ethereumc);
												$('#walletForm #xrp').val(obj.user_info[0].ripple);
												$('#walletForm #dtag').val(obj.user_info[0].ripple_tag);
												

									}
								});	
      },
       

     Profile.prototype.init = function () {
      		  		this.getInfo($('#base_url').val(),$('#user_id').val(),$('#token').val(),);
      		  		//this.getBankInfo($('#url').val());
      		  		
      }

    $.Profile = new Profile, $.Profile.Constructor = Profile

}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Profile.init();
    }(window.jQuery);

