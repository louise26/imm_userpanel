<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route[LOGIN_PAGE] 								= 'examples/login';
$route['default_controller'] 					= 'examples';


// D A S H S E C T I O N
$route['dashboard'] 							= 'users/dashboard';
$route['downlinepayments'] 						= 'users/dashboard/retrieve_downline_paymentstatus';
$route['earnings'] 								= 'users/dashboard/earnings';
$route['totalincome'] 							= 'users/dashboard/total_income';


$route['calendar'] 								= 'users/dashboard/calendar';
// E N  D  O F  D A S H B O A R D  S E C T I O N

// A C C O U N T  S E C T I O N
$route['account/settings']						= 'users/profile';
$route['account/userinfo']						= 'users/profile/getInfo';
$route['account/bankinfo']						= 'users/profile/getInfo';
$route['account/updateinfo']					= 'users/profile/updateProfile';
$route['account/updatephoto']					= 'users/profile/upload_photo';
$route['account/updatebankinfo']				= 'users/profile/updateBankInfo';
$route['account/updatewalletinfo']				= 'users/profile/updateWalletInfo';

$route['account/investment']					= 'users/investmentpurchase';
$route['account/purchase']						= 'users/investmentpurchase/purchase';
$route['rankupdate'] 							= 'users/investmentpurchase/updateRank';
$route['account/funds']							= 'users/depositfund';
$route['account/funds/btc']						= 'users/depositfund/depositViaBTC';
$route['account/deposit']						= 'users/depositfund/deposit';

$route['account/deposit-btc']					= 'users/depositfund/proceedPayment';
//$route['account/payment-notif/:any/:any']		= 'users/depositfund/notifyPayment';
$route['account/sendnotif']	                    = 'users/depositfund/sendPaymentNotification';
// E N  D  O F A C C O U N T  S E C T I O N



// R E P O R T S  S E C T I O N
$route['reports/directincome']					= 'users/directincome';
$route['reports/direct-income']					= 'users/directincome/getDirectIncome';
$route['reports/level-income-bonus']			= 'users/directincome/levelIncomeView';
$route['reports/levelincome']					= 'users/directincome/getLevelIncome';

$route['reports/profit-share']					= 'users/directincome/profitIncomeView';
$route['reports/profit-income']					= 'users/directincome/getProfitIncome';

$route['reports/bonus-profit']					= 'users/directincome/bonusIncomeView';
$route['reports/bonus-from-income']				= 'users/directincome/getBonusIncome';

$route['reports/royalty']						= 'users/directincome/royaltyIncomeView';
$route['reports/royalty-bonus']					= 'users/directincome/getRoyaltyIncome';

$route['reports/downline-purchases']			= 'users/directincome/downlineIncomeView';
$route['reports/downline-reports']				= 'users/directincome/getDownlineIncome';
$route['reports/search-downline-purchase']		= 'users/directincome/searchMonthIncome';


$route['reports/rank-achievement']				= 'users/directincome/rankView';
$route['reports/ranks']							= 'users/directincome/getRank';


$route['reports/direct-member']					= 'users/directincome/directMemberView';
$route['reports/direct-member-purchase']		= 'users/directincome/getDirectMember';
$route['reports/royalty-search']			   	= 'users/directincome/searchQualifier';


$route['reports/downline-members']				= 'users/directincome/downlineTreeView';
$route['reports/downline-members-reports']		= 'users/directincome/getDonwlineTree';

$route['reports/downline-member-report/:any']	= 'users/directincome/directMemberViewDonwline';
$route['reports/downline-members-report']		= 'users/directincome/getDirectMemberSponsor';

$route['reports/direct-members']				= 'users/directincome/downlineMemberTreeView';
$route['reports/direct-members-level1']			= 'users/directincome/level1';
$route['reports/direct-members-level2']			= 'users/directincome/level2';
$route['reports/direct-members-level3']			= 'users/directincome/level3';
$route['reports/direct-members-level4']			= 'users/directincome/level4';
$route['reports/direct-members-level5']			= 'users/directincome/level5';
$route['reports/direct-members-level6']			= 'users/directincome/level6';



$route['reports/downline-withdrawals']			= 'users/directincome/downlineWithdrawalView';
$route['reports/downline-withdrawals-search']	= 'users/directincome/searchRequest';

// E N  D  O F  S E C T I O N  S E C T I O N


// P O R T F O L I O  S E C T I O N
$route['portfolio/details']						= 'users/myportfolio';
$route['portfolio/my-details']					= 'users/myportfolio/getDetails';


$route['portfolio/close/:any/:any']				= 'users/myportfolio/closeAccount';

$route['portfolio/convert/:any/:any']			= 'users/myportfolio/convertAccount';




$route['portfolio/close/confirm']				= 'users/myportfolio/confirmClose';
$route['portfolio/convert/confirm']				= 'users/myportfolio/confirmConvert';

// E N  D  O F  P O R T F O L I O  S E C T I O N


// W A L L E T  S E C T I O N
$route['wallet/r-wallet']						= 'users/MyWallet';
$route['wallet/search-user']					= 'users/rwallet/searchUser';
$route['wallet/rwallet-transfer']				= 'users/rwallet/transferRwallet';

$route['wallet/e-wallet']						= 'users/ewallet';
$route['wallet/withdraw']						= 'users/ewallet/sendRequest';
$route['wallet/withdraw-history']				= 'users/ewallet/getWithdrawals';

$route['wallet/transfer']						= 'users/ewallet/transferFund';

$route['wallet/user-rwallet']				   = 'users/MyWallet';
$route['wallet/user-tarnsferrwallet']		   = 'users/MyWallet/transferFund';


$route['wallet/imc-wallet']						= 'users/immwallet';

$route['wallet/create-wallet']					= 'users/immwallet/createWallet';
// E N  D  O F  W A L L  E T  S E C T I O N

$route['referal-links']							= 'users/referals';
$route['promos']								= 'users/referals/promos';
$route['educational-material']					= 'users/referals/educational';

// C O I N S  S E C T I O N
$route['coins/purchases']						= 'users/coins';
$route['coins/purchases/historty']				= 'users/coins/getHistory';
$route['coins/purchases/sellhistorty']			= 'users/coins/getSellHistory';
$route['coins/buy']								= 'users/coins/buycoin';
$route['coins/sell']							= 'users/coins/sellCoin';

$route['coins/purchases/downline']				= 'users/coins/downlinePurchases';
$route['coins/purchases/downlines']				= 'users/coins/downlinecoin_purchase';

$route['coins/mycoins']			            	= 'users/coins/mycoins';
// E N  D  O F  C O I N  S E C T I O N


// H I S T O R Y  S E C T I O N

$route['history/transactions']					= 'users/history';
$route['history/search']						= 'users/history/searchHistory';

$route['history/addfundreport']					= 'users/history/add_fund_report';
$route['history/funds']							= 'users/history/addfund';


$route['history/update']						= 'users/history/updateTransaction';
// E N D  O F  H I S T O R Y  S E C T I O N




$route['register']								= 'users/register';
$route['register/search-sponsor']				= 'users/register/searchUser';
$route['register/create']						= 'users/register/create_user';


$route['password/reset']						= 'users/register/password_reset';
$route['password/send']							= 'users/register/sendCode';

$route['contact-support']						= 'users/contact';

$route['contact-support/send-message']			= 'users/contact/sendMessage';


$route['notification']							= 'users/dashboard/getNewRegistration';
$route['notification/credited']					= 'users/dashboard/getCredited';
$route['notification/debited']					= 'users/dashboard/getDebited';




$route['reset/password/:any']					= 'users/register/confirmCode';
$route['reset/password/confirm/confirms']		= 'users/register/resetView';
$route['reset/password/confirm/confirms/reset']	= 'users/register/resetPassword';

$route['password/recovery']				        ='examples/recover';
$route['password/recovery_verification/:any/:any']				='examples/recovery_verification';


$route['create-wallet']                         = 'users/immwallet/accessApi';

$route['sync-request']                          = 'users/immwallet/requestSync';
$route['account/buyviacard']  					= 'users/investmentpurchase/CreditCardPayment';
$route['account/pay']  							= 'users/investmentpurchase/check';
$route['account/payment-success']  				= 'users/investmentpurchase/successnotice';

$route['recover/password']					    = 'users/register/resetPass';
$route['password/recover']					    = 'users/register/rst';


$route['register/referral/:any']                = 'users/register/referral';





$route['404_override']			= '';
$route['translate_uri_dashes'] 	= FALSE;
