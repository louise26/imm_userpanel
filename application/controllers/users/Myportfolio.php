<?php
defined('BASEPATH') or exit('No direct script access allowed');



class Myportfolio  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		
		


	}

	public function index() {
			if($this->is_logged_in()) {
				
				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		         $this->db->cache_off();
		         
		         $rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									
								];
				return $this->load->view('users/portfolio',$data);
			}
			else {
				redirect('login');
			}


	}

	public function getDetails(){

		     $this->is_logged_in();
		     $use_id 	= $this->auth_user_id;
		     $data 		= [] ;
		     $converted = "";
      		 $converted1 = "";
      		 $action = "";
      		 $action1 = "";
      		 $lifejID = "";
      		 $i = 0 ;
      		   $this->db->cache_off();
		     	foreach ($this->model_credit_amt->query("SELECT * FROM lifejacket_subscription WHERE user_id='$use_id' ORDER BY id DESC")->result() as $key => $value) {
		     							$i +=1;	

		     					if($value->sponsor !="IMM") {
		     									$lifejID = $value->lifejacket_id;
		     					}
		     					else {
		     						   $lifejID = "LJIM" .$value->invoice_no;
		     					}

		     					if($this->model_lifejacket_subscription_coin_converted->count_ref(['id'=>$value->id]) > 0) {
		     									$converted ="IMM Coin Converted";
		     					}
		     					else {
		     						  if($value->sponsor =="IMM"){
		     						  	        $converted1 = "IMM";
		     						  }
		     						  $converted = $converted1. ' ' .$value->remark;
		     					}
		     					if($value->sponsor !="IMM"){
		     						 if($this->model_acc_close_request->count_ref(['lfjid'=>$value->lifejacket_id])>0) {
		     						 				foreach ($this->model_credit_amt->query("SELECT * FROM acc_close_request WHERE lfjid='".$value->lifejacket_id."'")->result() as $key => $value2) {
		     											
			     											if($value2->status==0){
			     													$action ="Request Sent";
			     											}
			     											else {
			     													$action ="Account Closed";
			     											}
		     										}
		     						 }
		     						 else {
		     						 			 $action = "<a class='btn btn-primary' href='close/".$value->lifejacket_id."/".$value->amount."'>Close Account</a>" ;
		     						 }
		     					}
		     				  else {
		     				  				  $action = "Not Allowed";
		     				  }
		     				  	if($this->model_acc_close_request->count_ref(['lfjid'=>$value->lifejacket_id])>0) {

		     				  	 				$action1 = "Not Allowed" ;
		     				  	 }
		     				  	 else {
		     				  	 			if($value->remark =="Package Purchase"){
		     				  	 					$action1 ="<a class='btn btn-primary' href='convert/".$value->lifejacket_id."/".$value->amount."'>Convert to IMM COIN</a>";
		     				  	 			}
		     				  	 			else {
		     				  	 				   $action1 = "Not Allowed";
		     				  	 			}
		     				  	 }

		     				  	 array_push($data, [
		     				  	 						$i,
		     				  	 						$value->transaction_no,
		     				  	 						$lifejID,
		     				  	 						date('F d, Y',strtotime($value->date)),
		     				  	 						'$ ' .$value->amount,
		     				  	 						$converted,
		     				  	 						$action,
		     				  	 						$action1
		     				  	 					]
		     				  	 				);
		     	}


		     $output = [

		     				'data' => $data
		     			];


		     echo json_encode($output);


	}


	public function closeAccount(){

		$id = $this->uri->segment(3);
		$lj = "";
		if($this->is_logged_in()) {

				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		         $this->db->cache_off();






				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' 		=> $this->auth_user_id,
									'info'	  		=> $userinfo,
									'lifejacket_id' => $id,
										
									
								];
				return $this->load->view('users/close_account',$data);


		}

		else {

			  redirect('login');

		}
	

	}



	public function confirmClose() {



		$ljid 	=  $this->input->post('ljid');
		$amount =  $this->input->post('amounts');
		$this->is_logged_in();
		$use_id 	= $this->auth_user_id;
				$data =[

						'lfjid'		=>$ljid ,
						'user_id'	=>$use_id,
						'amount'	=>$amount,
						'status'	=> 0

						];


			if($this->model_acc_close_request->insert($data)) {

					$this->session->set_userdata(['alert'=>'Close account request has been submitted','type'=>'success']);


			}
			else {

					$this->session->set_userdata(['alert'=>'Something went wrong','type'=>'danger']);
			}


			redirect('portfolio/details');

			
	}


	public function convertAccount(){

		$id = $this->uri->segment(3);
		$lj = "";
		if($this->is_logged_in()) {

				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		         $this->db->cache_off();

				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' 		=> $this->auth_user_id,
									'info'	  		=> $userinfo,
									'lifejacket_id' => $id,
										
									
								];
				return $this->load->view('users/convert_account',$data);
		}

		else {

			  redirect('login');

		}
	

	}




	public function confirmConvert() {



		$ljid 	=  $this->input->post('ljid');
		$amount =  $this->input->post('amounts');
		$this->is_logged_in();
		
		
		$percetage = 0 ;

		$pv =1;
		$use_id 	= $this->auth_user_id;
				$data =[
    						'lfjid'		=>$ljid ,
    						'user_id'	=>$use_id,
    						'amount'	=>$amount,
    						'status'	=> 0
					    ];

        
            $this->db->cache_off();
            foreach($this->model_users->query("Select percent from imm_coin_percent")->result() as $value){
                
                    $percentage  =  $value->percent;
            }
            

			if($this->model_lifejacket_subscription_coin_converted->query("INSERT INTO lifejacket_subscription_coin_converted SELECT d.* FROM lifejacket_subscription d WHERE lifejacket_id='$ljid'")) {


					$walleting      = 'final_imm_coin_wallet';
					$convertamt     = $amount  ;
					$coins          = $convertamt;
					$this->model_acc_close_request->query("update $walleting set amount=(amount+$coins) where user_id='".$use_id."'");
					$this->model_acc_close_request->query("update lifejacket_subscription set remark='Coin Converted', lifejacket_id='$walleting', sponsor='IMM' where lifejacket_id='$ljid'");
					$this->session->set_userdata(['alert'=>'Successfully converted to IMM COIN','type'=>'success']);
			}
			else {

					$this->session->set_userdata(['alert'=>'Something went wrong','type'=>'danger']);
			}

			redirect('portfolio/details');

			
	}



}
