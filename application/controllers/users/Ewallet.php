<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  Ewallet  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');

		//Load models
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_withdraw_request');
		$this->load->model('model_transfer_withdraw_history');
		
	
	}

	public function index() {
			if($this->is_logged_in()) {
				
				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		         $this->db->cache_off();
		         
		         $rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
											'first_name'	=>  $value->first_name,
											'last_name'		=>  $value->last_name,
											'acc_name'		=>  $value->acc_name,
											'acc_number'	=>	$value->ac_no,
											'bank_nm'		=>	$value->bank_nm,
											'branch_nm'		=> 	$value->branch_nm,
											'swift_code'	=>	$value->swift_code,
											'bitcoin'		=>	$value->bitcoin,
											'ethereum'		=>	$value->ethereum,
											'ripple'		=>	$value->ripple,
											't_code'		=> 	$value->t_code
										];
										
									$this->session->set_userdata([
																	'username' 		=>$value->email,
																	'first_name'	=>$value->first_name,
																]);
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									'ewallet_balance' 	=> $this->model_final_e_wallet->select('amount',['user_id'=>$use_id]),
									'stat' 	=> $this->model_final_e_wallet->query("Select status from popup where id='2'")->result()
								];


				return $this->load->view('users/ewallet',$data);
			}
			else {
				redirect('login');
			}


	}


	public function sendRequest() {

			$this->is_logged_in();

			$use_id =	$this->auth_user_id;

			$data 	= [] ;

			$first_name 	= $this->input->post('first_name');
			$last_name 		= $this->input->post('last_name');
			$acc_name 		= $this->input->post('acc_name');
			$acc_number 	= $this->input->post('ac_no');
			$bank_nm 		= $this->input->post('bank_nm');
			$branch_nm 		= $this->input->post('branch_nm');
			$amount 		= $this->input->post('amount');
			$t_code 		= $this->input->post('t_code');
			$password 		= $this->input->post('password');
			$amount 		= $this->input->post('amount');
			$service_charge = $this->input->post('service_charge');
			$net_amount 	= $this->input->post('net_amount');
			$payment 		= $this->input->post('payment');
			$description 	= $this->input->post('description');
			$balance 		= $this->input->post('balance');
			$swift_code 	= $this->input->post('swift_code');
			$bitcoin 		= $this->input->post('bitcoin');
			$ethereum 		= $this->input->post('ethereum');
			$ripple 		= $this->input->post('ripple');
				$email 			= "";


			$rand			= rand(0,1000000);
			$urls 			= "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			$date 			= date("Y-m-d");

			$new_wallet_bal = 0 ;

			$balances  =  0 ;

			$this->db->cache_off();
			foreach ($this->model_users->select('email',['user_id'=>$use_id]) as $key => $value) {
					
						$email = $value->email;
			}


			$this->db->cache_off();
			foreach ($this->model_final_e_wallet->select('amount',['user_id'=>$use_id]) as $key => $value) {
					
					$balances =  $value->amount;
			}



			$new_wallet_bal = $balances - $amount ;


			$wallet_data 	=  [
									'amount'=>$new_wallet_bal
								 ];

			$user_data = [
							'id'				=> NULL,
							'transaction_no'	=> 'IMM'.$date.$rand,
							'user_id' 			=> $use_id,
							'credit_amt'		=> '0',
							'debit_amt'			=> $amount,
							'admin_charge'		=> '0',
							'receiver_id'		=> '123456',
							'sender_id'			=> $use_id,
							'receive_date'		=> $date,
							'ttype'				=> $payment,
							'TranDescription'	=> 'Withdrawal Request From Admin',
							'Cause'				=> '0',
							'Remark'			=> 'Withdrawal Request',
							'invoice_no'		=> 'IMM'.$date.$rand,
							'product_name'		=>'Withdrawal Request',
							'status'			=> '0',
							'ewallet_used_by'	=> 'Withdrawal Ewallet',
							'current_url'		=> $urls

						];



			$request_data = [
								'id'					=> NULL,
								'transaction_number'	=> 'IMM'.$date.$rand,
								'user_id'				=> $use_id,
								'first_name'			=> $first_name,
								'last_name'				=> $last_name,
								'acc_name'				=> $acc_name,
								'acc_number'			=> $acc_number,
								'bank_nm'				=> $bank_nm,
								'branch_nm'				=> $branch_nm,
								'swift_code'			=> $swift_code,
								'request_amount'		=> $net_amount,
								'description'			=> $description,
								'admin_remark'			=> '',
								'admin_response_date'	=> '',
								'status'				=> '0',
								'posted_date'			=> $date,
								'withdraw_wallet'		=> 'final_e_wallet',
								'total_paid_amount'		=> $amount,
								'transaction_charge'	=> $service_charge,
								'bitcoin'				=> $bitcoin,
								'ripple'				=> $ripple,
								'ethereum'				=> $ethereum,
								'trans_type' 			=> $payment

							];


			if($password != $t_code){
					
					$data =  [
								'title'		=> 'Oops !',
								'msg'		=> 'Invalid Transaction Password',
								'status'	=> 'error'
							 ];

							 echo json_encode($data);


			}
			else if($password == $t_code) {
                      if($amount >=10){
							
        						if($balances > $amount) {
        								if($this->model_final_e_wallet->update($wallet_data,['user_id'=>$use_id])) {
        												$this->model_credit_amt->insert($user_data);	
        												$this->model_withdraw_request->insert($request_data);
        												//$this->sendEmailToClient($use_id,$amount,$service_charge,$payment,$email,$net_amount);
        											$data =  [
        
        														'title'		=> 'Good Job !',
        														'msg'		=> 'Your request has been sent',
        														'status'	=> 'success'
        
        											 		];
        								}
        								else {
        													$data =  [
        																'title'		=> 'Oops !',
        																'msg'		=> 'Something Went Wrong',
        																'status'	=> 'error'
        														 ];
        								}
        
        						}
        
        						else {
        
        
        								$data =  [
        																'title'		=> 'Oops !',
        																'msg'		=> 'Insufficient Balance in wallet',
        																'status'	=> 'error'
        														 ];
        						}
                        }
						
						else {
						    
						    $data =  [
																'title'		=> 'Oops !',
																'msg'		=> 'Minimum withdrawal is $10',
																'status'	=> 'error'
														 ];
						}


				echo json_encode($data);
			}
	}

	public function transferFund() {


			$this->is_logged_in();

			$use_id =	$this->auth_user_id;
			$data 	= [] ;

			$first_name 	= "";
			$last_name 		= "";
			$t_code 		= $this->input->post('t_code');
			$password 		= $this->input->post('password');
			$amount 		= $this->input->post('amount');
			$service_charge = $this->input->post('service_charge');
			$net_amount 	= $this->input->post('net_amount');
			$user 			= $this->input->post('user');
			$balance 		= $this->input->post('balance');
		

			$rand			=  rand(0000000001,9999999999);
			$urls 			=  "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			$date 			=  date("Y-m-d");

			$new_wallet_bal 	= 0 ;
			$reg_wallet_balance = 0 ;
			$e_wallet_balance = 0 ;

		
			foreach ($this->model_final_reg_wallet->select('amount',['user_id'=>$use_id]) as $key => $r) {
						$reg_wallet_balance = $r->amount;
			}

			
			foreach ($this->model_final_e_wallet->select('amount',['user_id'=>$use_id]) as $key => $e) {
						$e_wallet_balance = $e->amount;
			}

			$nt = $amount - ($amount * 0.05);

            foreach($this->model_users->select('first_name,last_name',['user_id'=>$use_id]) as $key => $person){
                $first_name =   $person->first_name;
                $last_name  =   $person->last_name;
            }
			if($t_code ==$password) {

								$user_data =  [
												'user_id'			=>  $use_id,
												'credit_amt' 		=>  $nt,
												'receiver_id'		=>  $use_id,
												'receive_date'		=>  $date,
												'TranDescription'	=> 'Transfer fund by'  .$first_name .' '. $last_name. 'to' .$first_name .' '. $last_name,
												'Remark'			=> 'Transfer fund by'  .$first_name .' '. $last_name. 'to' .$first_name .' '. $last_name,
												'Cause'				=> 'Transfer fund by'  .$first_name .' '. $last_name. 'to' .$first_name .' '. $last_name,
												'transaction_no'	=>  $rand,
												'invoice_no'		=>  $rand,
												'ttype'				=> 'Fund Transfer',
												'product_name'		=> 'Fund Transfer',
												'ewallet_used_by'	=> 'Withdrawal Wallet',
												'current_url'		=>  $urls,
												'debit_amt'			=> '0',
												'status'			=> '0',
												'admin_charge'		=>  $service_charge
											];

								$user_data1 =  [
													'user_id'			=>  $use_id,
													'debit_amt' 		=>  $amount,
													'receiver_id'		=>  $use_id,
													'receive_date'		=>  $date,
													'TranDescription'	=> 'Transfer fund by'  .$first_name .' '. $last_name. 'to' .$first_name .' '. $last_name .'with  5% admin charge',
													'Remark'			=> 'Transfer' .$amount. 'to' .$first_name .' '. $last_name  .'with  5% admin charge',
													'Cause'				=> 'Transfer fund by'  .$first_name .' '. $last_name. 'to' .$first_name .' '. $last_name,
													'transaction_no'	=>  $rand,
													'invoice_no'		=>  $rand,
													'ttype'				=> 'Fund Transfer',
													'product_name'		=> 'Fund Transfer',
													'ewallet_used_by'	=> 'Withdrawal Wallet',
													'current_url'		=>  $urls,
													'credit_amt'		=> '0',
													'status'			=> '0',
													
											];
                            if($amount >=10){
        						if($e_wallet_balance > $amount) {
        										
        										$this->db->cache_off();
        
        										$this->model_credit_amt->insert($user_data);
        
        										$final_amount = $reg_wallet_balance + $nt;
        
        										//$this->model_final_reg_wallet->update(['amount'=>$final_amount],['user_id'=>$use_id]);
        
        										$final_e_wallet_bal = $e_wallet_balance - $amount;
        
        										$this->model_credit_amt->insert($user_data1);
        
        										$this->model_final_e_wallet->update(['amount'=>$final_e_wallet_bal],['user_id'=>$use_id]);
        										
        										if($this->model_final_reg_wallet->update(['amount'=>$final_amount],['user_id'=>$use_id])) {
            										    
            										    
            										    $this->model_transfer_withdraw_history->insert([
            										                                                    'user_id' => $use_id,
            										                                                    'action'  => 'Ewallet to r wallet',
            										                                                    'new_balance'  => $final_amount,
            										                                                    'current_balance' => $reg_wallet_balance,
            										                                                    'link'  => $urls
            										                                                ]);
            									}
        
        									$data =  [
        												'title'		=> 'Good Job !',
        												'msg'		=> 'Your have successfully transferred your amount',
        												'status'	=> 'success'
        											];
        
        							}
        							else {
        									$data =  [
        												'title'		=> 'Oop !',
        												'msg'		=> 'You have insufficient balance in your e-wallet',
        												'status'	=> 'error'
        											];
        
        							}
                            }
							else {
							    	$data =  [
												'title'		=> 'Oop !',
												'msg'		=> 'Minimum amount is $10',
												'status'	=> 'error'
											];
							}
			}
			else {
									$data =  [
												'title'		=> 'Oops !',
												'msg'		=> 'Wrong Transaction Password',
												'status'	=> 'error'
											];

			}


			echo json_encode($data);

	}

	public function getWithdrawals(){

				$this->is_logged_in();

				$use_id =	$this->auth_user_id;

				$data 	= []  ;

				$i 		= 0 ;
				$stat 	= "" ;

				$this->db->cache_off();

					foreach ($this->model_withdraw_request->select('*',['user_id'=>$use_id]) as $key => $value) {
										
							$i +=1;

							if($value->status==1){
								 $stat ="Paid" ;
							}
							else {
								 $stat = "Pending";
							}

							array_push( $data,[
												$i,
												$value->transaction_number,
												'$ ' .$value->total_paid_amount,
												'$ ' .$value->transaction_charge,
												'$ ' .$value->request_amount,
												date('F d, Y',strtotime($value->posted_date)),
												$value->admin_remark,
												$value->admin_response_date,
												$value->trans_type,
												$stat
										]);
					}

			$output = [
						'data' => $data
					  ];

		echo json_encode($output);				


	}


	public function sendEmailToClient($username,$amount,$charge,$type,$email,$net_amount){

			    $date = date('F d, Y');
            	$from = 'info@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders | Withdrawals <$from>" . "\r\n";
                
			$msg = '<!doctype html>
              <html>
              <head>
                  <meta charset="utf-8">
                  <title>Withdrawal Approval</title>
                  <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
                  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
              </head>
              <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
                  <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
                      <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
                          <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
                              <img src="https://immtradersclub.com/images/logo1.png" height="70"><br/><br/>
                              <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
              line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
                                 	Withdrawal Request
                              </div>
                              <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
                                  Hello '.$username.' your Withdrawal request successfully submitted
                              </div>
                              <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
                              </div>
                              <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
              color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
                                  <p style="margin: 0px !important;">We are happy to inform you that your withdrawal request in the amount of $'.$amount.' USD made on '.$date.', has been processed and the funds will be transferred as followed:</p>
                              </div>
                              <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
                               
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px">Payment Method</h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px">'.$type.'</h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px">Net Amount </h3>
                                   <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px">'.$net_amount.'</h3>
                                 
                                  
                              </div>
                              <div class="ttl" style="margin: 14px 0px 0px 10px;width: 500px;font-family: Lato;font-weight: 400;color: rgb(255, 255, 255);font-size: 15px;line-height: 35px;padding: 6px 0px;background-color: rgb(230, 67, 60);">
                                 Payments made by wire transfer/Bitcoin might take up to 5 working days to reflect in your account balance.<br>
                                 This withdrawal request is subjected to a fee of '.$charge.' @ 5% USD, which was deducted from the total withdrawal amount.
                              </div>
                              <div class="line" style="height: 1px;background: rgb(218, 218, 218) none repeat scroll 0% 0%;margin-top: 20px;">
                              </div>
                              <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
                                We wish you a most profitable and enjoyable trading experience.<br><center>Customer Support Team</center> </p>
                          </div>
                      </div>
                  </div>
                  </div><br/><br/>
              </body>
              </html>';
				
				$subject = "Withdrawal Request Successfully Submitted";
				mail ( $email, $subject, $msg, $headeruser1 );
	}

}
