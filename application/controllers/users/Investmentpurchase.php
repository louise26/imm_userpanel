<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  Investmentpurchase  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		$this->db->cache_off();
	


	}

	
		public function index(){

			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
                
				$this->db->cache_off();
				
				$rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,

										];
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
										

								];
					return $this->load->view('users/packagepurchase',$data);

			}
			else {

				redirect('login');
			}
		}

	/* Sponsor Commission Code Starts Here*/
	public function commission_of_referal($ref,$useridss,$amount,$invoice_no,$packages){

	
		$date=date('Y-m-d');

   		$this->db->cache_off();
		foreach ($this->model_users->select('*',['user_id'=>$ref]) as $key => $value) {
			
			if($value->user_rank_name=='Cadet')
			{
				$spc=3;
			}
			else if($value->user_rank_name=='Rising Star')
			{
				$spc=5;
			}
			else if($value->user_rank_name=='Flying Star')
			{
				$spc=7;
			}
			else if($value->user_rank_name=='Champion')
			{
				$spc=8;
			}
			else if($value->user_rank_name=='Elite')
			{
				$spc=10;
			}
			else if($value->user_rank_name=='Co Founder')
			{
				$spc=10;
			}
			else 
			{
				$spc=0;
			}
		}
		$amount = $amount ;
		$pb 	= $amount ;

		$withdrawal_commission	=	$spc*$amount/100;
		$rwallet 				= 	$withdrawal_commission;
		$e_walletbal = 0 ;
		$new_balances = 0 ;

		if($withdrawal_commission!='' && $withdrawal_commission!=0)
			{
					$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					
					$this->db->cache_off();
			                        
			         foreach($this->model_final_e_wallet->select('amount',['user_id'=>$ref]) as $val){
			                               	
			                 $e_walletbal = $val->amount;         
			         }
			                 $new_balances = $rwallet + $e_walletbal;
			                        
			                 $this->model_final_e_wallet->update(['amount'=>$new_balances],['user_id'=>$ref]);
	
   					//$this->model_credit_amt->query("update final_e_wallet set amount=(amount+$rwallet) where user_id='$ref'");
   						
   					$this->model_credit_amt->query("insert into credit_debit values(NULL,'$invoice_no','$ref','$rwallet','0','0','$ref','$useridss','$date','Referral Bonus','Earn Referral Bonus from $useridss for $packages Package','Commission of MYR $rwallet For Package ".$amount." ','Referral Bonus','$invoice_no','Referral Bonus','0','Withdrawal Wallet',CURRENT_TIMESTAMP,'$urls')");

			}
			$this->commission_of_level($useridss,$amount,$invoice_no,$packages);

	}
	/* Sponsor Commission Code Ends Here*/

	/* Sponsor Commission Code Starts Here*/

	public function  commission_of_level($useridss,$amount,$invoice_no,$packages){

			$date =		date('Y-m-d');
			//$spc  =		0;

			$this->db->cache_off();
			foreach ($this->model_matrix_downline_ref->query("select * from matrix_downline_ref where down_id='$useridss' and level<7")->result() as $key => $value) {
						$ref = $value->income_id;
						$level = $value->level;
				foreach ($this->model_users->select('*',['user_id'=>$value->income_id]) as $key => $value1) {
									if($value1->user_rank_name=='Cadet' && $value->level==2)
									{
											$spc=2;
									}
									else if($value1->user_rank_name=='Cadet' && $value->level==3)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Rising Star' && $value->level==2)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Rising Star' && $value->level==3)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==2)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==3)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==4)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==2)
									{
										$spc=5;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==3)
									{
								 		$spc=3;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==4)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==5)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==2)
									{
										$spc=5;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==3)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==4)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==5)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==6)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==2)
									{
									 	$spc=5;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==3)
									{
										$spc=4;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==4)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==5)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==6)
									{
										$spc=1;
									}
									else 
									{
										$spc=0;
									}
	
					}
					$withdrawal_commission=$spc*$amount/100;
    
					$rwallet=$withdrawal_commission;
						$e_walletbal = 0 ;
					$new_balances = 0 ;

					if($withdrawal_commission!='' && $withdrawal_commission!=0)
					{
			                                  $this->db->cache_off();
			                        
			                        foreach($this->model_final_e_wallet->select('amount',['user_id'=>$value->income_id]) as $val){
			                               	$e_walletbal = $val->amount;         
			                        }
			                        $new_balances = $rwallet + $e_walletbal;
			                        
			                        $this->model_final_e_wallet->update(['amount'=>$new_balances],['user_id'=>$value->income_id]);
										//$this->model_credit_amt->query("update final_e_wallet set amount=(amount+$rwallet) where user_id='$ref'");

		 								$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	    								
	    								$this->model_credit_amt->query("insert into credit_debit values(NULL,'$invoice_no','$ref','$rwallet','0','0','$ref','$useridss','$date','Unilevel Bonus','Earn Unilevel Bonus from $useridss for $packages Package','Commission of USD $rwallet For Package ".$amount." ','Unilevel $level Bonus','$invoice_no','Unilevel Bonus','0','Withdrawal Wallet',CURRENT_TIMESTAMP,'$urls')");
	    						 				//Rank_update($useridss);
					}
			}
	}

	/* Sponsor Commission Code Ends Here*/


	public function purchase(){


			$this->is_logged_in();

			$use_id 		= $this->auth_user_id;
			$amount   		=  $this->input->post('amount');
			$password		=  $this->input->post('password');
			$data 			=	[];
			$ewa='final_reg_wallet';
			$walls="Withdrawal Reg Wallet";	

			$rand = rand(0000000001,9000000000);

			$invoice_no = $rand;
    		$start=date('Y-m-d');
    		$end = date('Y-m-d', strtotime('+12 months'));
    		$user_ewalletamt = 0;
    		$new_bal 		= 0;
    		$balance  		= 0;

    		$id = "";
			$this->db->cache_off();
			
				if($amount < 10){

										array_push($data,[

														'title'		=> 'Oops !',
														'msg'		=> 'Minimum Amount is $10',
														'status'	=> 'error'

												]);
				}
				else {

							$this->db->cache_off();
								foreach ($this->model_final_reg_wallet->select('*',['user_id'=>$use_id]) as $key => $value) {
											$user_ewalletamt=$value->amount;
								}

								$lfid="LJ".$use_id.$rand;
										

								if($user_ewalletamt > $amount){

										$balance = $user_ewalletamt - $amount ;
										foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {
										
										  	$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

											$id = $value->ref_id;

											if($this->model_final_reg_wallet->update(['amount'=>$balance],['user_id'=>$use_id])){


		    									$this->model_credit_amt->query("INSERT INTO `lifejacket_subscription` (`id`, `user_id`, `package`, `amount`, `pay_type`, `pin_no`, `transaction_no`, `date`, `expire_date`, `remark`, `ts`, `status`, `invoice_no`,`lifejacket_id`,`username`,`sponsor`,`pb`) VALUES (NULL, '$use_id', '$amount', '$amount', '$walls', '$password', '$rand', '$start', '$end', 'Package Purchase', CURRENT_TIMESTAMP, 'Active', '$rand','$lfid','".$use_id."','".$value->ref_id."','$amount')");

		    									$this->model_credit_amt->query("insert into credit_debit (`transaction_no`,`user_id`,`credit_amt`,`debit_amt`,`admin_charge`,`receiver_id`,`sender_id`,`receive_date`,`ttype`,`TranDescription`,`Cause`,`Remark`,`invoice_no`,`product_name`,`status`,`ewallet_used_by`,`current_url`) values('$rand','$use_id','0','$amount','0','$use_id','$use_id','$start','Package Purchase','Package Purchase by $use_id','Package Purchase by $use_id ','Package Purchase $use_id','$rand','Package Purchase by $use_id','0','$walls','$urls')");
											

		    										if($value->user_rank_name=='Normal User'){
		    													$this->model_credit_amt->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`,`price`) VALUES (NULL, '$use_id', 'Normal User', 'Cadet', CURRENT_TIMESTAMP, '$start','0')");
		    													$this->model_users->update(['user_rank_name'=>'Cadet','designation'=>'Cadet'],['user_id'=>$use_id]);
											
		    										}
		    										
											}


											else {

													array_push($data,[

																		'title'		=> 'Oops !',
																		'msg'		=> 'Something went wrong',
																		'status'	=> 'error'

																	]);
											}
										}


											foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value3) {
		    												$this->commission_of_referal($value3->ref_id,$use_id,$amount,$invoice_no,$amount); //commison for referral
		    										
											}
												//$this->rankfile(); //Update user ranks
												
												

												array_push($data,[
																		'title'		=> 'Good Job !',
																		'msg'		=> 'Package has been purchased',
																		'status'	=> 'success'
																	]);
																	
														$this->check_rank_kams($use_id);
													

								}

								else {

											array_push($data,[
																'title'		=> 'Oops !',
																'msg'		=> 'Insufficient Fund',
																'status'	=> 'error'


														]);


								}
									
				}
				echo json_encode($data);
	
	}
	
	public function updateRank() {
	    
	    	$data = array();
                 $this->is_logged_in();
			   	$use_id =	$this->auth_user_id;
			   	//$use_id = 'IMM1201657' ;
			   	$stat 	=  "";
			   	$sender =  "";
			   	$transd =  "";
			   	$name 	= "";
			   	$sendid = "";
			   	$total = 0 ;

			   	$start 	=	date('Y-m-').'01';
                $end 	=	date('Y-m-').'31';

                $start1 	=	'2017-11-01';
                $end1 		=   '2017-11-31';
                
                $downline_total = 0;
	
                     	foreach ($this->model_credit_amt->query("select * from user_registration where ref_id='$use_id'")->result() as $key => $value1) {
                     				
                     				$i +=1;
									$t1 		= 0; 
									$totaffl 	= 0;
								    $te1 		= 0;
								    $te_nov1 	= 0;
								    $te_nov123 	= 0;
                     				
                     				foreach ($this->model_credit_amt->query("select * from matrix_downline_ref where income_id='".$value1->user_id."'")->result() as $key => $value2) {
                     							
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1 from lifejacket_subscription where user_id='".$value2->down_id."'")->result() as $key => $value3) {

                     								$t 	 = $value3->tot1;
													$t1 += $t;

                     							}
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1 from lifejacket_subscription where user_id='".$value2->down_id."' and (date between '$start' and '$end')")->result() as $key => $value3) {
                     								 $te 	 = $value3->tot1;
													 $te1 	+= $te;
                     							}
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1_nov from lifejacket_subscription where user_id='".$value2->down_id."' and (date between '$start1' and '$end1')")->result() as $key => $value3) {
                  
                     									$te_nov=$value3->tot1_nov;
														$te_nov1+=$te_nov;

                     							}

                     				}
                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot from lifejacket_subscription where user_id='".$value1->user_id."'")->result() as $key => $c1) {
                     							 $total= $t1+$c1->tot;
                     				}

                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot from lifejacket_subscription where user_id='".$value1->user_id."' and (date between '$start' and '$end')")->result() as $key => $crr1) {
                     							 $totaffl= $te1+$crr1->tot;

                     				}
                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot_no from lifejacket_subscription where user_id='".$value1->user_id."' and (date between '$start1' and '$end1')")->result() as $key => $crr1_nov) {
                     						 $te_nov123= $te_nov1+$crr1_nov->tot_no;
                     				}

                     					$downline_total +=$total;
                     				    //echo  '$ ' . number_format($total,2) .'<br>';
                     					
                     		}
					   	
	            
	           	
	           	$this->rankAchieve($downline_total,$use_id);
	    
	}
	
	
	public function rankAchieve($totsum,$user) {
	    
	    
	            $rank = '' ;
	            
	            $existing = FALSE ;
	            
	            
	           
	            
	            foreach($this->model_users->select('user_rank_name',['user_id'=> $user]) as $key => $value){
	                $rank = $value->user_rank_name;
	            }
	            
	            
	             foreach($this->model_users->query("SELECT count(user_id) as usr from rank_achiever WHERE last_rank='". $rank ."' && user_id='$user'  ")->result() as $key => $value11) {
	                
	                        if($value11->usr == 0 ) {
	                             $existing = FALSE ;
	                        }
	                        
	                        else if($value11->usr > 0) {
	                            $existing = TRUE ;
	                        }
	            }
	            
        	   if($totsum>=1500000 )
        	{
               	  //mysql_query("update user_registration set user_rank_name='Co Founder', designation='Co Founder' where user_id='$user'");
               	  
               	  $this->model_users->query("update user_registration set user_rank_name='Co Founder', designation='Co Founder' where user_id='$user'");
        
               	 // mysql_query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Elite', 'Co Founder', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
               	 if($existing) {
               	     $this->model_users->query("INSERT INTO rank_achiever (id, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Co Founder', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
               	 }
               	  
        
        	}
        	else if($totsum>=500000)
        	{
        
        	    // mysql_query("update user_registration set user_rank_name='Elite', designation='Elite' where user_id='$user'");
        	   $this->model_users->query("update user_registration set user_rank_name='Elite', designation='Elite' where user_id='$user'");
        	   // mysql_query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Champion', 'Elite', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
        	   	 if($existing) {
        	            $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Elite', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
        	   	 }
        
        	}
        	else if($totsum>=100000)
        	{
                   $this->model_users->query("update user_registration set user_rank_name='Champion', designation='Champion' where user_id='$user'");
                if($existing) {
                   $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Champion', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
                }
                
        	}
        	else if($totsum>=35000)
        	{
              
                  $this->model_users->query("update user_registration set user_rank_name='Flying Star', designation='Flying Star' where user_id='$user'");
                  	 if($existing) {
                         $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Flying Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
                  	 }
        	}
        	else if($totsum>=5000 )
        	{
    
        	     $this->model_users->query("update user_registration set user_rank_name='Rising Star', designation='Rising Star' where user_id='$user'");
                 if($existing) {
                    $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Rising Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
                 }
        	}

        	else
        	{
              
        	} 
        	
	}
	public function CreditCardPayment(){



			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,

										];
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
										

								];
					
				return $this->load->view('users/cardpayment',$data);	

			}
			else {

				redirect('login');
			}
	}


	public function check(){

		
			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;
		     if(!empty($_POST['stripeToken']))
		    {
    			//get token, card and user info from the form
    			$token  = $_POST['stripeToken'];
    			$name = $_POST['name'];
    			$email = $_POST['email'];
    			$coin_type = $_POST['typ'];
    			$card_num = $_POST['card_num'];
    			$card_cvc = $_POST['cvc'];
    			$card_exp_month = $_POST['exp_month'];
    			$card_exp_year = $_POST['exp_year'];
    			
    			//include Stripe PHP library
    			require_once APPPATH."third_party/stripe/init.php";
    			
    			//set api key
    			
    			
    			try {
    			$stripe = array(
    			  "secret_key"      => "sk_live_0AAYM72nQ3fVmXbOQektbGW6",
    			  "publishable_key" => "pk_live_gzQWobLB8Uis8dW13f0hzys8"
    			);
    			
    				// $stripe = array(
        //     			  "secret_key"      => "sk_test_0C3fi4zRGsFaWTibVEgiOsW8",
        //     			  "publishable_key" => "pk_test_iu1l1HPuUeXqmyB193QpHzM0"
        //     			);
    			
    			//sc =  sk_live_0AAYM72nQ3fVmXbOQektbGW6
    			//pk =  pk_live_gzQWobLB8Uis8dW13f0hzys8
    			
    			\Stripe\Stripe::setApiKey($stripe['secret_key']);
    			
    			//add customer to stripe
    			$customer = \Stripe\Customer::create(array(
    				'email' => $email,
    				'source'  => $token
    			));
    			
    			//item information
    			$ebook = "" ;
    			
    			if($coin_type =="coin") {
    			        	$ebook = 'IMC Ebook Purchase' ;
    			}
    			else if($coin_type =="package") {
    			    	$ebook = "IMM Ebook Purchase";
    			    
    			}
    			$itemName   = strtoupper($ebook);
    			$itemNumber = $use_id ;
    		    $price      = $_POST['amount'] * 0.07;
    		    $newprice   = number_format(($_POST['amount'] + $price),2, '.', '');
    		    $myprice    = "$newprice";
    		    $finalprice = str_replace('.', '', $myprice);
    			$itemPrice  = $finalprice  ;
    
    			
    			$currency   = "usd";
    		
    			
    			//charge a credit or a debit card
    			$charge = \Stripe\Charge::create(array(
    				'customer' => $customer->id,
    				'amount'   => $itemPrice,
    				'currency' => $currency,
    				'description' => $itemName.' '.$itemNumber ,
    				'metadata' => array(
    					'item_id' => $itemNumber
    				)
    			));
			
			    //retrieve charge details
			    $chargeJson = $charge->jsonSerialize();

    			//check whether the charge is successful
    			if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
    			{
                    //$this->sendEmailToSender($_POST['amount'],$email,$use_id);
    				
    				if($coin_type=="coin") {
    				    
    				      if($_POST['amount'] >100) {
    				         $this->depositrwallet($_POST['amount']);
    				      }
    				      else {
    				          	$this->buycoin($_POST['amount']);
    				      }
    				}
    				else if($coin_type=="package") {
    				    
    				    if($_POST['amount'] >100) {
    				         $this->depositrwallet($_POST['amount']);
    				        
    				    }
    				    else if($_POST['amount'] <=100) {
    				         $this->paid($_POST['amount']);
    				    }
    				}
    				redirect('account/payment-success','refresh');
    			}
			else
			{
				echo "Invalid Token";
				$statusMsg = "";
			}
		
		}  catch(Stripe_CardError $e) {
          // Since it's a decline, Stripe_CardError will be caught
          $body = $e->getJsonBody();
          $err  = $body['error'];
        
          print('Status is:' . $e->getHttpStatus() . "\n");
          print('Type is:' . $err['type'] . "\n");
          print('Code is:' . $err['code'] . "\n");
          // param is '' in this case
          print('Param is:' . $err['param'] . "\n");
          print('Message is:' . $err['message'] . "\n");
        } catch (Stripe_InvalidRequestError $e) {
          // Invalid parameters were supplied to Stripe's API
             $body = $e->getJsonBody();
             $err  = $body['error'];
              $this->errornotice($err['message']);
        } catch (Stripe_AuthenticationError $e) {
          // Authentication with Stripe's API failed
           $body = $e->getJsonBody();
             $err  = $body['error'];
              $this->errornotice($err['message']);
          
          // (maybe you changed API keys recently)
        } catch (Stripe_ApiConnectionError $e) {
          // Network communication with Stripe failed
          
           $body = $e->getJsonBody();
             $err  = $body['error'];
              $this->errornotice($err['message']);
        } catch (Stripe_Error $e) {
          // Display a very generic error to the user, and maybe send
         //  echo var_dump($e);
          // yourself an email
            $body = $e->getJsonBody();
             $err  = $body['error'];
              $this->errornotice($err['message']);
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
             $err  = $body['error'];
        
                     
                     $this->errornotice($err['message']);
        }
		
		    }
	}
		else {


			redirect('login');
		}
	}
public function errornotice($error){
    
    	if($this->is_logged_in()){

					$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									'message'  => $error
								];
					
				return $this->load->view('users/errorpayment',$data);	

			}
			else {

				redirect('login');
			}
    
}
	public  function successnotice(){

		if($this->is_logged_in()){

					$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
					
				return $this->load->view('users/successpayment',$data);	

			}
			else {

				redirect('login');
			}
	}


		public function paid($amount){


			$this->is_logged_in();

			$use_id 		= $this->auth_user_id;
			$password    	=  '';
			
			$data 			=	[];
			$ewa='final_reg_wallet';
			$walls="Withdrawal Reg Wallet";	

			$rand = rand(0000000001,9000000000);

			$invoice_no = $rand;
    		$start=date('Y-m-d');
    		$end = date('Y-m-d', strtotime('+12 months'));
    		$user_ewalletamt = 0;
    		$new_bal 		= 0;
    		$balance  		= 0;

    		$id = "";
			

							$this->db->cache_off();
							
								$lfid="LJ".$use_id.$rand;
										
										foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {
										
										  	$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

											$id = $value->ref_id;
		    								$this->model_credit_amt->query("INSERT INTO `lifejacket_subscription` (`id`, `user_id`, `package`, `amount`, `pay_type`, `pin_no`, `transaction_no`, `date`, `expire_date`, `remark`, `ts`, `status`, `invoice_no`,`lifejacket_id`,`username`,`sponsor`,`pb`) VALUES (NULL, '$use_id', '$amount', '$amount', '$walls', '$password', '$rand', '$start', '$end', 'Package Purchase', CURRENT_TIMESTAMP, 'Active', '$rand','$lfid','".$use_id."','".$value->ref_id."','$amount')");

		    								$this->model_credit_amt->query("insert into credit_debit (`transaction_no`,`user_id`,`credit_amt`,`debit_amt`,`admin_charge`,`receiver_id`,`sender_id`,`receive_date`,`ttype`,`TranDescription`,`Cause`,`Remark`,`invoice_no`,`product_name`,`status`,`ewallet_used_by`,`current_url`) values('$rand','$use_id','0','$amount','0','$use_id','$use_id','$start','Package Purchase','Package Purchase by $use_id','Package Purchase by $use_id ','Package Purchase $use_id','$rand','Package Purchase by $use_id','0','$walls','$urls')");
											
		    								if($value->user_rank_name=='Normal User'){
		    											$this->model_credit_amt->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`,`price`) VALUES (NULL, '$use_id', 'Normal User', 'Cadet', CURRENT_TIMESTAMP, '$start','0')");
		    										    $this->model_users->update(['user_rank_name'=>'Cadet','designation'=>'Cadet'],['user_id'=>$use_id]);
											
		    								}	
								
											
										}

											foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value3) {
		    												$this->commission_of_referal($value3->ref_id,$use_id,$amount,$invoice_no,$amount); //commison for referral
		    										
											}
								//	$this->rankfile(); //Update user ranks
									
									$this->check_rank_kams($use_id);
				
				
		}
		
		public function sendEmailToSender($amount,$userid,$email){

            	$from = 'info@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders | Card Payment <$from>" . "\r\n";
			 $msg = '<!doctype html>
            <html>
            <head>
                <meta charset="utf-8">
                <title>Account Credential</title>
                <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
                <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
            </head>
            <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
                <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
                    <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
                        <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
                            <img src="http://immtradersclub.com/images/logo.png" style="margin:0 0 0 0; height:100px ;  "><br/><br/>
                            <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
            line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
                               Fund Transfer Notification
                            </div>
                            <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
                                Dear iMM-Traders,
                            </div>
                            <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
                            </div>
                            <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
            color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
                                <p style="margin: 0px !important;">You have received an amount of  $'.$amount.' from '.$email.' with userid : '.$userid.'  . To see the details, login to your stripe account and check.</p>
                            </div>
                            <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
                                   <h4>Secure Login URL: http://immtradersclub.com/member/login </h4>
                                <h3>ACCOUNT NOTIFICATIONS</h3>
                                <p>To ensure that you receive all our notifications, we recommend that you give your valid email address and check your email on regular basis.</p>
                              </div>
                            <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
                              Copyrights 2016 Immtradersclub. All Rights Reserved. </p>
                        </div>
                    </div>
                </div>
                </div><br/><br/>
            </body>
            </html>';
			$subject = "Credit/Debit Payment Successful";
			mail ( 'immtradersclub@gmail.com', $subject, $msg, $headeruser1 );
	}
	
		/* Sponsor Commission Code Starts Here*/
	public function commission_of_referal1($ref,$useridss,$amount,$invoice_no,$packages){

		$spc = 0;
		$date=date('Y-m-d');

   		$this->db->cache_off();
		foreach ($this->model_users->select('*',['user_id'=>$ref]) as $key => $value) {
			
			if($value->user_rank_name=='Cadet')
			{
				$spc=3;
			}
			else if($value->user_rank_name=='Rising Star')
			{
				$spc=5;
			}
			else if($value->user_rank_name=='Flying Star')
			{
				$spc=7;
			}
			else if($value->user_rank_name=='Champion')
			{
				$spc=8;
			}
			else if($value->user_rank_name=='Elite')
			{
				$spc=10;
			}
			else if($value->user_rank_name=='Co Founder')
			{
				$spc=10;
			}
			else 
			{
				$spc=0;
			}

		}

		$amount = $amount ;
		$pb 	= $amount ;

		$withdrawal_commission	=	$spc*$amount/100;
		$rwallet 				= 	$withdrawal_commission;

		if($withdrawal_commission!='' && $withdrawal_commission!=0)
			{
					$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	
   					$this->model_credit_amt->query("update final_e_wallet set amount=(amount+$rwallet) where user_id='$ref'");
   						
   					$this->model_credit_amt->query("insert into credit_debit values(NULL,'$invoice_no','$ref','$rwallet','0','0','$ref','$useridss','$date','Referral Bonus','Earn Coin Referral Bonus from $useridss for $packages Package','Commission of MYR $rwallet For Package ".$amount." ','Referral Bonus','$invoice_no','Referral Bonus','0','Withdrawal Wallet',CURRENT_TIMESTAMP,'$urls')");

			}
			$this->commission_of_level1($useridss,$amount,$invoice_no,$packages);

	}
	/* Sponsor Commission Code Ends Here*/

	/* Sponsor Commission Code Starts Here*/

	public function  commission_of_level1($useridss,$amount,$invoice_no,$packages){

			$date =		date('Y-m-d');
			$spc  =		0;

			$this->db->cache_off();
			foreach ($this->model_matrix_downline_ref->query("select * from matrix_downline_ref where down_id='$useridss' and level<7")->result() as $key => $value) {
						$ref = $value->income_id;
						$level = $value->level;
				foreach ($this->model_users->select('user_rank_name',['user_id'=>$value->income_id]) as $key => $value1) {
									if($value1->user_rank_name=='Cadet' && $value->level==2)
									{
											$spc=2;
									}
									else if($value1->user_rank_name=='Cadet' && $value->level==3)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Rising Star' && $value->level==2)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Rising Star' && $value->level==3)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==2)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==3)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==4)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==2)
									{
										$spc=5;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==3)
									{
								 		$spc=3;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==4)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==5)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==2)
									{
										$spc=5;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==3)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==4)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==5)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==6)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==2)
									{
									 	$spc=5;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==3)
									{
										$spc=4;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==4)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==5)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==6)
									{
										$spc=1;
									}
									else 
									{
										$spc=0;
									}
	
					}
					$withdrawal_commission=$spc*$amount/100;
    
					$rwallet=$withdrawal_commission;

					if($withdrawal_commission!='' && $withdrawal_commission!=0)
					{
			
										$this->model_credit_amt->query("update final_e_wallet set amount=(amount+$rwallet) where user_id='$ref'");

		 								$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	    								
	    								$this->model_credit_amt->query("insert into credit_debit values(NULL,'$invoice_no','$ref','$rwallet','0','0','$ref','$useridss','$date','Earn Coin Unilevel Bonus','Earn Unilevel Bonus from $useridss for $packages Package','Commission of USD $rwallet For Package ".$amount." ','Unilevel $level Bonus','$invoice_no','Unilevel Bonus','0','Withdrawal Wallet',CURRENT_TIMESTAMP,'$urls')");
	    						 				//Rank_update($useridss);
					}
			}
	}

	/* Sponsor Commission Code Ends Here*/



	public function check_rank_kams($noty){


				    $user = "";
					$totsum=0;
					$totsum = 0 ;
					$rank  = "";
					$this->db->cache_off();
				 foreach ($this->model_lifejacket_subscription->query("select * from lifejacket_subscription where user_id='$noty'")->result() as $key => $value) {
				
				 		 $user = $value->user_id;

				 		 foreach ($this->model_lifejacket_subscription->query("select sum(amount) as newsum from lifejacket_subscription where user_id='$user' and status='Active'")->result() as $key => $value1) {
				 		 			$totsum=$value1->newsum;
				 		 }
				 		 foreach ($this->model_lifejacket_subscription->query("select * from user_registration where user_id='$user'")->result() as $key => $value2) {
				 		 			$rank=$value2->user_rank_name;
				 		 }


				 		 if($totsum>=75000 && ($rank=='Elite' || $rank=='Champion' || $rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
							{
       	 						
       	 						 $this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Co Founder', designation='Co Founder' where user_id='$user'");
       	 						 $this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Elite', 'Co Founder', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
       	 						
							}
						else if($totsum>=30000 && ($rank=='Champion' || $rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
							{

	  						

       	 						 $this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Elite', designation='Elite' where user_id='$user'");

	    						 $this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Champion', 'Elite', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");

							}
						else if($totsum>=15000 && ($rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
						{
         						
         						$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Champion', designation='Champion' where user_id='$user'");


         						$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Flying Star', 'Champion', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
          					
						}
						else if($totsum>=5000 && ($rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
						{
       								$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Flying Star', designation='Flying Star' where user_id='$user'");

       								$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Rising Star', 'Flying Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
         						
						}
						else if($totsum>=500 && ($rank=='Cadet' || $rank=='Normal User'))
						{
									
									$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Rising Star', designation='Rising Star' where user_id='$user'");


									$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Cadet', 'Rising Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");

	 								
						}
						else if($totsum>=10 && $rank=='Normal User')
						{
									

									$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Cadet', designation='Cadet' where user_id='$user'");

										$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Normal User', 'Cadet', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
	 								
						}
						else
						{
      
						} 

				}
	}

	public function buycoin($amount){



		$this->is_logged_in();

		$userid 	= $this->auth_user_id;
				
	//	$amount  	= $this->input->post('amount');
		$password 	= '';
		$coin_type 	= 'IMM';

		$ewa 		= 'final_reg_wallet';
		$walls		= "Withdrawal Reg Wallet";

		$rand 		= rand(0000000001,9000000000);
    	$start 		= date('Y-m-d');
    	$end 		= date('Y-m-d', strtotime('+12 months'));

    	$user_ewalletamt = 0 ;
    	$urls 		=	"http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

    	$data 		= [] ;


    	$invoice_no=rand(1111111111,9999999999);

		$this->db->cache_off();
		



			        	foreach ($this->model_final_reg_wallet->query("select * from $ewa where user_id='$userid'")->result() as $key => $value) {
							        	$user_ewalletamt=$value->amount;
				        }


							$pv				= 	1;
              				$walleting 		= 	'final_imm_coin_wallet';
              				$convertamt 	= 	$amount * 100 / 100;
              				$convertamt_add = 	0 ;
              				$com_amt 		=	0 ;
              				$this->db->cache_off();
              				
							foreach ($this->model_final_imm_coin_wallet->query("select * from imm_coin_percent where id=1")->result() as $key => $value1) {
										  $convertamt_add=$amount*$value1->percent/100;
							}

							$convertamt 	=	$convertamt + $convertamt_add;
             				$coins 			=	$convertamt / $pv;


             				  $lfid="LJ".$userid.$rand;

             				  $this->db->cache_off();
             				//  if($this->model_final_reg_wallet->query("update $ewa set amount=(amount-$amount) where user_id='$userid'")){


             				  		$this->model_final_reg_wallet->query("update $walleting set amount=(amount+$coins) where user_id='$userid'");

             				  		$this->model_final_reg_wallet->query("INSERT INTO `lifejacket_subscription_coin` (`id`, `user_id`, `package`, `amount`, `pay_type`, `pin_no`, `transaction_no`, `date`, `expire_date`, `remark`, `ts`, `status`, `invoice_no`,`lifejacket_id`,`username`,`sponsor`,`pb`) VALUES (NULL, '$userid', '$coins', '$amount', '$walls', '$password', '$rand', '$start', '$end', 'Coin Purchase', CURRENT_TIMESTAMP, 'Active', '$rand','$walleting','".$userid."','".$coin_type."','$pv')");

             				  		$this->model_final_reg_wallet->query("INSERT into credit_debit (`transaction_no`,`user_id`,`credit_amt`,`debit_amt`,`admin_charge`,`receiver_id`,`sender_id`,`receive_date`,`ttype`,`TranDescription`,`Cause`,`Remark`,`invoice_no`,`product_name`,`status`,`ewallet_used_by`,`current_url`) values('$rand','$userid','0','$amount','0','$userid','$userid','$start','Coin Purchase','Coin Purchase by $userid','Coin Purchase by $userid ','Coin Purchase $userid','$rand','Coin Purchase by $userid','0','$walls','$urls')");
             				  		$this->db->cache_off();
             				  		 foreach ($this->model_users->query("select * from user_registration where user_id='$userid'")->result() as $key => $value2) {
             				  		 		 	
             				  		 		 	if($value2->ref_id !=''){

             				  		 		 			$com_amt =	$amount *	100	/ 100;
             				  		 		 			$this->commission_of_referal1($value2->ref_id,$userid,$com_amt,$invoice_no,$amount);
             				  		 		 			$this->model_final_reg_wallet->query("INSERT INTO `lifejacket_subscription` (`id`, `user_id`, `package`, `amount`, `pay_type`, `pin_no`, `transaction_no`, `date`, `expire_date`, `remark`, `ts`, `status`, `invoice_no`,`lifejacket_id`,`username`,`sponsor`,`pb`) VALUES (NULL, '$userid', '$coins', '$amount', '$walls', '$password', '$rand', '$start', '$end', 'Coin Purchase', CURRENT_TIMESTAMP, 'Active', '$rand','$walleting','".$userid."','".$coin_type."','$pv')");
             				  		 		 			$this->check_rank_kams($userid);
             				  		 		 	}
             				  		 }
             				 // }
	}
	
		public function depositrwallet($amount){



		$this->is_logged_in();

		$userid 	= $this->auth_user_id;
				
	//	$amount  	= $this->input->post('amount');
		$password 	= '';
		$coin_type 	= 'IMM';

		$ewa 		= 'final_reg_wallet';
		$walls		= "Withdrawal Reg Wallet";

		$rand 		= rand(0000000001,9000000000);
    	$start 		= date('Y-m-d');
    	$end 		= date('Y-m-d', strtotime('+12 months'));

    	$user_ewalletamt = 0 ;
    	$urls 		=	"http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

    	$data 		= [] ;


    	$invoice_no=rand(1111111111,9999999999);

		$this->db->cache_off();
		



			        	foreach ($this->model_final_reg_wallet->query("select * from $ewa where user_id='$userid'")->result() as $key => $value) {
							        	$user_ewalletamt=$value->amount;
				        }


							$pv				= 	1;
              				$walleting 		= 	'final_imm_coin_wallet';
              				$convertamt 	= 	$amount * 100 / 100;
              				$convertamt_add = 	0 ;
              				$com_amt 		=	0 ;
              				$this->db->cache_off();
              				
							foreach ($this->model_final_imm_coin_wallet->query("select * from imm_coin_percent where id=1")->result() as $key => $value1) {
										  $convertamt_add=$amount*$value1->percent/100;
							}

							$convertamt 	=	$convertamt + $convertamt_add;
             				$coins 			=	$convertamt / $pv;


             				  $lfid="LJ".$userid.$rand;

             				  $this->db->cache_off();
             				 if($this->model_final_reg_wallet->query("update $ewa set amount=(amount+$amount) where user_id='$userid'")){

                                        $this->model_final_reg_wallet->query("INSERT into credit_debit (`transaction_no`,`user_id`,`credit_amt`,`debit_amt`,`admin_charge`,`receiver_id`,`sender_id`,`receive_date`,`ttype`,`TranDescription`,`Cause`,`Remark`,`invoice_no`,`product_name`,`status`,`ewallet_used_by`,`current_url`) values('$rand','$userid','$amount','0','0','$userid','123456','$start','Fund Transfer','CASH','Fund Credited by admin ','Fund Credited by admin','$rand','Fund Credited by admin','0','$walls','$urls')");
             				  	
             				  }
	}

}