<?php
defined('BASEPATH') or exit('No direct script access allowed');



class Profile  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');  
	


	}

	public function index() {

			if($this->is_logged_in() ) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				$rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
											'contact'		=> $value->telephone,
											'country'		=> $value->country,
											'acc_name'		=> $value->acc_name,
											'acc_number'	=> $value->ac_no,
											'bank_name'		=> $value->bank_nm,
											'branch_name'	=> $value->branch_nm,
											'swift_code'	=> $value->swift_code

										];
									
						
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
										

								];
				return $this->load->view('users/profile',$data);
			}

			else {


				redirect('login');
			}

	}

	public function getInfo() {

		$this->is_logged_in();

		$use_id = $this->auth_user_id;
		$this->db->cache_off();
		$data  = [

					'user_info' => $this->model_users->select('*',['user_id'=>$use_id])
				];
				
				echo json_encode($data);

	}

	public function getBankInfo() {

		$this->is_logged_in();
		$this->db->cache_off();
		$use_id = $this->auth_user_id;

		$data  = [
					'user_info' => $this->model_users->select('acc_name,ac_no,bank_nm,branch_nm,swift_code',['user_id'=>$use_id])
				];

				echo json_encode($data);

	}

	public function updateProfile() {

		$this->is_logged_in();

		//message to display on popup
		$msg = [];

		$use_id = $this->auth_user_id;
		   
        $passwd 			=   $this->input->post('passwd') ;        
        $oldpasswd 			=   $this->input->post('oldpasswd') ;     
        $confirmpasswd 		= 	$this->input->post('confirmpasswd') ;    
        $transpasswd 		=   $this->input->post('transpasswd') ;   
        $ctranspasswd 		= 	$this->input->post('ctranspasswd') ;     
        $userd 				=   $this->input->post('userd') ;  

        if($passwd !=$confirmpasswd) {

        		$msg = [
        				 'title' 		=>  'Oops !',
        				 'status' 		=>  'error',
        				 'msg'	  		=>  'Mismatched Password'		

        			   ];
        }
        else {
        		if($transpasswd != $ctranspasswd) {
        				$msg = [
        					 'title' 		=>  'Oops !',
	        				 'status' 		=>  'error',
	        				 'msg'	  		=>  'Mismatch Trsanction Password'		

        			   ];		
        		}
        		else {
        						//data to be updated in user_registartion table
        						$data = [

        								'first_name' 				=>   $this->input->post('fname') ,   
	        							'last_name' 				=>   $this->input->post('lname') ,         
	        							'email'						=>   $this->input->post('email') ,    
	        						    't_code' 					=>   $this->input->post('transpasswd') ,   
	        						    'address' 					=>   $this->input->post('address') ,       
	        						    'country' 					=>   $this->input->post('country') ,   
	         						    'state' 					=>   $this->input->post('state') ,   
	          							'city' 						=>   $this->input->post('city') ,   
	           						    'zipcode' 					=>   $this->input->post('zipcode') ,   
	          						    'ccode' 					=>   $this->input->post('countrycode') ,  
	        		 					'merried_status' 			=>   $this->input->post('status') ,
	        		 					'sex' 					 	=>   $this->input->post('gender') ,  
	        		 					'dob' 						=>   $this->input->post('birthdate') ,   
	        		 					'telephone' 				=>   $this->input->post('contact') ,   
        							 ];
        							 $this->db->cache_off();
        					   		if ($this->model_users->update($data,['id'=>$userd])){
        					   						$this->updatePass();
        					   					$msg = [
        					 								'title' 		=> 'Goode Job',
	        				 								'status' 		=> 'success',
	        				 								'msg'	  		=> 'Profile successfully updated'		
        			   								];	
        					   		}
        					   		else {

        					   			$msg = [
        					 						'title' 		=>  'Oops !',
	        				 						'status' 		=>  'error',
	        										 'msg'	  		=>  'Something went wrong'		

        			  								 ];	

        					   		}

        		}
        }



       echo json_encode($msg); 

	}



	public function updateBankInfo () {


			$acc_name 	 	= $this->input->post('acc_name');
			$acc_no   	 	= $this->input->post('acc_no');
			$bank_name   	= $this->input->post('bank_name');
			$branch_name    = $this->input->post('branch_name');
			$swift_code     = $this->input->post('swift_code');
			$userd 			= $this->input->post('userd') ;  
			$msg = [];

					$data = [
								'acc_name'  	=> $acc_name,
								'ac_no'			=> $acc_no,
								'bank_nm'		=> $bank_name,
								'branch_nm'	=> $branch_name,
								'swift_code'	=> $swift_code
        								
        					];
        					$this->db->cache_off();
        					if ($this->model_users->update($data,['id'=>$userd])){
        					   						
        					   					$msg = [
        					 								'title' 		=> 'Goode Job',
	        				 								'status' 		=> 'success',
	        				 								'msg'	  		=> 'Bank details successfully updated'		
        			   								];	
        					   		}
        					   		else {

        					   			$msg = [
        					 						'title' 		=>  'Oops !',
	        				 						'status' 		=>  'error',
	        										 'msg'	  		=>  'Something went wrong'		

        			  								 ];	

        					   		}


       			 echo json_encode($msg);



	}

	public function updateWalletInfo () {


			$btc 	 	= $this->input->post('btc');
			$eth   	 	= $this->input->post('eth');
			$etc   		= $this->input->post('etc');
			$xrp    	= $this->input->post('xrp');
			$dtag     	= $this->input->post('dtag');
			$userd 		= $this->input->post('userd') ;  
			$msg = [];

					$data = [
								'bitcoin'  		=> $btc,
								'ethereum'		=> $eth,
								'ethereumc'		=> $etc,
								'ripple'		=> $xrp,
								'ripple_tag'	=> $dtag
        								
        					];

        					$this->db->cache_off();

        					if ($this->model_users->update($data,['id'=>$userd])){
		
        					   					$msg = [
        					 								'title' 		=> 'Goode Job',
	        				 								'status' 		=> 'success',
	        				 								'msg'	  		=> 'Wallet successfully updated'		
        			   								];	
        					   		}
        					   		else {

        					   			$msg = [
        					 						'title' 		=>  'Oops !',
	        				 						'status' 		=>  'error',
	        										 'msg'	  		=>  'Something went wrong'		

        			  								 ];	

        					   		}


       			 echo json_encode($msg);



	}
 public function upload_photo() { 

      header('Content-Type: application/json');
      $config['upload_path']   = 'assets/photos/'; 
      $config['allowed_types'] = 'gif|jpg|png'; 
      $config['max_size']      = 1024;
      $use_id = $this->input->post('userid');


      $this->load->library('upload', $config);


		
      if ( ! $this->upload->do_upload('image')) {
         $error = array('error' => $this->upload->display_errors()); 
         echo json_encode($error);
      }else { 
         $data = $this->upload->data();
         $success = ['success'=>$data['file_name']];
         	$this->db->cache_off();
         	$this->model_users->update(['image'=>$data['file_name']],['id'=>$use_id]);

         echo json_encode($success);
      } 
   }

public function updatePass() {

		 $passwd 			=   $this->input->post('passwd') ;        
         $oldpasswd 		=   $this->input->post('oldpasswd') ;  
          $userd 			=   $this->input->post('userd') ;  

          $data = [];
         if($passwd ==$oldpasswd ) {

         }  
         else {

         	$this->model_users->update(['passwd'=>$this->authentication->hash_passwd($passwd)],['id'=>$userd]);
         }
         

        
}
 

}