<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  MyWallet  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_transfer_withdraw_history');

       

	}

	public function index() {
			if($this->is_logged_in()) {
				
				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		        $this->db->cache_off();
		        $rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
											't_code'		=> 	$value->t_code,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									
									'rwallet_balance' 	=> $this->model_final_reg_wallet->select('amount',['user_id'=>$use_id]),
										
								];
				return $this->load->view('users/rrwallet',$data);
			}
			else {
				redirect('login');
			}

	}
	
	public function transferFund() {
	    
	    	$this->is_logged_in();
	    	$use_id 	= $this->auth_user_id;
	    	
	    	$amount     = $this->input->post('amount');
	    	$password   = $this->input->post('password');
	    	
	    	$transcode  = "" ;
	    	
	    	$sender_rwallet = 0;
	    	$sendername = "" ;
	    	$senderemail ="";
	    	
	    	
	    	
	    	$reciever_id = "";
	    	$recieve = $this->input->post('receiver');
	    	$receivername= "";
	    	$receiveremail = "";
	    	$receiver_rwallet = 0;
	    	
	    	
	    	
	    	$rand   =   rand(0000000001,9999999999);
            $date 	=   date("Y-m-d");
            $ttype  =   "Fund Transfer";
        	$msg    =   'Withdrawal Wallet';
          
        	$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	    	
	    	foreach($this->model_users->query("SELECT user_id FROM user_registration where user_id='".$recieve."' OR username='".$recieve."' OR email='".$recieve."'")->result() as $key => $ri) {
	    	        $reciever_id = $ri->user_id;
	    	}
	    	
	    
	    	//sender
	    	foreach($this->model_users->select('username,first_name,last_name,t_code',['user_id'=> $use_id]) as $key => $sender) {
	    	    $sendername = $sender->first_name . " " . $sender->last_name;
	    	    $senderemail = $sender->username;
	    	    $transcode = $sender->t_code;
	    	}
	    	foreach($this->model_final_reg_wallet->select('*',['user_id'=>$use_id]) as $key => $senderwallet) {
	    	        $sender_rwallet = $senderwallet->amount;
	    	    
	    	}
	    	//reciever
	    	foreach($this->model_users->select('username,first_name,last_name',['user_id'=> $reciever_id]) as $key => $receiver) {
	    	    $receivername = $receiver->first_name . " " . $receiver->last_name;
	    	    $receiveremail = $receiver->username;
	    	}
	    	foreach($this->model_final_reg_wallet->select('*',['user_id'=>$reciever_id]) as $key => $recieverwaller) {
	    	        	 $receiver_rwallet = 	$recieverwaller->amount;
	    	    
	    	}
	    	
	    		
	
	    	
	    	
	    	if( $amount >= 10) {
	    	    
	    	    if($password == $transcode) {
	    	        
	    	            	$receiver_data   = [
							        						'user_id'			=> $reciever_id,
							        						'receiver_id'		=> $reciever_id,
							        						'sender_id'			=> $use_id,
							        						'credit_amt'		=> $amount,
							        						'admin_charge'		=> '0',
							        						'receive_date'		=> $date ,
							        						'TranDescription'	=> 'Transfer fund by '.$sendername.' to '.$receivername,
							        						'Remark'			=> 'Transfer fund by '.$sendername.' to '.$receivername,
							        						'Cause'				=> 'Transfer fund by '.$sendername.' to '.$receivername,
							        						'transaction_no'	=> $rand,
							        						'invoice_no'		=> $rand,
							        						'ttype'				=> $ttype,
							        						'product_name'		=> $ttype,
							        						'ewallet_used_by'	=> $msg,
							        						'current_url'		=> $urls,
							        						'debit_amt'			=> '0',
							        						'status'			=> '0'

						        					];
							        $sender_data   = [
								        						'user_id'			=> $use_id,
								        						'debit_amt'			=> $amount,
								        						'receiver_id'		=> $reciever_id,
							        						    'sender_id'			=> $use_id,
								        						'admin_charge'		=> '0',
								        						'receive_date'		=> $date,
								        						'TranDescription'	=> 'Transfer fund $'.$amount.' to '.$receivername.'with 0% service charge',
								        						'Remark'			=> 'Transfer fund $'.$amount.' to '.$receivername.'with 0% service charge',
								        						'Cause'				=> 'Transfer fund by '.$sendername.' to '.$receivername,
								        						'transaction_no'	=> $rand,
								        						'invoice_no'		=> $rand,
								        						'ttype'				=> $ttype,
								        						'product_name'		=> $ttype,
								        						'ewallet_used_by'	=> $msg,
								        						'current_url'		=> $urls,
								        						'credit_amt'		=> '0',
								        						'status'			=> '0'
							        				];
							        				
							        			
							        			
							        if($amount > $sender_rwallet) {
							            
							            $this->session->set_flashdata('msg', " Failed to transfer !! .Youre trying to transfer $".$amount." to ".$receivername ." ".$reciever_id ." but you only have $" .$sender_rwallet ." in your wallet");
	    	        
	    	                             return redirect('wallet/user-rwallet','refresh');
							            
							        }	
							        else {
							            
							              
							               
							               
        									 
        									  if(!empty($reciever_id)) {
        									      
        									       $sender_balance  = $sender_rwallet  - $amount ;
							        	           $this->model_credit_amt->insert($sender_data);
							        	           $this->model_final_reg_wallet->update(['amount'=>$sender_balance],['user_id'=>$use_id]);
        								
                								    $receiver_balance = $receiver_rwallet + $amount ;
                								    $this->model_credit_amt->insert($receiver_data);
                									$this->model_final_reg_wallet->update(['amount'=>$receiver_balance],['user_id'=>$reciever_id]);
                									 
        	    	                                $this->session->set_flashdata('msg', "You have successfully transferred ".$amount." to ".$receivername ." ".$reciever_id);
	    	        
	    	                                        return redirect('wallet/user-rwallet','refresh');
							                    }
							               
							                     else {
							                          
							                          $this->session->set_flashdata('msg', "Something wen wrong ! Please try again");
	    	                                          return redirect('wallet/user-rwallet','refresh');
							                   
							                    }
        								// 	 $this->sendEmailToSender($sendername,$senderemail,$amount,$receivername,$reciever_id);
        								// 	 $this->sendEmailToReceiever($receivername,$receiveremail,$amount,$sendername,$use_id);
	    	        
	    	        
	    	                                $this->session->set_flashdata('msg', "You have successfully transferred ".$amount." to ".$receivername ." ".$reciever_id);
	    	        
	    	                                 return redirect('wallet/user-rwallet','refresh');
							            
							        }
							        	   
	    	        
	    	    }
	    	    else {
	    	        
	    	             $this->session->set_flashdata('msg', 'Wrong Transaction Password');
	    	             //this->session->keep_flashdata('msg', 'Wrong Transaction Password'); 
	    	        
	    	             return redirect('wallet/user-rwallet','refresh');
	    	    }
	    	    
	    	    
	    	}
	    	
	    	else {
	    	    
	    	        $this->session->set_flashdata('msg', 'Amount must be 10 dollar');
	    	        
	    	       return redirect('wallet/user-rwallet','refresh');
	    	    
	    	}
	    
	    	
	    	
	    	
	       
	}
	
	
	
	public function sendEmailToSender($usrename,$email,$amount,$receiver_name,$userid){


            	$from = 'info@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders | Fund Transfer <$from>" . "\r\n";
			 $msg = '<!doctype html>
            <html>
            <head>
                <meta charset="utf-8">
                <title>Account Credential</title>
                <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
                <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
            </head>
            <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
                <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
                    <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
                        <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
                            <img src="http://immtradersclub.com/images/logo.png" style="margin:0 0 0 0; height:100px ;  "><br/><br/>
                            <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
            line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
                               Fund Transfer Notification
                            </div>
                            <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
                                Dear '.$usrename.',
                            </div>
                            <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
                            </div>
                            <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
            color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
                                <p style="margin: 0px !important;">You have transferred an amount of  $'.$amount.' to '.$receiver_name.' with userid : '.$userid.'  from your IMM account . To see the details, login to your account and check.</p>
                            </div>
                            <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
                                   <h4>Secure Login URL: http://immtradersclub.com/member/login </h4>
                                <h3>ACCOUNT NOTIFICATIONS</h3>
                                <p>To ensure that you receive all our notifications, we recommend that you give your valid email address and check your email on regular basis.</p>
                              </div>
                            <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
                              Copyrights 2016 Immtradersclub. All Rights Reserved. </p>
                        </div>
                    </div>
                </div>
                </div><br/><br/>
            </body>
            </html>';

				$subject = "Fund Transfer";
				mail ( $email, $subject, $msg, $headeruser1 );
	}

	public function sendEmailToReceiever($usrename,$email,$amount,$fromsender,$userid){
            
            	$from = 'info@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders | Fund Transfer <$from>" . "\r\n";
				 $msg = '<!doctype html>
		          <html>
		          <head>
		              <meta charset="utf-8">
		              <title>Account Credential</title>
		              <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
		              <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
		          </head>
		          <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
		              <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
		                  <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
		                      <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
		                          <img src="http://immtradersclub.com/images/logo.png" style="margin:0 0 20px 0;  "><br/><br/>
		                          <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
		          line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
		                             Fund Transfer Notification
		                          </div>
		                          <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
		                              Dear '.$usrename.',
		                          </div>
		                          <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
		                          </div>
		                          <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
		          color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
		                              <p style="margin: 0px !important;">'.$fromsender.' with userid : '.$userid.'  has transferred  $'.$amount.' to your IMM account. To see the details, login to your account and check.</p>
		                          </div>
		                          <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
		                                 <h4>Secure Login URL: https://immtradersclub.com/member/login </h4>
		                              <h3>ACCOUNT NOTIFICATIONS</h3>
		                              <p>To ensure that you receive all our notifications, we recommend that you give your valid email address and check your email on regular basis.</p>
		                            </div>
		                          <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
		                            Copyrights 2018 Immtradersclub. All Rights Reserved. </p>
		                      </div>
		                  </div>
		              </div>
		              </div><br/><br/>
		          </body>
		          </html>';

					$subject = "Fund Transfer";
					
				    mail ( $email, $subject, $msg, $headeruser1 );

	}

	
}