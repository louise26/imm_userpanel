<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'application/libraries/coinspayment.inc.php';

class  Depositfund  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		//$this->load->library('CoinPaymentsAPI','coinspayment');


		$this->load->model('model_add_fund_request');
    
        
	}

	
		public function index(){
                


			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;



				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				$rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,

											'id'			=>  $value->id,

										];
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
										

								];
					return $this->load->view('users/depositfund',$data);

			}
			else {

				redirect('login');
			}
		}



		public function deposit() {

				$this->is_logged_in();
				$user_id =$this->auth_user_id;


				$amount 	= $this->input->post('amount');
				$type		= $this->input->post('type');
			
				$username 	= $this->input->post('username');
				$ttype		= 'rwallet';

				$data 		= [] ;

			


				$cps = new CoinPaymentsAPI();
                    
				$cps->Setup('f2637c9d95A745B65CA080F6481788e28CCc10707ED9e0c1bac82D534c75236e', 'e23ad9d213e8bb0966b6a4028461fe7ea9548604a76805cf62acbe8cb320830a');



				if($type =="Bank Transfer"){
								
								//$this->model_credit_amt->query("INSERT INTO add_fund_request values(NULL,'$user_id','$amount','$type','$ttype','Pending','','','".date('Y-m-d')."','','')");
						$this->db->cache_off();
								if($this->model_add_fund_request->insert([
																		'user_id'		=>	$user_id,
																		'amount'		=>	$amount,
																		'paymode'		=> $type,
																		'wallet_type'	=> $ttype,
																		'status'		=> 'Pending',
																		'posted_date'	=>	date('Y-m-d')

																		])) {


											array_push($data , [
														'msg'		=> 'success',
														'txn_id' 	=> '',
														'amount' 	=> $amount,
														'address' 	=> '',
														'img_url' 	=> '',
														'typ' 		=> $type,
														'dest_tag'	=> '',
													]);	

								}
								else {
										array_push($data,[
														'msg'		=> 'Something went wrong',
														'txn_id' 	=> '',
														'amount' 	=> '',
														'address' 	=> '',
														'img_url' 	=> '',
														'typ' 		=> '',
														'dest_tag'	=> '',
													]);	
								}
								
				}

				else {
						
							$result = $cps->CreateTransactionSimple($amount,'USD',$type,'','https://immtradersclub.com/',$username);

							if($result['error'] == 'ok'){

								$rcpt= '<a href="' . $result['result']['status_url'].'" target="_blank">'.$result['result']['txn_id'].'</a>';
								$this->db->cache_off();
								$this->model_add_fund_request->insert([

																		'user_id'				=> $user_id,
																		'amount'				=> $amount,
																		'paymode'				=> $type,
																		'wallet_type'			=> $ttype,
																		'status'				=> 'Waiting for approval',
																		'reciept_no'			=> 	htmlspecialchars($rcpt),
																		'reciept_update_date'	=> 	date('Y-m-d'),
																		'posted_date'			=> 	date('Y-m-d')


																		]);
								

								if($type !="XRP") {
											array_push($data ,[
														'msg'		=> 'success',
														'txn_id' 	=> $result['result']['txn_id'],
														'amount' 	=> $result['result']['amount'],
														'address' 	=> $result['result']['address'],
														'img_url' 	=> $result['result']['qrcode_url'],
														'typ' 		=> $type,
														'dest_tag'	=>  ''
														
													]);	

								}
								else {

										array_push($data ,[
														'msg'		=> 'success',
														'txn_id' 	=> $result['result']['txn_id'],
														'amount' 	=> $result['result']['amount'],
														'address' 	=> $result['result']['address'],
														'img_url' 	=> $result['result']['qrcode_url'],
														'typ' 		=> $type,
														'dest_tag'	=> $result['result']['dest_tag'],
													]);	

								}
								
							}
							else {

									array_push($data,[
														'msg'		=> $result['error'],
														'txn_id' 	=> '',
														'amount' 	=> '',
														'address' 	=> '',
														'img_url' 	=> '',
														'typ' 		=> '',
														'dest_tag'	=> '',
													]);	

							}
				}


					echo  json_encode($data,JSON_UNESCAPED_SLASHES);

		}
		
		public function depositViaBTC() {



			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;



				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,

											'id'			=>  $value->id,

										];
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
										

								];
					return $this->load->view('users/btcdeposit',$data);

			}
			else {

				redirect('login');
			}


		}


		public function proceedPayment() {

			if($this->is_logged_in()){

				$use_id 	=		$this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
				$email =  "";
				

				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {
							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
										
										$email = $value->email;
								
						}

						$address = "" ;
                        
					
						//$my_xpub 		    = "xpub6DKZ7xbwVC1Lg4QM9EGVpCvx58o16VrWFuzhDstTAzbjY7hKTqVk2JheBBqPcsCR9JhJTJoqgcmrKPkgHBhK8P15KJKRBdmd18ZrG8HRwGR"; company xpub | ambot sayo
						$my_xpub ="xpub6BnvEDqyN5A6PbxkaHPajN9X63Ac4HWfvi4u8yztZcchqQXLSJ7YdR7Bv7vmJgop4rhJeeTSS2nATVuftpPXYh2UxrSmBVWJMqdpfh3hAeu";
						$my_api_key 	    = "3739630b-0c66-4dd9-b245-280779d9afa2";

						$price_in_usd  		= $this->input->post('amount');
						$amount 			= $this->input->post('amount');
						$price_in_btc       = file_get_contents('https://blockchain.info/' . "tobtc?currency=USD&value=" . $price_in_usd);
			     		$callback_url		=  "https://immtradersclub.com/member/account/payment-notif/".$use_id."-".$email."/".$price_in_btc;

						$resp 				= file_get_contents("https://api.blockchain.info/v2/receive?key=" . $my_api_key . "&callback=" . urlencode($callback_url) . "&xpub=" .$my_xpub);
						$response 			= json_decode($resp);

						

						$this->model_add_fund_request->insert(['user_id' 				=> $use_id,
															   'amount' 				=> $amount,
															   'paymode'				=> 'BTC',
																'wallet_type'			=> 'rwallet',
																'status'				=> 'Waiting for approval',
																'reciept_no'			=>  $response->address,
																'reciept_update_date'	=> 	date('Y-m-d'),
																'posted_date'			=> 	date('Y-m-d')
															  ]);
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									'amount'  => $price_in_btc,
									'address' => $response->address,
									'email'   => $email
								];
					return $this->load->view('users/pay',$data);

			}
			else {

				redirect('login');
			}


		}


		public function notifyPayment() {

				$user_id = $this->uri->segment(3);
				$amount = $this->uri->segment(4);

				$date = date('F d, Y');
				$from = 'support@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders <$from>" . "\r\n";

				$msg = '<!doctype html>
              <html>
              <head>
                  <meta charset="utf-8">
                  <title>BTC PAYMENT</title>
                  <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
                  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
              </head>
              <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
                  <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
                      <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
                          <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
                              <img src="https://immtradersclub.com/images/logo1.png" height="70"><br/><br/>
                              <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
              line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
                                 	PAYMENT NOTIFICATION
                              </div>
                              <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
                                  Hello  iMM-Traders 
                              </div>
                              <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
                              </div>
                              <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
              color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
                                  <p style="margin: 0px !important;">Bitcoin payment has been successfully submitted by USER : '.$user_id.' with an amount of '.$amount.'BTC made on '.$date.'</p>
                              </div>
                              <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
                               
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"></h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"></h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"></h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"></h3>
                                 
                              </div>
                              <div class="ttl" style="margin: 14px 0px 0px 10px;width: 500px;font-family: Lato;font-weight: 400;color: rgb(255, 255, 255);font-size: 15px;line-height: 35px;padding: 6px 0px;background-color: rgb(230, 67, 60);">
                               
                              </div>
                              <div class="line" style="height: 1px;background: rgb(218, 218, 218) none repeat scroll 0% 0%;margin-top: 20px;">
                              </div>
                              <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
                                We wish you a most profitable and enjoyable trading experience.<br><center>Customer Support Team</center> </p>
                          </div>
                      </div>
                  </div>
                  </div><br/><br/>
              </body>
              </html>';

                 $subject = "Bitcoin Payment";

		           mail ( 'immtradersclub@gmail.com', $subject, $msg, $headeruser1 );


		}
		
		public function sendPaymentNotification(){
		       $user_id = $this->input->post('user_id');
			   $amount = $this->input->post('amount');
			   $address = $this->input->post('address');
               $email  = $this->input->post('email');
				$date = date('F d, Y');
				$from = 'info@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders Club <$from>" . "\r\n";

				$msg = '<!doctype html>
              <html>
              <head>
                  <meta charset="utf-8">
                  <title>BTC PAYMENT</title>
                  <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
                  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
              </head>
              <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
                  <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
                      <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
                          <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
                              <img src="https://immtradersclub.com/images/logo1.png" height="70"><br/><br/>
                              <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
              line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
                                 	DEPOSIT FUND  NOTIFICATION
                              </div>
                              <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
                                  Hello  iMM-Traders 
                              </div>
                              <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
                              </div>
                              <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
              color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
                                  <p style="margin: 0px !important;">Bitcoin payment has been successfully submitted by USER : '.$user_id.'-'.$email.' with an amount of '.$amount.'BTC made on '.$date.', through our generated address : '.$address.'</p>
                              </div>
                              <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
                               
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px">Please check your wallet to confirm the transaction</h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"></h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"> </h3>
                                  <h3 style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"> </h3>
                                 
                              </div>
                              <div class="ttl" style="margin: 14px 0px 0px 10px;width: 500px;font-family: Lato;font-weight: 400;color: rgb(255, 255, 255);font-size: 15px;line-height: 35px;padding: 6px 0px;background-color: rgb(230, 67, 60);">
                               
                              </div>
                              <div class="line" style="height: 1px;background: rgb(218, 218, 218) none repeat scroll 0% 0%;margin-top: 20px;">
                              </div>
                              <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
                                We wish you a most profitable and enjoyable trading experience.<br><center>Customer Support Team</center> </p>
                          </div>
                      </div>
                  </div>
                  </div><br/><br/>
              </body>
              </html>';
                $subject = "Bitcoin Payment";


		          if( mail ( 'immtradersclub@gmail.com', $subject, $msg, $headeruser1 )) {
		              
		              echo json_encode(['msg'=>'success']);
		              
		          }
		          else {
		                 echo json_encode(['msg'=>'fail']);
		          }


		

		}

}