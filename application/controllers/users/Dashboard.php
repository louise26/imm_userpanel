<?php
defined('BASEPATH') or exit('No direct script access allowed');



class Dashboard  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		

	}

	public function index () {
			
				if($this->is_logged_in() ) {
					$use_id =$this->auth_user_id;
					$userinfo = [];
					$data =[];
				
					//$this->db->cache_off();
					$rankachieve = "" ;
					

             
                
				foreach ($this->model_users->query("SELECT qualify_date FROM rank_achiever where user_id='$use_id'")->result() as $key => $value) {
							$rankachieve = $value->qualify_date;
				}
				$rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				$this->session->set_userdata(['qualify_date'=>$rankachieve]);
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->email,
											'rank'			=> 	$rankss,
											'image_name' 	=> $value->image,
											'fname'			=> $value->first_name,
											'lname'			=> $value->last_name,
										];
						}
						
				
								$data = [
											'user_id' => $this->auth_user_id,
											'info'	  => $userinfo,
											'latest_referral'  => $this->model_users->query("select first_name,last_name,user_id,ts,registration_date from user_registration where ref_id='".$use_id."' order by id desc limit 0,5")->result(),
											'top_history'       => $this->model_users->query("select ttype,receive_date from credit_debit where user_id='".$use_id."' order by id desc limit 0,5")->result(),
											'total_referral' 	=> $this->model_matrix_downline_ref->count_ref(['income_id'=>$use_id,'level'=>1]),
											'total_downline' 	=> $this->model_matrix_downline_ref->count_ref(['income_id'=>$use_id]),
											'ewallet_balance' 	=> $this->model_final_e_wallet->select('amount',['user_id'=>$use_id]),
											'rwallet_balance' 	=> $this->model_final_reg_wallet->select('amount',['user_id'=>$use_id]),
											'total_earnings'	 => $this->model_credit_amt->query("SELECT SUM(credit_amt) as total,ttype  from credit_debit where user_id='$use_id'  GROUP BY ttype")->result(),
											'immcoin' 	=> $this->model_final_imm_coin_wallet->select('amount',['user_id'=>$use_id]),
											'etc' 	=> $this->model_final_bitcoin_wallet->select('amount',['user_id'=>$use_id]),
											'eth' 	=> $this->model_final_ethereum_wallet->select('amount',['user_id'=>$use_id]),
											'ltc' 	=> $this->model_final_ethereum_classic_wallet->select('amount',['user_id'=>$use_id]),
											'xrp' 	=> $this->model_final_ripple_wallet->select('amount',['user_id'=>$use_id]),
											'announcement' 	=> $this->model_credit_amt->query("SELECT * from promo")->result(),
											
										];
										
					return $this->load->view('users/dashboard',$data);
				}
				else {

					redirect('login');
				}
	}

	public function retrieve_user_details() {

			$this->is_logged_in();
			$use_id =$this->auth_user_id;
			$this->db->trans_start();

			$data = [] ;
				
				$data = [
							'total_referral' 	=> $this->model_matrix_downline_ref->count_ref(['income_id'=>$use_id,'level'=>1]),
							'total_downline' 	=> $this->model_matrix_downline_ref->count_ref(['income_id'=>$use_id]),
							'ewallet_balance' 	=> $this->model_final_e_wallet->select('amount',['user_id'=>$use_id]),
							'rwallet_balance' 	=> $this->model_final_reg_wallet->select('amount',['user_id'=>$use_id]),
					];
					$this->db->trans_complete();
					$this->db->close();
			echo json_encode($data);
	}


	public function retrieve_downline_paymentstatus() {
		    $this->is_logged_in();
			$use_id =$this->auth_user_id;

			 $paid 		= 0;
			 $unpaid 	= 0 ;
		
			 $data = [] ;
		
						foreach ($this->model_matrix_downline_ref->select('*',['income_id'=>$use_id]) as $key => $value) {
							
									if($this->model_lifejacket_subscription->count_ref(['user_id'=>$value->down_id]) > 0) {
											$paid +=1;
										}
										else {
											$unpaid +=1;

										}	
							}
						$data = [
								'paid_downline' 	=> $paid,
								'unpaid_downline'	=> $unpaid
						];
	
			echo json_encode($data);
	}

	public function total_income() {
		$this->is_logged_in();
		$use_id =$this->auth_user_id;
		$data = [] ;
	
			$data = [
						'total_income' => $this->model_credit_amt->query("SELECT sum(credit_amt) as total from credit_debit where user_id='$use_id' and ttype <> 'Fund Transfer' and (ttype='Generation Income' OR ttype='Referral Bonus' OR ttype='Unilevel Bonus' OR ttype='Matrix Bonus' OR ttype='Profit Sharing Bonus' OR ttype='Generation Bonus' OR ttype='Royalty Bonus')")->result(),
						];
		echo json_encode($data);

	}

	public function earnings() {

		$this->is_logged_in();
		$use_id =$this->auth_user_id;

		$total_profit = 0 ;
		$total_royalty = 0;
		$total_level = 0 ;
		$total_referral = 0;


		foreach ($this->model_credit_amt->query("SELECT SUM(credit_amt) as total_profit  from credit_debit where user_id='$use_id'  and ttype='Profit Sharing Bonus'")->result() as $key => $value) {
					$total_profit = $total_profit + $value->total_profit;
		}
		foreach ( $this->model_credit_amt->query("SELECT SUM(credit_amt) as total_royalty  from credit_debit where user_id='$use_id'  and ttype='Royalty Bonus'")->result() as $key => $value) {
					$total_royalty = $total_royalty + $value->total_royalty;
		}
		foreach (  $this->model_credit_amt->query("SELECT SUM(credit_amt) as total_level  from credit_debit where user_id='$use_id'  and ttype='Unilevel Bonus'")->result() as $key => $value) {
					$total_level = $total_level  + $value->total_level;
		}
		foreach (  $this->model_credit_amt->query("SELECT SUM(credit_amt) as total_referral  from credit_debit where user_id='$use_id'  and ttype='Referral Bonus'")->result() as $key => $value) {
					$total_referral = $total_referral + $value->total_referral;
		}

								$data = [
											'profit' => $total_profit,
											'royalty' =>$total_royalty,
											'referral' =>$total_referral,
											 'level'=>$total_level,
										];
			echo json_encode($data);
	}


	public function getNewRegistration(){


				$this->is_logged_in();
				$use_id = $this->auth_user_id;

		
					    
					      $date = date('Y-m-d');
			$newdate = date('Y-m-d',strtotime('-1 day', strtotime($date)));
						$data = [
									'new_users'	=> $this->model_users->query("select * from user_registration where ref_id='".$use_id."' and registration_date='".$newdate."' order by id desc limit 0,10")->result(),
								];
				echo json_encode($data);
	}

    public function getDebited(){
			$this->is_logged_in();
			$use_id = $this->auth_user_id;
 //strtotime('-1 day', strtotime($date_raw)))
		    $date = date('Y-m-d');
			$newdate = date('Y-m-d',strtotime('-1 day', strtotime($date)));
											
		//	$this->db->cache_off();

						$data = [
									'debited'	=> $this->model_users->query("select  * from credit_debit where user_id='".$use_id."' and receive_date='".$newdate."' and debit_amt>0 order by id desc limit 0,5")->result(),
								];
								echo json_encode($data);
	}

	public function getCredited(){
	    
			$this->is_logged_in();
			$use_id = $this->auth_user_id;
		//	$date = date('d') -1;
		//	$newdate = date('Y-m-'.$date);		
		  $date = date('Y-m-d');
			$newdate = date('Y-m-d',strtotime('-1 day', strtotime($date)));
			
		//	$this->db->cache_off();
						$data = [
									
									'credited'	=> $this->model_users->query("select * from credit_debit where user_id='".$use_id."' and receive_date='".$newdate."' and credit_amt>0 order by id desc limit 0,10")->result(),
								];



								echo json_encode($data);
	}
	public function calendar(){
		$events = array();
			//	$this->db->cache_off();
			    foreach ($this->model_users->query("SELECT * FROM calendar")->result() as $key => $value) {

			    	$e = array();
					     $e['title'] =$value->title;
					     $e['start'] =$value->start;
					     $e['end'] =$value->end;
					     $allday = ($value->allDay == "true") ? true : false;
					     $e['allDay'] = $allday;
			     		array_push($events, $e);
			    	
			    }
			   
			     
			    
    echo json_encode($events);
	}


	public function updateRanks($use_id) {
	    
	    	    $data = array();
                 $this->is_logged_in();
			   	//$use_id =	$this->auth_user_id;
			   	//$use_id = 'IMM1201657' ;
			   	$stat 	=  "";
			   	$sender =  "";
			   	$transd =  "";
			   	$name 	= "";
			   	$sendid = "";
			   	$total = 0 ;

			   	$start 	=	date('Y-m-').'01';
                $end 	=	date('Y-m-').'31';

                $start1 	=	'2017-11-01';
                $end1 		=   '2017-11-31';
                
                $downline_total = 0;
	
                     	foreach ($this->model_credit_amt->query("select * from user_registration where ref_id='$use_id'")->result() as $key => $value1) {
                     				
                     				$i +=1;
									$t1 		= 0; 
									$totaffl 	= 0;
								    $te1 		= 0;
								    $te_nov1 	= 0;
								    $te_nov123 	= 0;
                     				
                     				foreach ($this->model_credit_amt->query("select * from matrix_downline_ref where income_id='".$value1->user_id."'")->result() as $key => $value2) {
                     							
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1 from lifejacket_subscription where user_id='".$value2->down_id."'")->result() as $key => $value3) {

                     								$t 	 = $value3->tot1;
													$t1 += $t;

                     							}
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1 from lifejacket_subscription where user_id='".$value2->down_id."' and (date between '$start' and '$end')")->result() as $key => $value3) {
                     								 $te 	 = $value3->tot1;
													 $te1 	+= $te;
                     							}
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1_nov from lifejacket_subscription where user_id='".$value2->down_id."' and (date between '$start1' and '$end1')")->result() as $key => $value3) {
                  
                     									$te_nov=$value3->tot1_nov;
														$te_nov1+=$te_nov;

                     							}

                     				}
                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot from lifejacket_subscription where user_id='".$value1->user_id."'")->result() as $key => $c1) {
                     							 $total= $t1+$c1->tot;
                     				}

                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot from lifejacket_subscription where user_id='".$value1->user_id."' and (date between '$start' and '$end')")->result() as $key => $crr1) {
                     							 $totaffl= $te1+$crr1->tot;

                     				}
                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot_no from lifejacket_subscription where user_id='".$value1->user_id."' and (date between '$start1' and '$end1')")->result() as $key => $crr1_nov) {
                     						 $te_nov123= $te_nov1+$crr1_nov->tot_no;
                     				}
                     				
                     					$downline_total +=$total;
                     				    //echo  '$ ' . number_format($total,2) .'<br>';
                     		}
					   	
	           	$this->rankAchieve($downline_total,$use_id);
	    
	}
	
	public function updateRank() {
	    
	        foreach($this->model_credit_amt->query("Select sum(amount) as total from lifejacket_subscription where user_id='IMM1471394' ")->result() as $key => $val) {
	            
	              echo $val->total;
	        }
	}
	
	public function rankAchieve($totsum,$user) {
	    
	    
	            $rank = '' ;
	            
	            $existing = FALSE ;
	            
	            
	           
	            
	            foreach($this->model_users->select('user_rank_name',['user_id'=> $user]) as $key => $value){
	                $rank = $value->user_rank_name;
	            }
	            
	            
	             foreach($this->model_users->query("SELECT count(user_id) as usr from rank_achiever WHERE last_rank='". $rank ."' && user_id='$user'  ")->result() as $key => $value11) {
	                
	                        if($value11->usr == 0 ) {
	                             $existing = FALSE ;
	                        }
	                        
	                        else if($value11->usr > 0) {
	                            $existing = TRUE ;
	                        }
	            }
	            
        	   if($totsum>=1500000 )
        	{
               	  //mysql_query("update user_registration set user_rank_name='Co Founder', designation='Co Founder' where user_id='$user'");
               	  
               	  $this->model_users->query("update user_registration set user_rank_name='Co Founder', designation='Co Founder' where user_id='$user'");
        
               	 // mysql_query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Elite', 'Co Founder', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
               	 if($existing) {
               	     $this->model_users->query("INSERT INTO rank_achiever (id, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Co Founder', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
               	 }
               	  
        
        	}
        	else if($totsum>=500000)
        	{
        
        	    // mysql_query("update user_registration set user_rank_name='Elite', designation='Elite' where user_id='$user'");
        	   $this->model_users->query("update user_registration set user_rank_name='Elite', designation='Elite' where user_id='$user'");
        	   // mysql_query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Champion', 'Elite', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
        	   	 if($existing) {
        	            $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Elite', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
        	   	 }
        
        	}
        	else if($totsum>=100000)
        	{
                   $this->model_users->query("update user_registration set user_rank_name='Champion', designation='Champion' where user_id='$user'");
                if($existing) {
                   $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Champion', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
                }
                
        	}
        	else if($totsum>=35000)
        	{
              
                  $this->model_users->query("update user_registration set user_rank_name='Flying Star', designation='Flying Star' where user_id='$user'");
                  	 if($existing) {
                         $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Flying Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
                  	 }
        	}
        	else if($totsum>=5000 )
        	{
    
        	     $this->model_users->query("update user_registration set user_rank_name='Rising Star', designation='Rising Star' where user_id='$user'");
                 if($existing) {
                    $this->model_users->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', '$rank', 'Rising Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
                 }
        	}

        	else
        	{
              
        	} 
        	
	}
	
	
	public function check_rank_kams($noty){


				$user = "";
					$totsum=0;
					$totsum = 0 ;
					$rank  = "";
					$this->db->cache_off();
				 foreach ($this->model_lifejacket_subscription->query("select * from lifejacket_subscription where user_id='$noty'")->result() as $key => $value) {
				
				 		 $user = $value->user_id;

				 		 foreach ($this->model_lifejacket_subscription->query("select sum(amount) as newsum from lifejacket_subscription where user_id='$user' and status='Active'")->result() as $key => $value1) {
				 		 			$totsum=$value1->newsum;
				 		 }
				 		 foreach ($this->model_lifejacket_subscription->query("select * from user_registration where user_id='$user'")->result() as $key => $value2) {
				 		 			$rank=$value2->user_rank_name;
				 		 }


				 		 if($totsum>=75000 && ($rank=='Elite' || $rank=='Champion' || $rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
							{
       	 						
       	 						 $this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Co Founder', designation='Co Founder' where user_id='$user'");
       	 						 $this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Elite', 'Co Founder', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
       	 						
							}
						else if($totsum>=30000 && ($rank=='Champion' || $rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
							{

	  						

       	 						 $this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Elite', designation='Elite' where user_id='$user'");

	    						 $this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Champion', 'Elite', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");

							}
						else if($totsum>=15000 && ($rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
						{
         						
         						$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Champion', designation='Champion' where user_id='$user'");


         						$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Flying Star', 'Champion', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
          					
						}
						else if($totsum>=5000 && ($rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
						{
       								$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Flying Star', designation='Flying Star' where user_id='$user'");

       								$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Rising Star', 'Flying Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
         						
						}
						else if($totsum>=500 && ($rank=='Cadet' || $rank=='Normal User'))
						{
									
									$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Rising Star', designation='Rising Star' where user_id='$user'");


									$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Cadet', 'Rising Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");

	 								
						}
						else if($totsum>=10 && $rank=='Normal User')
						{
									

									$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Cadet', designation='Cadet' where user_id='$user'");

										$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Normal User', 'Cadet', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
	 								
						}
						else
						{
      
						} 

				}
	}


}