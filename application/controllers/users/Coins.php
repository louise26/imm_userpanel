<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  Coins  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		
      

	}

	
		public function index(){


			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;



				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				$rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('id,user_id,username,email,image,first_name,last_name,user_rank_name',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}

						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
					return $this->load->view('users/coin_purchase',$data);

			}
			else {

				redirect('login');
			}
		}


		public function getHistory(){


			$this->is_logged_in();

			$use_id =$this->auth_user_id;

			$i= 0 ;
			$data = [] ;


			$sponsor = "" ;

			foreach ($this->model_credit_amt->query("SELECT * from lifejacket_subscription_coin where user_id='$use_id' order by id desc")->result() as $key => $value) {
						
						  $i +=1;

							if($value->sponsor=='ETC') {
									$sponsor = "LTC";
							}
							else {

								  $sponsor = $value->sponsor;
							}
						array_push($data,[
											$i,
											$value->transaction_no,
											date('F d, Y', strtotime($value->date)),
											number_format($value->amount,2),
											$value->pb,
											$value->package,
											$sponsor,
											'Paid'

										]);
			}




			$output = ['data' =>$data];


			echo json_encode($output);

		}

		public function getSellHistory(){


			$this->is_logged_in();

			$use_id =$this->auth_user_id;

			$i= 0 ;
			$data = [] ;


			$sponsor = "" ;

			foreach ($this->model_credit_amt->query("SELECT * from lifejacket_subscription_coin_sell where user_id='$use_id' order by id desc")->result() as $key => $value) {
						
								$i +=1;

							if($value->sponsor=='ETC') {
									$sponsor = "LTC";
							}
							else {

								  $sponsor = $value->sponsor;
							}
						array_push($data,[
											$i,
											$value->transaction_no,
											date('F d, Y', strtotime($value->date)),
											number_format($value->amount,2),
											$value->pb,
											$value->package,
											$sponsor,
											$value->status

										]);
			}


			$output = ['data' =>$data];


			echo json_encode($output);

		}

		public function downlinePurchases(){


						if($this->is_logged_in()){

					$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				foreach ($this->model_users->select('id,user_id,username,first_name,last_name,email,image,user_rank_name',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,

										];
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,

								];
					return $this->load->view('users/downlinecoin_purchase',$data);

			}
			else {

				redirect('login');
			}

		}


		/* Sponsor Commission Code Starts Here*/
	public function commission_of_referal($ref,$useridss,$amount,$invoice_no,$packages){

	//	$spc = 0;
		$date=date('Y-m-d');

   		$this->db->cache_off();
		foreach ($this->model_users->select('*',['user_id'=>$ref]) as $key => $value) {
			
			if($value->user_rank_name=='Cadet')
			{
				$spc=3;
			}
			else if($value->user_rank_name=='Rising Star')
			{
				$spc=5;
			}
			else if($value->user_rank_name=='Flying Star')
			{
				$spc=7;
			}
			else if($value->user_rank_name=='Champion')
			{
				$spc=8;
			}
			else if($value->user_rank_name=='Elite')
			{
				$spc=10;
			}
			else if($value->user_rank_name=='Co Founder')
			{
				$spc=10;
			}
			else 
			{
				$spc=0;
			}

		}

		$amount = $amount ;
		$pb 	= $amount ;

		$withdrawal_commission	=	$spc*$amount/100;
		$rwallet 				= 	$withdrawal_commission;
		$e_walletbal = 0 ;
		$new_balances = 0 ;

		if($withdrawal_commission!='' && $withdrawal_commission!=0)
			{
					$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					 $this->db->cache_off();
			                        
			                        foreach($this->model_final_e_wallet->select('amount',['user_id'=>$ref]) as $val){
			                               	
			                               	$e_walletbal = $val->amount;         
			                        }
			                        $new_balances = $rwallet + $e_walletbal;
			                        
			                        $this->model_final_e_wallet->update(['amount'=>$new_balances],['user_id'=>$ref]);
	
   					//$this->model_credit_amt->query("update final_e_wallet set amount=(amount+$rwallet) where user_id='$ref'");
   						
   					$this->model_credit_amt->query("insert into credit_debit values(NULL,'$invoice_no','$ref','$rwallet','0','0','$ref','$useridss','$date','Referral Bonus','Earn Coin Referral Bonus from $useridss for $packages Package','Commission of MYR $rwallet For Package ".$amount." ','Referral Bonus','$invoice_no','Referral Bonus','0','Withdrawal Wallet',CURRENT_TIMESTAMP,'$urls')");

			}
			$this->commission_of_level($useridss,$amount,$invoice_no,$packages);

	}
	/* Sponsor Commission Code Ends Here*/

	/* Sponsor Commission Code Starts Here*/

	public function  commission_of_level($useridss,$amount,$invoice_no,$packages){

			$date =		date('Y-m-d');
		
           // $spc  =		0;
			$this->db->cache_off();
			foreach ($this->model_matrix_downline_ref->query("select * from matrix_downline_ref where down_id='$useridss' and level<7")->result() as $key => $value) {
						$ref    = $value->income_id;
						$level  = $value->level;
					    
				foreach ($this->model_users->select('user_rank_name',['user_id'=>$value->income_id]) as $key => $value1) {
									if($value1->user_rank_name=='Cadet' && $value->level==2)
									{
											$spc=2;
									}
									else if($value1->user_rank_name=='Cadet' && $value->level==3)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Rising Star' && $value->level==2)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Rising Star' && $value->level==3)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==2)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==3)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Flying Star' && $value->level==4)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==2)
									{
										$spc=5;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==3)
									{
								 		$spc=3;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==4)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Champion' && $value->level==5)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==2)
									{
										$spc=5;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==3)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==4)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==5)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Elite' && $value->level==6)
									{
										$spc=1;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==2)
									{
									 	$spc=5;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==3)
									{
										$spc=4;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==4)
									{
										$spc=3;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==5)
									{
										$spc=2;
									}
									else if($value1->user_rank_name=='Co Founder' && $value->level==6)
									{
										$spc=1;
									}
									else 
									{
										$spc=0;
									}
	
					}
					$withdrawal_commission=$spc *  ($amount / 100 ) ;
    
					$rwallet=$withdrawal_commission;
					
					$e_walletbal = 0 ;
					$new_balances = 0 ;
					

					if($withdrawal_commission!='' && $withdrawal_commission!=0)
					{
			                        $this->db->cache_off();
			                        
			                        foreach($this->model_final_e_wallet->select('amount',['user_id'=>$value->income_id]) as $val){
			                               	$e_walletbal = $val->amount;         
			                        }
			                        $new_balances = $rwallet + $e_walletbal;
			                        
			                        $this->model_final_e_wallet->update(['amount'=>$new_balances],['user_id'=>$value->income_id]);
									//$this->model_final_e_wallet->query("update final_e_wallet set amount=(amount+$rwallet) where user_id='$ref'");

		 							$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	    								
	    							$this->model_credit_amt->query("insert into credit_debit values(NULL,'$invoice_no','$ref','$rwallet','0','0','$ref','$useridss','$date','Unilevel Bonus','Earn Coin Unilevel Bonus from $useridss for $packages Package','Commission of USD $rwallet For Package ".$amount." ','Unilevel $level Bonus','$invoice_no','Unilevel Bonus','0','Withdrawal Wallet',CURRENT_TIMESTAMP,'$urls')");
	    						 				//Rank_update($useridss);
					}
			}
	}

	/* Sponsor Commission Code Ends Here*/



	public function check_rank_kams($noty){


				$user = "";
					$totsum=0;
					$totsum = 0 ;
					$rank  = "";
					$this->db->cache_off();
				 foreach ($this->model_lifejacket_subscription->query("select * from lifejacket_subscription where user_id='$noty'")->result() as $key => $value) {
				
				 		 $user = $value->user_id;

				 		 foreach ($this->model_lifejacket_subscription->query("select sum(amount) as newsum from lifejacket_subscription where user_id='$user' and status='Active'")->result() as $key => $value1) {
				 		 			$totsum=$value1->newsum;
				 		 }
				 		 foreach ($this->model_lifejacket_subscription->query("select * from user_registration where user_id='$user'")->result() as $key => $value2) {
				 		 			$rank=$value2->user_rank_name;
				 		 }


				 		 if($totsum>=75000 && ($rank=='Elite' || $rank=='Champion' || $rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
							{
       	 						
       	 						 $this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Co Founder', designation='Co Founder' where user_id='$user'");
       	 						 $this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Elite', 'Co Founder', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
       	 						
							}
						else if($totsum>=30000 && ($rank=='Champion' || $rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
							{

       	 						 $this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Elite', designation='Elite' where user_id='$user'");
	    						 $this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Champion', 'Elite', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");

							}
						else if($totsum>=15000 && ($rank=='Flying Star' || $rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
						{
         						
         						$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Champion', designation='Champion' where user_id='$user'");
         						$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Flying Star', 'Champion', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
          					
						}
						else if($totsum>=5000 && ($rank=='Rising Star' || $rank=='Cadet' || $rank=='Normal User'))
						{
       								$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Flying Star', designation='Flying Star' where user_id='$user'");
       								$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Rising Star', 'Flying Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
         						
						}
						else if($totsum>=500 && ($rank=='Cadet' || $rank=='Normal User'))
						{
									
									$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Rising Star', designation='Rising Star' where user_id='$user'");


									$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Cadet', 'Rising Star', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");

	 								
						}
						else if($totsum>=10 && $rank=='Normal User')
						{
									

									$this->model_lifejacket_subscription->query("update user_registration set user_rank_name='Cadet', designation='Cadet' where user_id='$user'");

										$this->model_lifejacket_subscription->query("INSERT INTO `rank_achiever` (`id`, `user_id`, `last_rank`, `move_rank`, `ts`, `qualify_date`, `price`) VALUES (NULL, '$user', 'Normal User', 'Cadet', CURRENT_TIMESTAMP, '".date('Y-m-d')."', '0')");
	 								
						}
						else
						{
      
						} 

				}
	}

	public function buycoin(){



		$this->is_logged_in();

		$userid 	= $this->auth_user_id;
				
		$amount  	= $this->input->post('amount');
		$password 	= $this->input->post('password');
		$coin_type 	= $this->input->post('coin_type');

		$ewa 		= 'final_reg_wallet';
		$walls		= "Withdrawal Reg Wallet";

		$rand 		= rand(0000000001,9000000000);
    	$start 		= date('Y-m-d');
    	$end 		= date('Y-m-d', strtotime('+12 months'));

    	$user_ewalletamt = 0 ;
    	$urls 		=	"http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

    	$data 		= [] ;


    	$invoice_no=rand(1111111111,9999999999);

		$this->db->cache_off();
		

		if($this->model_users->count_ref(['user_id'=>$userid,'t_code'=>$password]) > 0) {

				foreach ($this->model_final_reg_wallet->query("select * from $ewa where user_id='$userid'")->result() as $key => $value) {
								$user_ewalletamt=$value->amount;
				}

			if($amount >=10){
				if($user_ewalletamt >= $amount) {

							$pv				= 	1;
              				$walleting 		= 	'final_imm_coin_wallet';
              				$convertamt 	= 	$amount * 100 / 100;
              				$convertamt_add = 	0 ;
              				$com_amt 		=	0 ;
              					$this->db->cache_off();
							foreach ($this->model_final_imm_coin_wallet->query("select * from imm_coin_percent where id=1")->result() as $key => $value1) {
										  $convertamt_add=$amount*$value1->percent/100;
							}

							$convertamt 	=	$convertamt + $convertamt_add;
             				$coins 			=	$convertamt / $pv;



             				  $lfid="LJ".$userid.$rand;

             				  	$this->db->cache_off();
             				  if($this->model_final_reg_wallet->query("update $ewa set amount=(amount-$amount) where user_id='$userid'")){


             				  		$this->model_final_reg_wallet->query("update $walleting set amount=(amount+$coins) where user_id='$userid'");


             				  		$this->model_final_reg_wallet->query("INSERT INTO `lifejacket_subscription_coin` (`id`, `user_id`, `package`, `amount`, `pay_type`, `pin_no`, `transaction_no`, `date`, `expire_date`, `remark`, `ts`, `status`, `invoice_no`,`lifejacket_id`,`username`,`sponsor`,`pb`) VALUES (NULL, '$userid', '$coins', '$amount', '$walls', '$password', '$rand', '$start', '$end', 'Coin Purchase', CURRENT_TIMESTAMP, 'Active', '$rand','$walleting','".$userid."','".$coin_type."','$pv')");



             				  		$this->model_final_reg_wallet->query("INSERT into credit_debit (`transaction_no`,`user_id`,`credit_amt`,`debit_amt`,`admin_charge`,`receiver_id`,`sender_id`,`receive_date`,`ttype`,`TranDescription`,`Cause`,`Remark`,`invoice_no`,`product_name`,`status`,`ewallet_used_by`,`current_url`) values('$rand','$userid','0','$amount','0','$userid','$userid','$start','Coin Purchase','Coin Purchase by $userid','Coin Purchase by $userid ','Coin Purchase $userid','$rand','Coin Purchase by $userid','0','$walls','$urls')");
             				  		

             				  	
             				  			$this->db->cache_off();
             				  		 foreach ($this->model_users->query("select * from user_registration where user_id='$userid'")->result() as $key => $value2) {
             				  		 		 	
             				  		 		 	if($value2->ref_id !=''){

             				  		 		 			$com_amt =	$amount *	100	/ 100;

             				  		 		 			$this->commission_of_referal($value2->ref_id,$userid,$com_amt,$invoice_no,$amount);



             				  		 		 			$this->model_final_reg_wallet->query("INSERT INTO `lifejacket_subscription` (`id`, `user_id`, `package`, `amount`, `pay_type`, `pin_no`, `transaction_no`, `date`, `expire_date`, `remark`, `ts`, `status`, `invoice_no`,`lifejacket_id`,`username`,`sponsor`,`pb`) VALUES (NULL, '$userid', '$coins', '$amount', '$walls', '$password', '$rand', '$start', '$end', 'Coin Purchase', CURRENT_TIMESTAMP, 'Active', '$rand','$walleting','".$userid."','".$coin_type."','$pv')");
             				  		 		 			
             	
             				  		 		 			$this->check_rank_kams($userid);

             				  		 		 			
             				  		 		 	}
             				  		 }

             				  }

             				  else {

             				  			array_push($data,[

															'title'		=> 'Oops !',
															'msg'		=> 'Something went wrong',
															'status'	=> 'error'
														]);
             				  			
             				  }
								
				}
				else {
							array_push($data,[

												'title'		=> 'Oops !',
												'msg'		=> 'Insufficient Fund ',
												'status'	=> 'error'
											]);
						
				}
		}

				else {
								array_push($data,[

												'title'		=> 'Oops !',
												'msg'		=> 'Minimum purchased must be $10',
												'status'	=> 'error'
											]);


				}



										array_push($data ,[
															'title'		=> 'Good Job !',
															'msg'		=> 'Coin has been purchased',
															'status'	=> 'success'
														]);

		}
		else {

							array_push($data,[

												'title'		=> 'Oops !',
												'msg'		=> 'Incorrect Transaction Password',
												'status'	=> 'error'
											]);

		}

			echo json_encode($data);
		

	}



	public function sellCoin(){



		$this->is_logged_in();

		$userid 	= $this->auth_user_id;
				
		$coins  	= $this->input->post('amount');
		$password 	= $this->input->post('password');
		$coin_type 	= $this->input->post('coin_type');

		$walleting	= "";
		$pv 		= 0 ;

		$data 		= [] ;

		$this->db->cache_off();

		if($coin_type =="BTC"){
				$this->db->cache_off();
			foreach ($this->model_credit_amt->query("SELECT original_rate from currency_rates where id='1'")->result() as $key => $value) {
					
					 $pv=$value->original_rate;
             		 $walleting='final_bitcoin_wallet';
			}

		}
		else if($coin_type =="XRP"){
					$this->db->cache_off();
					foreach ($this->model_credit_amt->query("SELECT original_rate from currency_rates where id='2'")->result() as $key => $value) {
					
					 $pv=$value->original_rate;
             		 $walleting='final_ripple_wallet';
			}
		}
		else if($coin_type =="ETH"){
						$this->db->cache_off();
					foreach ($this->model_credit_amt->query("SELECT original_rate from currency_rates where id='3'")->result() as $key => $value) {
					
					 $pv=$value->original_rate;
             		 $walleting='final_ethereum_wallet';
			}
		}
		else if($coin_type =="ETC"){
					$this->db->cache_off();
					foreach ($this->model_credit_amt->query("SELECT original_rate from currency_rates where id='4'")->result() as $key => $value) {
					
					 $pv=$value->original_rate;
             		 $walleting='final_ethereum_classic_wallet';
			}
		}
		else {

			 $pv=1;
              $walleting='final_bitcoin_wallet';
		}

		$ewa=$walleting;
		$walls=$coin_type." Wallet";	
		$coin=$coins;
		$user_ewalletamt	= "";

		 $rand = rand(0000000001,9000000000);
   		 $start=date('Y-m-d');
    	 $end = date('Y-m-d', strtotime('+12 months'));

    	 $this->db->cache_off();


    	 if($this->model_users->count_ref(['user_id'=>$userid,'t_code'=>$password])> 0){
    	 		 foreach ($this->model_credit_amt->query("select * from $ewa where user_id='$userid'")->result() as $key => $value) {
    	 					$user_ewalletamt=$value->amount;
    				}

    	 		$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

    	 		if($user_ewalletamt>=$coin){

    	 				   $amount=$coin*$pv;

    	 				    $lfid="LJ".$userid.$rand;
    	 				    $this->db->cache_off();
    	 				    if($this->model_credit_amt->query("update $ewa set amount=(amount-$coin) where user_id='$userid'")){
		    								
		    								$this->model_credit_amt->query("INSERT INTO `lifejacket_subscription_coin_sell` (`id`, `user_id`, `package`, `amount`, `pay_type`, `pin_no`, `transaction_no`, `date`, `expire_date`, `remark`, `ts`, `status`, `invoice_no`,`lifejacket_id`,`username`,`sponsor`,`pb`) VALUES (NULL, '$userid', '$coin', '$amount', '$walls', '$password', '$rand', '$start', '$end', 'Coin Sell', CURRENT_TIMESTAMP, 'Unpaid', '$rand','$walleting','".$userid."','".$coin_type."','$pv')");


		    									array_push($data,[

							    	 							'title'		=> 'Good job !',
																'msg'		=> 'Coin has been successfully purchased',
																'status'	=> 'success'
							    	 						]);

    	 				    }
    	 				    else {
    	 				    	array_push($data,[

				    	 							'title'		=> 'Oops !',
													'msg'		=> 'Something went wrong',
													'status'	=> 'error'
				    	 						]);
    	 				    }

    	 		}
    	 		else {

    	 			array_push($data,[

    	 							'title'		=> 'Oops !',
									'msg'		=> 'Insufficient Fund',
									'status'	=> 'error'
    	 						]);

    	 		}
    	 }
    	 else {
    	 		array_push($data,[

    	 							'title'		=> 'Oops !',
									'msg'		=> 'Incorrect Transaction Password',
									'status'	=> 'error'
    	 						]);

    	 }
    	

    	 echo json_encode($data);
	}

	public function downlinecoin_purchase(){

			$i = 0 ;
			$data  = [] ;

			$this->is_logged_in();

			$userid 	= $this->auth_user_id;

			foreach ($this->model_credit_amt->query("select * from matrix_downline_ref where income_id='$userid' ")->result() as $key => $value) {
					foreach ($this->model_credit_amt->query("select * from lifejacket_subscription_coin where user_id='".$value->down_id."' and remark='Coin Purchase'")->result() as $key => $value1) {
							foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value->down_id."'")->result() as $key => $value2) {
								
								$i +=1;
								array_push($data,[	
													$i,
													$value1->user_id,
													$value2->username,
													$value1->amount,
													$value->level,
													$value1->sponsor .' Coin Purchase',
													date('F d, Y',strtotime($value1->date))


												]);
							}
					}
			}

			$output = ['data'=>$data];


		echo json_encode($output);
	}

		public function searchdownlinecoin_purchase(){

			$i = 0 ;
			$data  = [] ;

			$this->is_logged_in();

			$userid 	= $this->auth_user_id;
			  	$df 	=  date('Y-m-d',strtotime($this->input->post('df')));
			   	$dt 	=  date('Y-m-d',strtotime( $this->input->post('dt')))	;

			foreach ($this->model_credit_amt->query("select * from matrix_downline_ref where income_id='$userid'   ")->result() as $key => $value) {
					foreach ($this->model_credit_amt->query("SELECT * from lifejacket_subscription_coin where user_id='".$value->down_id."' and remark='Coin Purchase' and date BETWEEN '$df' and '$dt'")->result() as $key => $value1) {
							foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value->down_id."'")->result() as $key => $value2) {
								
								$i +=1;
								array_push($data,[	
													'id'=>$i,

													'userid'=>$value1->user_id,
													'username'=>$value2->username,
													'amount'=>$value1->amount,
													'level'=>$value->level,
													'sponsor'=>$value1->sponsor .' Coin Purchase',
													'date'=>date('F d, Y',strtotime($value1->date))


												]);
							}
					}
			}

			$output = ['data'=>$data];


		echo json_encode($output);
	}


	public function mycoins() {
			if($this->is_logged_in()) {
				
				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		         $this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
											't_code'		=> 	$value->t_code,
										];
						}
						 $this->db->cache_off();
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									'etc' 	=> $this->model_final_bitcoin_wallet->select('amount',['user_id'=>$use_id]),
									'eth' 	=> $this->model_final_ethereum_wallet->select('amount',['user_id'=>$use_id]),
									'ltc' 	=> $this->model_final_ethereum_classic_wallet->select('amount',['user_id'=>$use_id]),
									'xrp' 	=> $this->model_final_ripple_wallet->select('amount',['user_id'=>$use_id]),
									'imm' 	=> $this->model_final_imm_coin_wallet->select('amount',['user_id'=>$use_id]),
									
								];
				return $this->load->view('users/mycoins',$data);
			}
			else {
				redirect('login');
			}


	}

	
}