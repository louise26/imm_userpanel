<?php
defined('BASEPATH') or exit('No direct script access allowed');



class Directincome  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		
	



	}

	public function index() {

			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
		           $this->db->cache_off();
		           
		           
		           $rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						
						}
						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,

								];
				return $this->load->view('users/directincome',$data);
			}

			else {


				redirect('login');
			}

	}

	public function getDirectIncome(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data = array();
			   	$use_id =	$this->auth_user_id;
			   	$stat 	=  	"";

			 		 foreach ($this->model_credit_amt->query("SELECT transaction_no,sender_id,credit_amt,ttype,receive_date,status FROM credit_debit WHERE user_id='$use_id' and ttype='Referral Bonus' and (product_name='Referral Bonus' || product_name='Coin Referral Bonus') order by id desc")->result() as $key => $value) {
							   		$i +=1;
							  	 	$row = array();
							  	 	$row[] = $i;
									$row[] = $value->transaction_no;
									$row[] = $value->sender_id;
									$row[] = '$ ' . number_format($value->credit_amt,2);
									$row[] = $value->ttype;
									$row[] = date('F d, Y',strtotime($value->receive_date));

									if($value->status==0){
											$stat = 'Paid';
										} 
									else {
												$stat = 'Unpaid';
										}
									
									$row[]  = $stat;
									$data[] = $row;	

							   	}
							   	
			   		$output = array(
							"data" => $data,
						);  

			
		echo json_encode($output);

	}
	public function levelIncomeView(){

			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
				$this->db->cache_off();
				
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {
							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/levelincome',$data);
			}
			else {
				redirect('login');
			}
	}
	public function profitIncomeView(){

			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
					$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/profitshareincome',$data);
			}
			else {
				redirect('login');
			}
	}
	public function bonusIncomeView(){

			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
					$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/bonusprofit',$data);
			}
			else {
				redirect('login');
			}
	}
	public function getLevelIncome(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data = array();
                   
			   	$use_id =	$this->auth_user_id;
			   	$stat 	=  	"";

			
			 		 foreach ($this->model_credit_amt->query("SELECT transaction_no,sender_id,credit_amt,ttype,receive_date,status,Remark FROM credit_debit WHERE user_id='$use_id' and (ttype='Unilevel Bonus' || ttype='') and (product_name='Unilevel Bonus'  || product_name='Coin Unilevel Bonus') order by id desc")->result() as $key => $value) {
							   		$i +=1;
							  	 	$row   = array();
							  	 	$row[] = $i;
									$row[] = $value->transaction_no;
									$row[] = $value->sender_id;
									$row[] = '$ ' . $value->credit_amt;
									$row[] = $value->ttype;
									$row[] = $value->Remark;
									$row[] = date('F d, Y',strtotime($value->receive_date));

									if($value->status==0){
											$stat = 'Paid';
										} 
									else {
												$stat = 'Unpaid';
										}
									$row[]  = $stat;
									$data[] = $row;	

							   	}
			   		$output = array(
							"data" => $data,
						);  

			
		
		echo json_encode($output);

	}
	public function getProfitIncome(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data = array();
                   
			   	$use_id =	$this->auth_user_id;
			   	$stat 	=  	"";
			   	$sender =  "";
			   	$transd = "";
			   	$name = "";
			   	$sendid = "";
			
			 		 foreach ($this->model_credit_amt->query("select * from credit_debit where user_id='$use_id' and ttype='Profit Sharing Bonus' order by receive_date desc")->result() as $key => $value) {
							   		$i +=1;
							  	 	$row = array();
							  	 	$row[] = $i;
									$row[] = $value->transaction_no;
									if($value->sender_id=='123456') {
											$sender = "IMM2223334";
									}
									else {
											 	$sender	= $value->sender_id;
									}
									$sendid = $value->sender_id;
									$row[] = $sender;
									foreach ($this->model_users->query("SELECT first_name,last_name from user_registration where user_id='$sendid'")->result() as $key => $value1) {
										$name = $value1->first_name . ' ' . $value1->last_name;	
									}
									$row[] = $name;

									$row[] = '$ ' . $value->credit_amt;

									if($value->TranDescription=="IMM Coin"){
											$transd = "IMM Coin" ;
									}
									else {
										   $transd = '';
									}

									$row[] = $transd. ' '.$value->ttype;
									
									$row[] = date('F d, Y',strtotime($value->receive_date));

									if($value->status==0){
											$stat = 'Paid';
										} 
									else {
												$stat = 'Unpaid';
										}

									$row[]  = $stat;
									$data[] = $row;	

							   	}
			   		$output = array(
							"data" => $data,
						);  

			
		
		echo json_encode($output);

	}


	public function getBonusIncome(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data = array();
                   
			   	$use_id =	$this->auth_user_id;
			   	$stat 	=  	"";
			   	$sender =  "";
			   	$transd = "";
			   	$name = "";
			   	$sendid = "";

			 	foreach ($this->model_credit_amt->query("SELECT transaction_no,sender_id,credit_amt,TranDescription,receive_date,status from credit_debit where user_id='$use_id'  and product_name='Bonus From Profit' order by receive_date desc LIMIT 1000")->result() as $key => $value) {
							   		$i +=1;
							  	 	$row = array();
							  	 	$row[] = $i;
									$row[] = $value->transaction_no;
									if($value->sender_id=='123456') {
											$sender = "IMM2223334";

									}
									else {
											 	$sender	= $value->sender_id;
									}
									$sendid = $value->sender_id;
									$row[] = $sender;
									

									$row[] = '$ ' . $value->credit_amt;

									if($value->TranDescription=="IMM Coin"){
											$transd = "IMM Coin" ;
									}
									else {
										   $transd = '';
									}

									$row[] = $transd. '  Bonus From Profit ';
									
									$row[] = date('F d, Y',strtotime($value->receive_date));

									if($value->status==0){
											$stat = 'Paid';
										} 
									else {
												$stat = 'Unpaid';
										}

									$row[]  = $stat;

									$data[] = $row;	

							   	}
			   		$output = array(
							"data" => $data,
						);  

		echo json_encode($output);

	}

	public function royaltyIncomeView(){

			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
					$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/royalty',$data);
			}
			else {
				redirect('login');
			}
	}

	public function getRoyaltyIncome(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data = array();
                   
			   	$use_id =	$this->auth_user_id;
			   	$stat 	=  	"";
			   	$sender =  "";
			   	$transd = "";
			   	$name = "";
			   	$sendid = "";
		       
			 		 foreach ($this->model_credit_amt->query("SELECT * from credit_debit where user_id='$use_id' and ttype='Royalty Bonus'  order by id desc")->result() as $key => $value) {
							   		$i +=1;
							  	 	$row = array();
							  	 	$row[] = $i;
									$row[] = $value->transaction_no;
									$row[] = '$ ' .$value->credit_amt;
									$row[] = $value->ttype;

								
									$d =  date('m',strtotime($value->receive_date)) ;
									
									if($value->status==0){
											$stat = 'Paid';
										} 
									else {
												$stat = 'Unpaid';
										}
								    $num = $d-2;
								    $x   = 0 ;
									
									if($value->receive_date=='2018-01-31'){
									    $x = 12;
									}
									if($value->receive_date=='2018-02-24'){
									    $x = 1;
									}
									if( $num == 0 ) {
											$x = $num + 1;
									}
									else if ($num < 0 ) {
											$x = $num + 12;
									}
									else if($num > 0 ){
											$x = $num ;
									}
									if($value->receive_date=='2018-01-31'){
									    $row [] =  date('F', mktime(0, 0, 0, 12, 10)); 
									}
									else if($value->receive_date=='2018-03-31') {
									        $row [] =  date('F', mktime(0, 0, 0, 1, 10)); 
									}
									else {
									     $row[] = date('F', mktime(0, 0, 0, $x, 10));
									}
								
									$row [] = date('F d, Y',strtotime($value->receive_date));
									$row[]  = $stat;
									$data[] = $row;	

							   	}
			   		$output = array(
							"data" => $data,
						);  

		echo json_encode($output);
		
	}

	public function downlineIncomeView(){

			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
						$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/downline_purchases',$data);
			}
			else {
				redirect('login');
			}
	}

	public function getDownlineIncome(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data = array();
                   
			   	$use_id 	=	$this->auth_user_id;
			   	$stat 		=  	"";
			   	$sender 	=   "";
			   	$transd 	=  	"";
			   	$name 		=   "";
			   	$sendid 	=   "";
			   	$date 		=    0;
			   	$data2 		=   "";
			   	$data11 	=   "";
			   	$amount 	=   "";
			   	$username   =   "";
			   	$user 		=   "";
			   	$remarks 	=   "";
			   	$date1 		=   "";
			   	$stat 		=   "";
			   	$value1 	=   "";
		
			 		 foreach ($this->model_credit_amt->query("SELECT  down_id,level,l_date from matrix_downline_ref where income_id='$use_id' order by l_date DESC")->result() as $key => $value) {
							  		foreach($this->model_credit_amt->query("SELECT * from lifejacket_subscription  JOIN user_registration ON lifejacket_subscription.user_id=user_registration.user_id  where lifejacket_subscription.user_id='".$value->down_id."'  and (lifejacket_subscription.remark='Package Purchase' || lifejacket_subscription.remark='Coin Purchase') ")->result() as $value1){

							  					$row 	= array();
							  					$i 		+=1;
							  					$row [] = $i;
							  					$row [] = $value1->user_id;
							  					$row [] = $value1->username;
							  					$row [] = '$ ' .$value1->amount ;
												$row [] = $value->level;
												$row [] = $value1->remark;
												$row [] = date('F d, Y',strtotime($value1->date));

												$data[] = $row;	
							  		} 								
							   }


			   		$output = array(
							"data" => $data,
						);  
			       
			
		
		echo json_encode($output);
	}
	
	
	public function searchMonthIncome() {
	    
	    
	            $this->is_logged_in();
	            $use_ids 	=	$this->input->post('userid');
	            $df 	    =	$this->input->post('df');
	            $dt 	    =	$this->input->post('dt');
			   
			    $use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
						$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
			   	$condition1 = "" ;
			   	
			   	$condition2 = "" ;
			   	
			   	
			   	$result = [] ;
			   	
			 
			   	if(!empty($use_ids)  && !empty($df)  && !empty($dt)) {
			   	    
			   	    
			   	    $condition2 = "AND down_id='$use_ids' " ;
			   	    
			   	    $condition1 = " AND CAST(lifejacket_subscription.ts as date) BETWEEN '".date('Y-m-d',strtotime($df))."' AND '".date('Y-m-d',strtotime($dt))."' ";
			   	    
			   	}
			   	else if(empty($use_ids)  && !empty($df)  && !empty($dt)) {
			   	    
			   	    
			   	     $condition2 = "" ;
			   	    
			   	     $condition1 = " AND CAST(lifejacket_subscription.ts as date) BETWEEN '".date('Y-m-d',strtotime($df))."' AND '".date('Y-m-d',strtotime($dt))."' ";
			   	    
			   	    
			   	}
			   	
			   	 else if(!empty($use_ids) && empty($df)  && empty($dt)) { 
			   	    
			   	     $condition2 = "AND down_id='$use_ids' " ;
			   	    
			   	     $condition1 = "";
			   	    
			   	}
			   	
			   	else  {
			   	    
			   	    $condition2 = "" ;
			   	    
			   	     $condition1 = "";
			   	    
			   	    
			   	    
			   	}
			   	
			 
			   	
			   	
		
			 		 foreach ($this->model_credit_amt->query("SELECT  down_id,level,l_date from matrix_downline_ref where income_id='$use_id' $condition2 order by l_date DESC")->result() as $key => $value) {
							  		foreach($this->model_credit_amt->query("SELECT * from lifejacket_subscription  JOIN user_registration ON lifejacket_subscription.user_id=user_registration.user_id  where lifejacket_subscription.user_id='".$value->down_id."'   $condition1 ")->result() as $value1){

												
												
												array_push($result,[
												    
												                    'user_id' =>$value1->user_id,
												                    'username' => $value1->username,
												                    'amount' => $value1->amount,
												                    'level' =>  $value->level,
												                    'remark' => $value1->remark,
												                    'date' => date('F d, Y',strtotime($value1->date))
												    
												            ]);
							  		} 								
							   }
			  
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									'result' => $result
								];

	    
	        return $this->load->view('users/downline_search_purchase',$data);
	    
	}

	public function rankView(){
			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
					$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/rank',$data);
			}
			else {
				redirect('login');
			}
	}
		public function getRank(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data 	= array();
                   
			   	$use_id =	$this->auth_user_id;
			   	$stat 	=  	"";
			   	$sender =  	"";
			   	$transd = 	"";
			   	$name 	= 	"";
			   	$sendid = 	"";

		
			 		 foreach ($this->model_credit_amt->query("SELECT * FROM rank_achiever where user_id='$use_id'")->result() as $key => $value) {
							   		$i 		+=1;
							  	 	$row 	= array();
							  	 	$row[] 	= $i;
									$row[]  =  $value->last_rank;
									$row[]  = $value->move_rank;
									$row[]  = date('F d, Y',strtotime($value->qualify_date));
									$data[] = $row;	

							   	}
			   		$output = array(
							"data" => $data,
						);  

			
		
		echo json_encode($output);

	}

	public function directMemberView(){

			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
						$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/direct_member',$data);
			}
			else {
				redirect('login');
			}
	}

	public function getDirectMember(){

			   $i=0;	
			   	$this->is_logged_in();
			   	$data = array();
                   
			   	$use_id =	$this->auth_user_id;
			   	$stat 	=  "";
			   	$sender =  "";
			   	$transd =  "";
			   	$name 	= "";
			   	$sendid = "";

			   	$start 	=	date('Y-m-').'01';
                $end 	=	date('Y-m-').'31';

                $start1 	=	'2017-11-01';
                $end1 		=   '2017-11-31';
	
                     	foreach ($this->model_credit_amt->query("select * from user_registration where ref_id='$use_id'")->result() as $key => $value1) {
                     					$row = array();
                     				$i +=1;
									$t1 		= 0; 
									$totaffl 	= 0;
								    $te1 		= 0;
								    $te_nov1 	= 0;
								    $te_nov123 	= 0;
                     				
                     				foreach ($this->model_credit_amt->query("select * from matrix_downline_ref where income_id='".$value1->user_id."'")->result() as $key => $value2) {
                     							
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1 from lifejacket_subscription where user_id='".$value2->down_id."'")->result() as $key => $value3) {

                     								$t 	 = $value3->tot1;
													$t1 += $t;

                     							}
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1 from lifejacket_subscription where user_id='".$value2->down_id."' and (date between '$start' and '$end')")->result() as $key => $value3) {
                     								 $te 	 = $value3->tot1;
													 $te1 	+= $te;
                     							}
                     							foreach ($this->model_credit_amt->query("select sum(amount) as tot1_nov from lifejacket_subscription where user_id='".$value2->down_id."' and (date between '$start1' and '$end1')")->result() as $key => $value3) {
                  
                     									$te_nov=$value3->tot1_nov;
														$te_nov1+=$te_nov;

                     							}

                     				}
                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot from lifejacket_subscription where user_id='".$value1->user_id."'")->result() as $key => $c1) {
                     							 $total= $t1+$c1->tot;
                     				}

                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot from lifejacket_subscription where user_id='".$value1->user_id."' and (date between '$start' and '$end')")->result() as $key => $crr1) {
                     							 $totaffl= $te1+$crr1->tot;

                     				}
                     				foreach ($this->model_credit_amt->query("select sum(amount) as tot_no from lifejacket_subscription where user_id='".$value1->user_id."' and (date between '$start1' and '$end1')")->result() as $key => $crr1_nov) {
                     						 $te_nov123= $te_nov1+$crr1_nov->tot_no;
                     				}

                     						$row  [] = $i;
                     						$row  [] = $value1->user_id;
                     						$row  [] =  '<a href="'.site_url().'reports/downline-member-report/'. $value1->user_id.'">'.$value1->username.'</a>';
                     						$row  [] = $value1->first_name .' '. $value1->last_name;
                     						$row  [] = '$ ' . number_format($total,2);
                     					
                     						$row  [] = '$ ' . number_format($totaffl,2);
                     						$row  [] = date('F d, Y',strtotime($value1->registration_date));
         

                     				$data[] = $row;	
                     		}
					   	
						   		$output = array(
													"data" => $data,
												);  

			echo json_encode($output);

	}

	public function  downlineTreeView() {


			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
						$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/downline_tree',$data);
			}
			else {
				redirect('login');
			}

	}
/// DOWNLINE MEMBERS
	public function getDonwlineTree() {

				$i=0;	
			   	$this->is_logged_in();
			   	$use_id =$this->auth_user_id;
			   	$data = array();
			   	
			   	foreach ($this->model_credit_amt->query("SELECT * FROM matrix_downline_ref WHERE income_id='$use_id'")->result() as $key => $value) {
			   				
                   	  $i +=1;	

                   	 foreach ($this->model_credit_amt->query("select username,first_name,last_name,user_rank_name,registration_date from user_registration where user_id='".$value->down_id."'")->result() as $key => $value1) {
                   	 	


	                   	 	 foreach ($this->model_credit_amt->query("SELECT qualify_date FROM rank_achiever where user_id='".$value->down_id."' and move_rank='".$value1->user_rank_name."'")->result() as $key => $value2) {


			                   	 	 	foreach ($this->model_credit_amt->query("SELECT sum(amount) as domamts from lifejacket_subscription where user_id='".$value->down_id."'")->result() as $key => $value3) {
			                   	 	 					
			                   	 	 					$row = array();
									                
									 
					                   	 				 array_push($data,

					                   	 				 		[
					                   	 				 				$i,
					                   	 				 				$value->down_id,
					                   	 				 				$value1->username,
					                   	 				 				$value1->first_name . ' '.$value1->last_name,
					                   	 				 				$value1->user_rank_name,
					                   	 				 				date('F d, Y' ,strtotime($value2->qualify_date)),
					                   	 				 				'$ ' . number_format($value3->domamts,4),
					                   	 				 				$value->level,
					                   	 				 				date('F d, Y',strtotime($value1->registration_date))
					                   	 				 		] 

					                   	 				 	);
					                   	 		
		                   	 					}
	                   		 	}
                   	 }      
                   	            
			   	}


			   	$output = array(
													"data" => $data,
												);  

			echo json_encode($output);

	}
//END OF DOWNLINE MEMBERS
	public function  downlineMemberTreeView() {


			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
				$upline_id = "";
				$upline_name ="";
					$this->db->cache_off();
				foreach ($this->model_credit_amt->query("select * from matrix_downline_ref where down_id='".$use_id."' and level='1'")->result() as $key => $value) {
						foreach ($this->model_credit_amt->query("select user_id,first_name,last_name from user_registration where user_id='".$value->income_id."'")->result() as $key => $value1) {
									
									$upline_id = $value1->user_id;
									$upline_name = $value1->first_name . ' ' . $value1->last_name;
						}
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->username,
											'id'			=>  $value->id,
											'upline_id'		=>  $upline_id,
											'upline_name'	=>	$upline_name

										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/genealogy',$data);
			}
			else {
				redirect('login');
			}

	}

// DOWNLINE MEMBER TREE
	public function level1(){
				$i=0;	
			   	$this->is_logged_in();
			   	$use_id =$this->auth_user_id;
			   	$data = array();
			  
			   	$count =  "";
			   	$stat = '<i class="fa fa-close"></i>' ;
			   	foreach ($this->model_credit_amt->query("SELECT * from matrix_downline_ref where income_id='$use_id' and level='1'")->result() as $key => $value) {
			   	     
			   				foreach ($this->model_credit_amt->query("SELECT * from user_registration where  user_id='".$value->down_id."'")->result() as $key => $value1) {
			   					
			   					 foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value1->ref_id."'")->result() as $key => $value2) {
			   					 			
			   					 			foreach ($this->model_credit_amt->query("select status from lifejacket_subscription where user_id='".$value1->user_id."' ")->result() as $key => $value3) {

													$count = $value3->status;
												
        			 
        			                                if( $count =='Active'){
			   					 				        	$stat = '<i class="fa fa-check"></i>';
        			   					 			}
        			   					 		    else if( $count !='Active') {
        			   					 					$stat = '<i class="fa fa-close"></i>';
        			   					 			}
        			   		
			   					 			}
			   					 			
			   					 				 		 if($value1->user_rank_name=='Normal User') {
        			   					 			        	$stat = '<i class="fa fa-close"></i>';
        			   					 			    }
			   					 			$i +=1;
			   					 			array_push($data,
			   									[
			   											$i,
				   										$value1->user_id,
				   										$value1->first_name . ' '. $value1->last_name,
				   										$value1->email,
				   										$value1->ref_id .'-' . $value2->first_name .' '. $value2->last_name,
				   										$value1->ts,
				   										$stat
			   									]) ;
			   					 }
			   					
			   				}
			   	}
			   	  	$output = array(
													"data" => $data,

												);  

			echo json_encode($output);

	}

	public function level2(){
				$i=0;	
			   	$this->is_logged_in();
			   	$use_id =$this->auth_user_id;
			   	$data = array();
			    	$stat = '<i class="fa fa-close"></i>' ;
			   	$count =  "";
			   	foreach ($this->model_credit_amt->query("SELECT * from matrix_downline_ref where income_id='$use_id' and level='2'")->result() as $key => $value) {
			   				foreach ($this->model_credit_amt->query("SELECT * from user_registration where  user_id='".$value->down_id."'")->result() as $key => $value1) {
			   					
			   					 foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value1->ref_id."'")->result() as $key => $value2) {
			   					 			
			   					 			foreach ($this->model_credit_amt->query("select * from lifejacket_subscription where user_id='".$value1->user_id."' ")->result() as $key => $value3) {

													$count = $value3->status;
													
													
													
        			                                if( $count =='Active'){
			   					 				        	$stat = '<i class="fa fa-check"></i>';
        			   					 			}
        			   					 		    else if( $count !='Active') {
        			   					 					$stat = '<i class="fa fa-close"></i>';
        			   					 			}
        			   		
			   					 			}
			   					 			
			   					 				 		 if($value1->user_rank_name=='Normal User') {
        			   					 			        	$stat = '<i class="fa fa-close"></i>';
        			   					 			    }
			   					 			$i +=1;
			   					 			array_push($data,
			   									[
			   											$i,
				   										$value1->user_id,
				   										$value1->first_name . ' '. $value1->last_name,
				   										$value1->email,
				   										$value1->ref_id .'-' . $value2->first_name .' '. $value2->last_name,
				   										$value1->ts,
				   										$stat
			   									]) ;
			   					 }
			   					
			   				}
			   	}
			   	  	$output = array(
													"data" => $data,
												);  

			echo json_encode($output);

	}

	public function level3(){
				$i=0;	
			   	$this->is_logged_in();
			   	$use_id =$this->auth_user_id;
			   	$data = array();
			    	$stat = '<i class="fa fa-close"></i>' ;
			   	$count =  "";
			   	foreach ($this->model_credit_amt->query("SELECT * from matrix_downline_ref where income_id='$use_id' and level='3'")->result() as $key => $value) {
			   				foreach ($this->model_credit_amt->query("SELECT * from user_registration where  user_id='".$value->down_id."'")->result() as $key => $value1) {
			   					
			   					 foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value1->ref_id."'")->result() as $key => $value2) {
			   					 			
			   					 			foreach ($this->model_credit_amt->query("select * from lifejacket_subscription where user_id='".$value1->user_id."' ")->result() as $key => $value3) {

													$count = $value3->status;
													
													
        			                                if( $count =='Active'){
			   					 				        	$stat = '<i class="fa fa-check"></i>';
        			   					 			}
        			   					 		    else if( $count !='Active') {
        			   					 					$stat = '<i class="fa fa-close"></i>';
        			   					 			}
        			   		
			   					 			}
			   					 			
			   					 				 		 if($value1->user_rank_name=='Normal User') {
        			   					 			        	$stat = '<i class="fa fa-close"></i>';
        			   					 			    }
			   					 			$i +=1;
			   					 			array_push($data,
			   									[
			   											$i,
				   										$value1->user_id,
				   										$value1->first_name . ' '. $value1->last_name,
				   										$value1->email,
				   										$value1->ref_id .'-' . $value2->first_name .' '. $value2->last_name,
				   										$value1->ts,
				   										$stat
			   									]) ;
			   					 }
			   					
			   				}
			   	}
			   	  	$output = array(
													"data" => $data,
												);  

			echo json_encode($output);

	}
	public function level4(){
				$i=0;	
			   	$this->is_logged_in();
			   	$use_id =$this->auth_user_id;
			   	$data = array();
			   	$stat = '<i class="fa fa-close"></i>' ;
			   	$count =  "";
			   	foreach ($this->model_credit_amt->query("SELECT * from matrix_downline_ref where income_id='$use_id' and level='4'")->result() as $key => $value) {
			   				foreach ($this->model_credit_amt->query("SELECT * from user_registration where  user_id='".$value->down_id."'")->result() as $key => $value1) {
			   					 foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value1->ref_id."'")->result() as $key => $value2) {
			   					 			foreach ($this->model_credit_amt->query("select * from lifejacket_subscription where user_id='".$value1->user_id."' ")->result() as $key => $value3) {

													$count = $value3->status;
													
													
        			                                if( $count =='Active'){
			   					 				        	$stat = '<i class="fa fa-check"></i>';
        			   					 			}
        			   					 		    else if( $count !='Active') {
        			   					 					$stat = '<i class="fa fa-close"></i>';
        			   					 			}
        			   		
			   					 			}
			   					 			
			   					 				 		 if($value1->user_rank_name=='Normal User') {
        			   					 			        	$stat = '<i class="fa fa-close"></i>';
        			   					 			    }
			   					 			$i +=1;
			   					 			array_push($data,
			   									[
			   											$i,
				   										$value1->user_id,
				   										$value1->first_name . ' '. $value1->last_name,
				   										$value1->email,
				   										$value1->ref_id .'-' . $value2->first_name .' '. $value2->last_name,
				   										$value1->ts,
				   										$stat
			   									]) ;
			   					 }
			   					
			   				}
			   	}
			   	  	$output = array(
													"data" => $data,
												);  

			echo json_encode($output);

	}
	public function level5(){
				$i=0;	
			   	$this->is_logged_in();
			   	$use_id =$this->auth_user_id;
			   	$data = array();
			   	$stat = '<i class="fa fa-close"></i>' ;
			   	$count =  "";
			   	foreach ($this->model_credit_amt->query("SELECT * from matrix_downline_ref where income_id='$use_id' and level='5'")->result() as $key => $value) {
			   				foreach ($this->model_credit_amt->query("SELECT * from user_registration where  user_id='".$value->down_id."'")->result() as $key => $value1) {
			   					
			   					 foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value1->ref_id."'")->result() as $key => $value2) {
			   					 			
			   					 			foreach ($this->model_credit_amt->query("select * from lifejacket_subscription where user_id='".$value1->user_id."' ")->result() as $key => $value3) {

													$count = $value3->status;
													
													
        			                                if( $count =='Active'){
			   					 				        	$stat = '<i class="fa fa-check"></i>';
        			   					 			}
        			   					 		    else if( $count !='Active') {
        			   					 					$stat = '<i class="fa fa-close"></i>';
        			   					 			}
        			   		
			   					 			}
			   					 			
			   					 				 		 if($value1->user_rank_name=='Normal User') {
        			   					 			        	$stat = '<i class="fa fa-close"></i>';
        			   					 			    }
			   					 			$i +=1;
			   					 			array_push($data,
					   									[
					   											$i,
						   										$value1->user_id,
						   										$value1->first_name . ' '. $value1->last_name,
						   										$value1->email,
						   										$value1->ref_id .'-' . $value2->first_name .' '. $value2->last_name,
						   										$value1->ts,
						   										$stat
					   									]) ;
			   					 }
			   					
			   				}
			   	}
			   	  	$output = array(
													"data" => $data,
												);  

			echo json_encode($output);

	}

	public function level6(){
				$i=0;	
			   	$this->is_logged_in();
			   	$use_id =$this->auth_user_id;
			   	$data = array();
			   	$stat = '<i class="fa fa-close"></i>' ;
			   	$count =  "";
			   	foreach ($this->model_credit_amt->query("SELECT * from matrix_downline_ref where income_id='$use_id' and level='6'")->result() as $key => $value) {
			   				foreach ($this->model_credit_amt->query("SELECT * from user_registration where  user_id='".$value->down_id."'")->result() as $key => $value1) {
			   					
			   					 foreach ($this->model_credit_amt->query("select * from user_registration where user_id='".$value1->ref_id."'")->result() as $key => $value2) {
			   					 			
			   					 			foreach ($this->model_credit_amt->query("select * from lifejacket_subscription where user_id='".$value1->user_id."' ")->result() as $key => $value3) {

													$count = $value3->status;
													
													
													
        			                                if( $count =='Active'){
			   					 				        	$stat = '<i class="fa fa-check"></i>';
        			   					 			}
        			   					 		    else if( $count !='Active') {
        			   					 					$stat = '<i class="fa fa-close"></i>';
        			   					 			}
        			   		
			   					 			}
			   					 			
			   					 				 		 if($value1->user_rank_name=='Normal User') {
        			   					 			        	$stat = '<i class="fa fa-close"></i>';
        			   					 			    }
			   					 			$i +=1;
			   					 			array_push($data,
			   									[
			   											$i,
				   										$value1->user_id,
				   										$value1->first_name . ' '. $value1->last_name,
				   										$value1->email,
				   										$value1->ref_id .'-' . $value2->first_name .' '. $value2->last_name,
				   										$value1->ts,
				   										$stat
			   									]) ;
			   					 }
			   					
			   				}
			   	}
			   	  	$output = array(
									"data" => $data,
									);  

			echo json_encode($output);

	}

	//END OF DOWNLINE MEMBER TREE

	//Downline withdarwals
	public function  downlineWithdrawalView() {
			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;

				$data 		=		[];
				$userinfo 	= 		[];
						$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/downline_withdrawal',$data);
			}
			else {
				redirect('login');
			}

	}

	public function searchRequest() {

				$this->is_logged_in();

			   	$use_id = $this->auth_user_id;
			   	$query  = "";
			   	$type 	= $this->input->post('type');
			   	$df 	=  date('Y-m-d',strtotime($this->input->post('dateFrom')));
			   	$dt 	=  date('Y-m-d',strtotime( $this->input->post('dateTo')))	;
			   	$data 	= [] ;
			   	$i      = 0 ;

			   if($type =="all") {
					   	foreach ($this->model_credit_amt->query("SELECT down_id from matrix_downline_ref where income_id='$use_id'")->result() as $key => $value) {
					   			 foreach ($this->model_credit_amt->query("SELECT user_registration.user_id,user_registration.first_name,user_registration.last_name,withdraw_request.request_amount,withdraw_request.trans_type,withdraw_request.posted_date,withdraw_request.admin_remark from withdraw_request JOIN user_registration ON withdraw_request.user_id=user_registration.user_id where withdraw_request.user_id='".$value->down_id."' and  withdraw_request.posted_date BETWEEN '".$df."' AND '".$dt."'")->result() as $key => $value1) {
					   			 			
					   			 			$i +=1;

					   			 			array_push($data,
					   			 							[
					   			 								"id"		=> 	$i,
					   			 								"user_id" 	=> 	$value1->user_id,
					   			 								"fullname"	=>	$value1->first_name .' '.$value1->last_name,
					   			 								"amount"	=>	'$ ' .$value1->request_amount,
					   			 								"amount_inr" => '₹ ' .$value1->request_amount * 60,
					   			 								"trans_type"=>	$value1->trans_type,
					   			 								"date"		=>	date('F d, Y',strtotime($value1->posted_date)),
					   			 								"remark"	=>	$value1->admin_remark
					   			 							]
					   			 						);
									 	
									 }			
					   	} 	
				}
				else {	
						foreach ($this->model_credit_amt->query("SELECT down_id from matrix_downline_ref where income_id='$use_id'")->result() as $key => $value) {
								foreach ($this->model_credit_amt->query("SELECT user_registration.user_id,user_registration.first_name,user_registration.last_name,withdraw_request.request_amount,withdraw_request.trans_type,withdraw_request.posted_date,withdraw_request.admin_remark from withdraw_request JOIN user_registration ON withdraw_request.user_id=user_registration.user_id where withdraw_request.user_id='".$value->down_id."' and  withdraw_request.posted_date BETWEEN '".$df."' AND '".$dt."' AND withdraw_request.trans_type='".$type."'")->result() as $key => $value1) {

					   			 			$i +=1;
					   			 			array_push($data,
					   			 							[
					   			 								"id"		=> 	$i,
					   			 								"user_id" 	=> 	$value1->user_id,
					   			 								"fullname"	=>	$value1->first_name .' '.$value1->last_name,
					   			 								"amount"	=>	'$ ' .$value1->request_amount,
					   			 								"amount_inr" => '$ ' .$value1->request_amount * 60,
					   			 								"trans_type"=>	$value1->trans_type,
					   			 								"date"		=>	date('F d, Y',strtotime($value1->posted_date)),
					   			 								"remark"	=>	$value1->admin_remark
					   			 							]
					   			 						);
								}	
					   	}
				}
			
			$output= ['data'=>$data];
		echo json_encode($output);	   	

	}
	//End of donwline withdrawals
    public function searchQualifier() {
				$this->is_logged_in();
			   	$use_id = $this->auth_user_id;
			   	$query  = "";
			   	$type 	= $this->input->post('type');
			   	$df 	=  date('Y-m-d',strtotime($this->input->post('dateFrom')));
			   	$dt 	=  date('Y-m-d',strtotime( $this->input->post('dateTo')))	;
			   	$data 	= [] ;
			   	$i      = 0 ;
			   	$t1 	= 0;
			   	foreach ($this->model_credit_amt->query("select user_id,first_name,last_name,username,registration_date from user_registration where ref_id='$use_id'")->result() as $key => $value1) {
                     					$row = array();
                     					$t1=0; 
										$totaffl=0;
									    $te1=0;
									    $te_nov1=0;
									    $te_nov123=0;
									    $i +=1;
                     				foreach ($this->model_credit_amt->query("select down_id from matrix_downline_ref where income_id='".$value1->user_id."'")->result() as $key => $value2) {
                     							foreach ($this->model_credit_amt->query("Select sum(amount) as tot1_nov from lifejacket_subscription where user_id='".$value2->down_id."' and (date between '$df' and '$dt')")->result() as $key => $value3) {
													$te_nov=$value3->tot1_nov;
													$te_nov1+=$te_nov;
											}	
					
                     					}
                     						foreach ($this->model_credit_amt->query("select sum(amount) as tot_no from lifejacket_subscription where  user_id='".$value1->user_id."' and (date between '$df' and '$dt')")->result() as $key => $value4) {

												  $te_nov123= $te_nov1+$value4->tot_no;
												
											}
                     							array_push($data,
					   			 							[
					   			 								"id"		=> 	$i,
					   			 								"username"	=> $value1->username,
					   			 								"user_id" 	=> 	$value1->user_id,
					   			 								"fullname"	=>	$value1->first_name .' '.$value1->last_name,
					   			 								"amount"	=>	'$ ' .$te_nov123,
					   			 								"date"	=>   date('F d, Y',strtotime($value1->registration_date))
					   			 							]
					   			 						);
                     				}
                     				
            $output= ['data'=>$data];
            echo json_encode($output);			

    }
    public function directMemberViewDonwline(){
			if($this->is_logged_in()) {
				$use_id =$this->auth_user_id;
				$this->session->set_userdata(['down_user'=>$this->uri->segment(3)]);
				$data 		=		[];
				$userinfo 	= 		[];
				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {
							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
				return $this->load->view('users/downline_report',$data);
			}
			else {
				redirect('login');
			}
	}
	public function getDirectMemberSponsor(){

				
			   	$this->is_logged_in();
			   	$data = array();
                   
			   	$use_id =	$this->session->userdata('down_user');
			   	   
                     $username= "";
                     $fullbname = "";
                     $rank = "";
                     $regdate = "";

                     $ksm;
   $i=0;
	
                     	foreach ($this->model_credit_amt->query("select * from matrix_downline_ref where income_id='$use_id'")->result() as $key => $value1) {
                     					$row = array();
                     				
                                
                     				$t1=0;
                     				foreach ($this->model_credit_amt->query("select * from user_registration where  user_id='".$value1->down_id."'")->result() as $key => $value2) {
                     						$username = $value2->username;
                     						$fullbname = $value2->first_name .' '. $value2->last_name;
                     							$rank = $value2->user_rank_name;
                     						$regdate = $value2->registration_date;				
                     						

                     				}
                     				foreach ($this->model_credit_amt->query("select sum(amount) as domamts from lifejacket_subscription where user_id='".$value1->down_id."'")->result() as $key => $amounts) {
                     							 $ksm=$amounts->domamts;

                     							  if($ksm=='') { 
													 		$ksm=0; 
													 } else { 
													 		$ksm=$ksm; 
													 } 
                     				}
                     						$i +=1;

                     						$row  [] = $i;
                     						$row  [] = $value1->down_id;
                     						$row  [] = $username;  
                     						$row  [] = $fullbname;
                     						$row  [] = $rank;
                     						$row  [] = '$ ' . number_format($ksm,2);
                     						$row  [] = $value1->level;
                     						$row  [] = date('F d, Y',strtotime($regdate));
         

                     				$data[] = $row;	
                     		}
					   	
						   		$output = array(
													"data" => $data,
												);  

			echo json_encode($output);

	}
  
	


}