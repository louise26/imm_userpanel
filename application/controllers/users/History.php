<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  History  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		
     


	}

	
		public function index(){


			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;



				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				$rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,

										];
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,

								];
					return $this->load->view('users/history',$data);

			}
			else {

				redirect('login');
			}
		}



		public function searchHistory() {

				$this->is_logged_in();
				$userid =$this->auth_user_id;


				$types 	= $this->input->post('ttype');
			   	$df 	=  date('Y-m-d',strtotime($this->input->post('df')));
			   	$dt 	=  date('Y-m-d',strtotime( $this->input->post('dt')))	;
			   	$data 	= [] ;
			  
			   	$i = 0 ;
			   	$sponsor = "";
			   	$status = "" ;
			   	$sender = "";
			   if($types !="all") {


							$this->db->cache_off();
							foreach ($this->model_credit_amt->query("SELECT * from credit_debit where user_id='$userid'  AND receive_date BETWEEN '$df' AND '$dt'  AND  ttype='$types'")->result() as $key => $value) {
								$this->db->cache_off();
					   			foreach ($this->model_credit_amt->query("SELECT * from user_registration where user_id= '".$value->sender_id."'")->result() as $key => $value1) {
					   					$i +=1;

						   				 if($value->ttype=="Coin Purchase"){
						   				 		foreach ($this->model_credit_amt->query("SELECT sponsor from lifejacket_subscription_coin where invoice_no='".$value->invoice_no."'")->result() as $key => $value2) {
						   				 				$sponsor = $value2->sponsor;
						   								
						   						}
						   				 }

						   				 if($value->sender_id !="123456"){

						   				 	$sender = $value->sender_id;
						   				 }
						   				 else {
						   				 			$sender ="IMM2223334";

						   				 }

						   				 if($value->status==0){
						   				 				$status = "Paid";
						   				 }
						   				 else {
						   				 	$status ='Unpaid';
						   				 }
						   				 array_push($data,[
						   				 
						   				 						'id' 				=> $i,
						   				 						'transaction_no'	=> $value->transaction_no,
						   				 						'name'				=>  $value1->first_name .' '. $value1->last_name,
						   				 						'sender'			=> $sender,
						   				 						'credit_amt'		=> $value->credit_amt,
						   				 						'debit_amt'			=> $value->debit_amt,
						   				 						'ttype'				=> $value->ttype,
						   				 						'remark'			=> $sponsor .' '. $value->ttype . ' to ' .$value->receiver_id  ,
						   				 						'date'				=> date('F d, Y',strtotime($value->receive_date)),
						   				 						'status'			=> $status
						   				 					]);	
					   			}

					   		}
				}
				else if( $types=="all") {	

									$this->db->cache_off();
					   		foreach ($this->model_credit_amt->query("SELECT * from credit_debit where user_id='$userid' AND  receive_date BETWEEN '$df' AND '$dt'  order by id desc ")->result() as $key => $value) {
					   			$this->db->cache_off();
					   			foreach ($this->model_credit_amt->query("SELECT * from user_registration where user_id= '".$value->sender_id."'")->result() as $key => $value1) {
					   					$i +=1;

						   				 if($value->ttype=="Coin Purchase"){
						   				 		foreach ($this->model_credit_amt->query("SELECT sponsor from lifejacket_subscription_coin where invoice_no='".$value->invoice_no."'")->result() as $key => $value2) {
						   				 				$sponsor = $value2->sponsor;
						   								
						   						}
						   				 }
						   				 if($value->sender_id !="123456"){

						   				 	$sender = $value->sender_id;
						   				 }
						   				 else {
						   				 			$sender ="IMM2223334";

						   				 }

						   				 if($value->status==0){
						   				 				$status = "Paid";
						   				 }
						   				 else {
						   				 	$status ='Unpaid';
						   				 }
						   				 array_push($data,[
						   				 
						   				 						'id' 				=> $i,
						   				 						'transaction_no'	=> $value->transaction_no,
						   				 						'name'				=> $value1->first_name .' '. $value1->last_name,
						   				 						'sender'			=> $sender,
						   				 						'credit_amt'		=> $value->credit_amt,
						   				 						'debit_amt'			=> $value->debit_amt,
						   				 						'ttype'				=> $value->ttype,
						   				 						'remark'			=> $sponsor .' '. $value->ttype . ' to ' .$value->receiver_id  ,
						   				 						'date'				=> date('F d, Y',strtotime($value->receive_date)),
						   				 						'status'			=> $status
						   				 					]);		
					   			}
					   		}
	   		
				}
			
			
	$output= ['data'=>$data];

			echo json_encode($output);	   	


		}

		public function addfund() {


			if($this->is_logged_in()){

				$use_id =$this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
										];

						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
								];
						return $this->load->view('users/add_fund',$data);
			}
			else {
					redirect('login');
			}
	}

	public function add_fund_report() {

				$this->is_logged_in();
				$userid = $this->auth_user_id;
				$action = "";
				$data = [] ;

					$i = 0 ;

				$this->db->cache_off();
				foreach ($this->model_credit_amt->query("SELECT * from add_fund_request where user_id='$userid'  order by id desc")->result() as $key => $value) {


						$i +=1;

						if($value->status=="Pending"){
									$action ='<form name="info" method="post" action="update" >
									       <input type="hidden" name="imm_token" value="'.$this->security->get_csrf_hash().'">
                                 		<input type="hidden" name="cap_id" value="'.$value->id.'">
                                 			<input type="text" name="transaction_no" class="form-control" required value="" placeholder="Enter Transaction ID/No">
                                 		<button class="btn btn-primary form-control" type="submit" id="btn_submit">Submit</button>
                                 	</form>';
						}
						else {
							$action = $value->status;
						}
						array_push($data,
										[
											$i,
											'$ '. $value->amount,
											$value->paymode,
											$value->wallet_type,
											date('F d, Y',strtotime($value->posted_date)),
											$action,
											$value->status
										]
									);
				}

			$output = ['data' =>$data];

			echo json_encode($output);

		}



		public function updateTransaction() {

			$id = $this->input->post('cap_id');
			$tx = $this->input->post('transaction_no');



				$data = [];


	

				$this->db->cache_off();

					if($this->model_add_fund_request->update(['reciept_update_date'=>date('Y-m-d'),'reciept_no'=>$tx,'status'=>'Waiting for approval'],['id'=>$id])){

							$data = [
											'title' 	=>'Good Job !',
											'status' 	=>'success',
											'msg'		=> 'Transaction no successfully submitted',
											'value'		=> $id

										];

										$this->session->set_userdata(['alert'=>'Successfully updated','type'=>'success']);
					}
					else {

							$this->session->set_userdata(['alert'=>'Unable to process right now','type'=>'danger']);
					}



				redirect('history/funds');


		}


}