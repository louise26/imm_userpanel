<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  Contact  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
     

	}

	public function index() {
			if($this->is_logged_in()) {
				
				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		         $this->db->cache_off();
		         $rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->email,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
											't_code'		=> 	$value->t_code,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									
									'rwallet_balance' 	=> $this->model_final_reg_wallet->select('amount',['user_id'=>$use_id]),
										
								];
				return $this->load->view('users/contact',$data);
			}
			else {
				redirect('login');
			}


	}

	public function sendMessage() {

				$email		 = $this->input->post('email');
				$subject 	 = $this->input->post('subject');
				$department  = $this->input->post('department');
				$message 	 = $this->input->post('message');
				$data 		 = [] ;
      
      
            	$from = 'support@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders  Support <$from>" . "\r\n";

				 $msg = '<!doctype html>
		          <html>
		          <head>
		              <meta charset="utf-8">
		              <title>Account Credential</title>
		              <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
		              <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
		          </head>
		          <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
		              <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
		                  <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
		                      <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
		                          <img src="http://immtradersclub.com/images/logo.png" style="margin:0 0 20px 0;  "><br/><br/>
		                          <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
		          line-height: 30px;padding: 18px 0px 12px;background-color: #337ab7; font-family: Arial, cursive;">
		                            CONTACT SUPPORT
		                          </div>
		                          <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 15px;line-height: 35px; margin-top: 13px;">
		                              <table>
		                              	<tr>
		                              		<td align="left">From   :</td><td align="left">'.$email.'</td>
		                              	</tr>
		                              	<tr>
		                              		<td align="left">To   :</td><td align="left">'.$department.' Department</td>
		                              	</tr>
		                              </table>
		                          </div>
		                          <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
		                          </div>
		                          <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 20px;line-height: 23px;
		          color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
		                              <p style="margin: 0px !important;">'.$message.'</p>
		                          </div>
		                          <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
		                                 
		                            </div>
		                          <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
		                            Copyrights 2018 Immtradersclub. All Rights Reserved. </p>
		                      </div>
		                  </div>
		              </div>
		              </div><br/><br/>
		          </body>
		          </html>';

   

				if(  mail ( 'immtradersclub@gmail.com', $subject, $msg, $headeruser1 )){

						$data =  [
									'title'		=> 'Good Job !',
									'msg'		=> 'Your message has been sent.',
									'status'	=> 'success'
								];

				}
				else {


								$data =  [
												'title'		=> 'Oops !',
												'msg'		=> 'Something went wrong. Please try again',
												'status'	=> 'error'
											];

				}


				echo json_encode($data);


	}

}