<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  Rwallet  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_transfer_withdraw_history');

       

	}

	public function index() {
			if($this->is_logged_in()) {
				
				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		        $this->db->cache_off();
		        $rankss = "" ;
				foreach($this->model_users->query("Select move_rank from rank_achiever where user_id='$use_id' AND id=(Select max(id) from rank_achiever where user_id='$use_id')")->result() as $key => $rank) {
				    $rankss = $rank->move_rank;
				}
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$rankss,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
											't_code'		=> 	$value->t_code,
										];
						}
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									
									'rwallet_balance' 	=> $this->model_final_reg_wallet->select('amount',['user_id'=>$use_id]),
										
								];
				return $this->load->view('users/rwallet',$data);
			}
			else {
				redirect('login');
			}

	}

	public function searchUser(){

				$this->is_logged_in();
				$use_id 	= $this->auth_user_id;


				$user = $this->input->post('user');
				$data = [] ;
				$email = " " ;
				

				


				if($this->model_users->query("Select email,first_name,last_name,user_id from user_registration where user_id='$user' OR email='$user'")->result()) {

						foreach ($this->model_users->query("Select email,first_name,last_name,user_id from user_registration where user_id='$user' OR email='$user'")->result() as $key => $value) {
							$email = $value->email;
							
							array_push($data,
											[
												'email' 		=>	$value->email,
												'user_id' 		=>	$value->user_id,
												'name' 			=> 	$value->first_name . ' ' .$value->last_name,
											]
									);
						}
				}
				else {
								array_push($data,
													[
														'email' => 'User not found !',
														'name' 	=> 	'',
														'user_id' => ''
													]
										);

				}

				echo json_encode($data);
				
	}

	public function transferRwallet(){

		$this->is_logged_in();
		$use_id 	= $this->auth_user_id;

		$data	= [] ;
		$t_code       		=	$this->input->post('t_code') ;  
        $password 	  		=	$this->input->post('password');       
        $sender_id 	  		=	$this->input->post('sender_id');      
        $sender_name  		=	"";      
        $amount       		=	$this->input->post('amount');    
        $sender_balance     =	$this->input->post('balance');   
		$receiver_id  		=	$this->input->post('receiver_id');    
        $receiver_name 		=	$this->input->post('receiver_name');  

        $rand=rand(0000000001,9999999999);

        $date 	= date("Y-m-d");
        $receiver_wallet 	= 0 ;
        //$sender_wallet 		= $sender_balance - $amount ; 
       
       	$receiveid = "" ;


        $senderemail  		= "" ;
        $receiveremail 		= "" ;

        $rwalletbalance    = 0;

         $this->db->cache_off();
         foreach ($this->model_users->query("Select user_id from user_registration where user_id='$receiver_id' OR email='$receiver_id' OR username='$receiver_id'")->result() as $key => $value) {
         			$receiveid = $value->user_id;
         }
        foreach ($this->model_users->select('email,first_name,last_name',['user_id'=>$use_id]) as $key => $value) {
        		$senderemail = $value->email;
        		   $sender_name = $value->first_name .' ' . $value->last_name ;
        }
         $this->db->cache_off();
          foreach ($this->model_users->query("Select email from user_registration where user_id='$receiver_id' OR email='$receiver_id' OR username='$receiver_id'")->result() as $key => $value) {
        		$receiveremail = $value->email;
        }


        
        $ttype="Fund Transfer";
    	$msg='Withdrawal Wallet';
          
    	$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

    		  $this->db->cache_off();
        	foreach ($this->model_final_reg_wallet->select('amount',['user_id'=>$receiveid]) as $key => $value) {
						        					$receiver_wallet = $value->amount;
	
						      }

			  $this->db->cache_off();
        	foreach ($this->model_final_reg_wallet->select('amount',['user_id'=>$use_id]) as $key => $value) {
						        					$rwalletbalance = $value->amount;
	
						      }

        		if($receiver_id == $use_id){
        					
        							$data =  [
												'title'		=> 'Oops !',
												'msg'		=> 'Not allowed to transfer to self',
												'status'	=> 'error'
											];
        		}

        		else {
        				if($t_code == $password) {

						        	$receiver_data   = [
							        						'user_id'			=> $receiveid,
							        						'receiver_id'			=> $receiveid,
							        						'sender_id'			=> $use_id,
							        						'credit_amt'		=> $amount,
							        						'admin_charge'		=> '0',
							        						'receive_date'		=> $date ,
							        						'TranDescription'	=> 'Transfer fund by '.$sender_name.' to '.$receiveid,
							        						'Remark'			=> 'Transfer fund by '.$sender_name.' to '.$receiveid,
							        						'Cause'				=> 'Transfer fund by '.$sender_name.' to '.$receiveid,
							        						'transaction_no'	=> $rand,
							        						'invoice_no'		=> $rand,
							        						'ttype'				=> $ttype,
							        						'product_name'		=> $ttype,
							        						'ewallet_used_by'	=> $msg,
							        						'current_url'		=> $urls,
							        						'debit_amt'			=> '0',
							        						'status'			=> '0'

						        					];
							        $sender_data   = [
								        						'user_id'			=> $use_id,
								        						'debit_amt'			=> $amount,
								        						'receiver_id'		=> $receiveid,
							        						    'sender_id'			=> $use_id,
								        						'admin_charge'		=> '0',
								        						'receive_date'		=> $date,
								        						'TranDescription'	=> 'Transfer fund $'.$amount.' to '.$receiveid.'with 0% service charge',
								        						'Remark'			=> 'Transfer fund $'.$amount.' to '.$receiveid.'with 0% service charge',
								        						'Cause'				=> 'Transfer fund by '.$sender_name.' to '.$receiveid,
								        						'transaction_no'	=> $rand,
								        						'invoice_no'		=> $rand,
								        						'ttype'				=> $ttype,
								        						'product_name'		=> $ttype,
								        						'ewallet_used_by'	=> $msg,
								        						'current_url'		=> $urls,
								        						'credit_amt'		=> '0',
								        						'status'			=> '0'
							        				];
							        				
                                    if($amount >=10) {
        							        	if( $rwalletbalance > $amount){
        
        
        							        				    $wallet_final =  $receiver_wallet + $amount;
        							        				 
        							        				     $sender_wallet = $rwalletbalance - $amount ; 
        
        											        	 $this->model_credit_amt->insert($receiver_data);
        											        	 $this->model_final_reg_wallet->update(['amount'=>$wallet_final],['user_id'=>$receiveid]);
        											        	 $this->sendEmailToReceiever($receiver_name,$receiveremail,$amount,$sender_name,$sender_id);
        
                                                                $this->model_transfer_withdraw_history->insert([
            										                                                    'user_id' => $sender_id,
            										                                                    'action'  => $sender_id .'rwallet to '. $receiveid .'rwallet',
            										                                                    'new_balance'  => $sender_wallet,
            										                                                    'current_balance' => $rwalletbalance,
            										                                                    'link'  => $urls
            										                                                ]);
            										           $this->model_transfer_withdraw_history->insert([
            										                                                    'user_id' => $receiveid,
            										                                                    'action'  => $sender_id .'rwallet to '. $receiveid .'rwallet',
            										                                                    'new_balance'  => $wallet_final,
            										                                                    'current_balance' => $receiver_wallet,
            										                                                    'link'  => $urls
            										                                                ]);
        
        												          	 $this->model_credit_amt->insert($sender_data);
        											        		 $this->model_final_reg_wallet->update(['amount'=>$sender_wallet],['user_id'=>$sender_id]);
        											        		 $this->sendEmailToSender($sender_name,$senderemail,$amount,$receiver_name,$receiver_id);
        											        		 $data =  [
        																	'title'		=> 'Good Job !',
        																	'msg'		=> 'You have scuccessfully transfered your fund',
        																	'status'	=> 'success'
        																];
        
        							        	}
        
        							        	else {
        
        							        			$data =  [
        												'title'		=> 'Oops !',
        												'msg'		=> 'Not enough balance in your wallet',
        												'status'	=> 'error'
        										];
        
        							        	}
                                    }
							       else {
							           
							           
							           $data =  [
																	'title'		=> 'Oops!',
																	'msg'		=> 'Minimum Fund Transfer is $10',
																	'status'	=> 'error'
																];
							       }

						        


        				}
        				else {
        						$data =  [
												'title'		=> 'Oops !',
												'msg'		=> 'Wrong Transaction Password',
												'status'	=> 'error'
										];
        				}
        		}

        	echo json_encode($data);

	}



	public function sendEmailToSender($usrename,$email,$amount,$receiver_name,$userid){


            	$from = 'info@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders | Fund Transfer <$from>" . "\r\n";
			 $msg = '<!doctype html>
            <html>
            <head>
                <meta charset="utf-8">
                <title>Account Credential</title>
                <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
                <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
            </head>
            <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
                <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
                    <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
                        <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
                            <img src="http://immtradersclub.com/images/logo.png" style="margin:0 0 0 0; height:100px ;  "><br/><br/>
                            <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
            line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
                               Fund Transfer Notification
                            </div>
                            <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
                                Dear '.$usrename.',
                            </div>
                            <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
                            </div>
                            <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
            color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
                                <p style="margin: 0px !important;">You have transferred an amount of  $'.$amount.' to '.$receiver_name.' with userid : '.$userid.'  from your IMM account . To see the details, login to your account and check.</p>
                            </div>
                            <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
                                   <h4>Secure Login URL: http://immtradersclub.com/member/login </h4>
                                <h3>ACCOUNT NOTIFICATIONS</h3>
                                <p>To ensure that you receive all our notifications, we recommend that you give your valid email address and check your email on regular basis.</p>
                              </div>
                            <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
                              Copyrights 2016 Immtradersclub. All Rights Reserved. </p>
                        </div>
                    </div>
                </div>
                </div><br/><br/>
            </body>
            </html>';

				$subject = "Fund Transfer";
				mail ( $email, $subject, $msg, $headeruser1 );
	}

	public function sendEmailToReceiever($usrename,$email,$amount,$fromsender,$userid){
            
            	$from = 'info@immtradersclub.com';
            	$headeruser1="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.="Mime-Version: 1.0\r\n";
                $headeruser1.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headeruser1.= "From:iMM-Traders | Fund Transfer <$from>" . "\r\n";
				 $msg = '<!doctype html>
		          <html>
		          <head>
		              <meta charset="utf-8">
		              <title>Account Credential</title>
		              <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
		              <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
		          </head>
		          <body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
		              <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
		                  <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
		                      <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
		                          <img src="http://immtradersclub.com/images/logo.png" style="margin:0 0 20px 0;  "><br/><br/>
		                          <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
		          line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
		                             Fund Transfer Notification
		                          </div>
		                          <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
		                              Dear '.$usrename.',
		                          </div>
		                          <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
		                          </div>
		                          <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
		          color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
		                              <p style="margin: 0px !important;">'.$fromsender.' with userid : '.$userid.'  has transferred  $'.$amount.' to your IMM account. To see the details, login to your account and check.</p>
		                          </div>
		                          <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">
		                                 <h4>Secure Login URL: https://immtradersclub.com/member/login </h4>
		                              <h3>ACCOUNT NOTIFICATIONS</h3>
		                              <p>To ensure that you receive all our notifications, we recommend that you give your valid email address and check your email on regular basis.</p>
		                            </div>
		                          <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
		                            Copyrights 2018 Immtradersclub. All Rights Reserved. </p>
		                      </div>
		                  </div>
		              </div>
		              </div><br/><br/>
		          </body>
		          </html>';

					$subject = "Fund Transfer";
					
				    mail ( $email, $subject, $msg, $headeruser1 );

	}


}
