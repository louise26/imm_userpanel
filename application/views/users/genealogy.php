<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#img-upload{
    width: 50%;
    position: center;       
}
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">MEMBERS</h4>
                            <p>MY GENEALOGY</p>
                            <p>Upliner  Id  :<span>  <?=$info['upline_id']?></span></p>
                            <p>Upliner Name : <span>  <?=$info['upline_name']?></span> </p>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            

                                                <div class="row">
                                                            
                                                              <div class="col-lg-12">
                                                    <ul class="nav nav-tabs navtab-bg nav-justified">
                                                        <li class="active">
                                                            <a href="#level1" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-home"></i></span>
                                                                <span class="hidden-xs">Level 1</span>
                                                            </a>
                                                        </li>
                                                        <li class="" onclick="getLevel2()">
                                                            <a href="#level2" data-toggle="tab" aria-expanded="true">
                                                                <span class="visible-xs"><i class="fa fa-user"></i></span>
                                                                <span class="hidden-xs">Level 2</span>
                                                            </a>
                                                        </li>
                                                        <li class="" onclick="getLevel3()">
                                                            <a href="#level3" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                                                <span class="hidden-xs">Level 3</span>
                                                            </a>
                                                        </li>
                                                        <li class="" onclick="getLevel4()">
                                                            <a href="#level4" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                                                <span class="hidden-xs">Level 4</span>
                                                            </a>
                                                        </li>
                                                        <li class="" onclick="getLevel5()">
                                                            <a href="#level5" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                                                <span class="hidden-xs">Level 5</span>
                                                            </a>
                                                        </li>
                                                        <li class="" onclick="getLevel6()">
                                                            <a href="#level6" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                                                <span class="hidden-xs">Level 6</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="level1">
                                                                    
                                                                    <table id="direct-income" class="table table-striped table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Member ID</th>
                                                                            <th>Fullname</th>
                                                                            <th>Email</th>
                                                                            <th>Introducer</th>
                                                                            <th>Date Joined</th>
                                                                            <th>Active</th>
                 
                                                                        </tr>
                                                                        </thead>
                                                                    </table>
                                                        </div>
                                                        <div class="tab-pane " id="level2">
                                                               <table id="direct-income1" class="table table-striped table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Member ID</th>
                                                                            <th>Fullname</th>
                                                                            <th>Email</th>
                                                                            <th>Introducer</th>
                                                                            <th>Date Joined</th>
                                                                            <th>Active</th>
                 
                                                                        </tr>
                                                                        </thead>
                                                                    </table>
                                                        </div>
                                                        <div class="tab-pane" id="level3">
                                                          <table id="direct-income2" class="table table-striped table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Member ID</th>
                                                                            <th>Fullname</th>
                                                                            <th>Email</th>
                                                                            <th>Introducer</th>
                                                                            <th>Date Joined</th>
                                                                            <th>Active</th>
                 
                                                                        </tr>
                                                                        </thead>
                                                                    </table>
                                                        </div>
                                                        <div class="tab-pane" id="level4">
                                                            <table id="direct-income3" class="table table-striped table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Member ID</th>
                                                                            <th>Fullname</th>
                                                                            <th>Email</th>
                                                                            <th>Introducer</th>
                                                                            <th>Date Joined</th>
                                                                            <th>Active</th>
                 
                                                                        </tr>
                                                                        </thead>
                                                                    </table>
                                                        </div>
                                                        <div class="tab-pane" id="level5">
                                                           <table id="direct-income4" class="table table-striped table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Member ID</th>
                                                                            <th>Fullname</th>
                                                                            <th>Email</th>
                                                                            <th>Introducer</th>
                                                                            <th>Date Joined</th>
                                                                            <th>Active</th>
                 
                                                                        </tr>
                                                                        </thead>
                                                                    </table>
                                                        </div>
                                                        <div class="tab-pane" id="level6">
                                                            <table id="direct-income5" class="table table-striped table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Member ID</th>
                                                                            <th>Fullname</th>
                                                                            <th>Email</th>
                                                                            <th>Introducer</th>
                                                                            <th>Date Joined</th>
                                                                            <th>Active</th>
                 
                                                                        </tr>
                                                                        </thead>
                                                                    </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>

                                        </div>
                                    </div>
                                </div>

                            </div> <!-- End Row -->


                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                    <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>

<script>
    $(document).ready(function() {
        $('#direct-income').DataTable({
            "ajax": 'direct-members-level1',    
             dom: "Bfrtip",
            buttons: [{
                extend: "copy",
                className: "btn-success"
            }, {
                extend: "csv"
            }, {
                extend: "excel"
            }, {
                extend: "pdf"
            }, {
                extend: "print"
            }],
            "language": {
                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                  },
            responsive: !0
        });


        
    });

    function getLevel2(){
                 $('#direct-income1').DataTable().destroy();
                 $('#direct-income1').DataTable({
                    "ajax": 'direct-members-level2',    
                     dom: "Bfrtip",
                    buttons: [{
                        extend: "copy",
                        className: "btn-success"
                    }, {
                        extend: "csv"
                    }, {
                        extend: "excel"
                    }, {
                        extend: "pdf"
                    }, {
                        extend: "print"
                    }],
                    "language": {
                             "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                          },
                    responsive: !0
                });

        }
        function getLevel3(){
                   $('#direct-income2').DataTable().destroy();
                 $('#direct-income2').DataTable({
                    "ajax": 'direct-members-level3',    
                     dom: "Bfrtip",
                    buttons: [{
                        extend: "copy",
                        className: "btn-success"
                    }, {
                        extend: "csv"
                    }, {
                        extend: "excel"
                    }, {
                        extend: "pdf"
                    }, {
                        extend: "print"
                    }],
                    "language": {
                             "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                          },
                    responsive: !0
                });

        }
        function getLevel4(){
                  $('#direct-income3').DataTable().destroy();
                 $('#direct-income3').DataTable({
                    "ajax": 'direct-members-level4',    
                     dom: "Bfrtip",
                    buttons: [{
                        extend: "copy",
                        className: "btn-success"
                    }, {
                        extend: "csv"
                    }, {
                        extend: "excel"
                    }, {
                        extend: "pdf"
                    }, {
                        extend: "print"
                    }],
                    "language": {
                             "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                          },
                    responsive: !0
                });

        }

        function getLevel5(){
                   $('#direct-income4').DataTable().destroy();
                     $('#direct-income4').DataTable({
                            "ajax": 'direct-members-level5',    
                             dom: "Bfrtip",
                            buttons: [{
                                extend: "copy",
                                className: "btn-success"
                            }, {
                                extend: "csv"
                            }, {
                                extend: "excel"
                            }, {
                                extend: "pdf"
                            }, {
                                extend: "print"
                            }],
                            "language": {
                                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                                  },
                            responsive: !0
                        });

        }

        function getLevel6(){

                 $('#direct-income5').DataTable().destroy();
                 $('#direct-income5').DataTable({
                    "ajax": 'direct-members-level6',    
                     dom: "Bfrtip",
                    buttons: [{
                        extend: "copy",
                        className: "btn-success"
                    }, {
                        extend: "csv"
                    }, {
                        extend: "excel"
                    }, {
                        extend: "pdf"
                    }, {
                        extend: "print"
                    }],
                    "language": {
                             "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                          },
                    responsive: !0
                });

        }
</script>

<?php $this->view('users/body_footer')?>