<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
                      position: relative;
                      overflow: hidden;
                  }
                  .btn-file input[type=file] {
                      position: absolute;
                      top: 0;
                      right: 0;
                      min-width: 100%;
                      min-height: 100%;
                      font-size: 100px;
                      text-align: right;
                      filter: alpha(opacity=0);
                      opacity: 0;
                      outline: none;
                      background: white;
                      cursor: inherit;
                      display: block;
                  }
                  #img-upload{
                      width: 50%;
                      position: center;       
                  }
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">DEPOSIT FUND SECTION</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row">   
                                                    <div class="col-md-4">   
                                                    </div>
                                                    <div class="col-md-4">
                                                            <!-- <form name="depositForm">  -->
                                                               
                                                            <!--   <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">-->
                                                            <!--    <div class="form-group">   -->
                                                            <!--            <label> AMOUNT TO DEPOSIT</label>-->
                                                            <!--            <input type="number" step="any" min="10" name="amount" class="form-control" >-->
                                                            <!--            <input type="hidden" name="username" value="<?=$info['username']?>">-->
                                                            <!--    </div>-->
                                                            <!--    <div class="form-group">-->
                                                            <!--          <label>Payment Method</label>-->
                                                            <!--          <select class="form-control" required="" name="method">-->
                                                            <!--               <option value="">--Select Payment Method--</option>-->
                                                            <!--               <option value="Bank Transfer">Bank Transfer</option>-->
                                                            <!--               <option value="BTC">Bitcoin</option>-->
                                                            <!--               <option value="ETH">Ethereum</option>-->
                                                            <!--               <option value="XRP">Ripple</option>-->
                                                            <!--          </select>-->
                                                            <!--    </div>-->
                                    
                                                            <!--    <div class="form-group">   -->
                                                            <!--            <button type="submit" id="btn_deposit" class="btn btn-primary form-control"><span class="fa fa-paper-plane-o"> </span> SUBMIT   </button>-->
                                                            <!--    </div>-->
                                                            <!--</form>-->
                                                            <form name="depositForm">  
                                                               
                                                               <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                                                                <div class="form-group">   
                                                                        <label> AMOUNT TO DEPOSIT</label>
                                                                        <input type="number" step="any" min="10" name="amount" class="form-control" >
                                                                        <input type="hidden" name="username" value="<?=$info['username']?>">
                                                                </div>
                                                                <div class="form-group">
                                                                      <label>Payment Method</label>
                                                                      <select class="form-control" required="" name="method" >
                                                                           <option value="Bank Transfer">Bank Transfer</option>
                                                                          
                                                                      </select>
                                                                </div>
                                    
                                                                <div class="form-group">   
                                                                        <button type="submit" id="btn_deposit" class="btn btn-primary form-control"><span class="fa fa-paper-plane-o"> </span> SUBMIT   </button>
                                                                </div>
                                                               <div class="form-group">  
                                                                  <center><label> OR</label></center>
                                                               </div>
                                                               
                                                                <div class="form-group">  
                                                                          <a href="<?=site_url()?>account/funds/btc" class="btn btn-primary form-control"><span class="fa fa-btc"> </span> DEPOSIT VIA BITCOIN   </a>
                                                               </div>
                                                             
                                                                 
                                                            </form>
                                                    </div>
                                                    <div class="col-md-4"> 
                                                             
                                                    </div>




                                                        <!--  Modal content for the above example -->
                                                        <div class="modal fade bs-example-modal-lg" id="BankDetails" tabindex="-1" role="dialog" aria-labelledby="Bank Details" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title" id="myLargeModalLabel">Bank Details</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                            
                                                                              <p class="">
                                                          Your add fund request has been successfully submitted. <br> Below are the details where you need to send the amount.Once you have sent the exact amount, you need to update the transaction number so that the admin will know that you made the payment. <br> And once admin will approved the payment request, your amount will be add in the wallet.<br>
                                                              Note : Payment will be added in your wallet by 12 Noon or 6 PM as per hongkong time<br><br>
                                                              Payment Details <br><br>
                                                                            Amount to pay : <span id="amnt">100</span> USD<br>
                                                                            Pay Mode : Bank Transfer<br>
                                                                            <br>
                                                                                                                      </p>
                                                                                                  <br>
                                                                                                  <table class="table">
                                                                                    <tr>
                                                                                      <td>Account Name :</td><td> Interday Markets Management Inc. </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                            
                                                                                      <td>Bank Name :</td><td> Axis Bank Limited</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Account Number :</td><td> 917020043527769</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>IFSC Code :</td><td> UTIB0000035</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Branch :</td><td> Siliguri Branch</td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br><br>
                                                                                  <table class="table">
                                                                                    <tr>
                                                                                      <td>Account Name : </td><td>Interday Markets management Inc </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Bank Name : </td><td>Union Bank </td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                      <td>Account Number :</td> <td>588501010050201</td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                      <td>IFSC Code :</td><td> UBIN0558851 </tr></td>
                                                                                  <tr>
                                                                                      <td>Branch : </td><td>Salbari Sukna, Darjeeling-734009</td>
                                                                                  </tr>
                                                                                  </table>
                                                                                  <br><br>
                                                                                  <table class="table">
                                                                                    <tr>
                                                                                      <td>Account Name :</td><td> Interday Markets Management Limited </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Bank Name :</td><td> Citibank (Hongkong) Limited</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Bank Code :</td><td> 250</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Branch Code :</td><td> 390</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Account Number :</td><td> 86896970</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>IFSC/Swift Code :</td><td> CITIHKAX</td>
                                                                                    </tr>
                                                                                    
                                                                                </table>
                                                                                
                                                                                <br><br>
                                                                                  <table class="table">
                                                                                    <tr>
                                                                                      <td>Account Name :</td><td> Interday Markets Management Inc </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Bank Name :</td><td> Eastwest Bank</td>
                                                                                    </tr>
                                                                                  
                                                                                    <tr>
                                                                                      <td>Branch Name :</td><td>Cebu Grand Cenia</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>Account Number :</td><td> 200021154473</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                      <td>IFSC/Swift Code :</td><td> EWBCPHMMXXX</td>
                                                                                    </tr>
                                                                                    
                                                                                </table>
                                                                               
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->



                                                          <!--  Modal content for the above example -->
                                                        <div class="modal fade bs-example-modal-lg" id="addressDetails" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title" id="myLargeModalLabel">PAYMENT DETAILS</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                                  <div class="for">
                                                                                          <label>Your add fund request has been successfully submitted.<br>
                                                                                          Send the exact amount to the address below.</label>
                                                                                  </div>
                                                                              <div class="row">
                                                                                      <div class="form-group">
                                                                                          <label>Amount :</label>
                                                                                          <input type="text" name="dp_amount" class="form-control" readonly="">
                                                                                      </div>
                                                                                      <div class="form-group">
                                                                                            <label id="type"> Adress</label>
                                                                                            <input type="text" name="address" id="address" class="form-control" readonly="">
                                                                                      </div>
                                                                                      <div class="form-group">
                                                                                              <label>Destination Tag (Only for Ripple)</label>
                                                                                              <input type="text" name="dest" class="form-control" readonly="">
                                                                                      </div>
                                                                                      <div class="form-group">
                                                                                            <span id="qrcode"></span>
                                                                                      </div>
                                                                              </div>
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                  
                                                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>


<script>
    $(document).ready(function() {
            
                $('[name=depositForm]').submit(function(e){
                            $('#btn_deposit').attr('disabled',true);
                            $('#btn_deposit').html('<span class="fa fa-spin fa-spinner"> </span> Processing ..please wait .. ');

                                  $.ajax({
                                  type: "POST",
                                  url: 'deposit',
                                  data:   {  
                                               'imm_token'     : $('#token').val(),
                                              'password'        :   $('[name=password]').val(),
                                              'amount'          :   $('[name=amount]').val(),
                                              'type'            :   $('[name=method]').val(),
                                              'username'        :   $('[name=username]').val(),
                                            
                                         },
                                  cache: false,
                                  success: function(data){

                                          var obj  = JSON.parse(data)
                                                console.log(obj[0].msg);
                                                       
                                                      $('#btn_deposit').attr('disabled',false);
                                                      $('#btn_deposit').html('<span class="fa fa-paper-plane-o"> </span>SUBMIT')
                                                      $('[name=amount]').val('');
                                                      $('[name=password]').val('');


                                                        if($('[name=method]').val() =="Bank Transfer"){
                                                                
                                                              $('#amnt').html(obj[0].amount);

                                                              swal('Good Job !','Add fund request has been submitted','success');

                                                               $('#BankDetails').modal('show');
                                                          }
                                                          else {
                                                                  
                                                                  if(obj[0].msg=='success') {

                                                                            swal('Good Job !','Add fund request has been submitted','success');
                                                                           $('#BankDetails').modal('hide');
                                                                            $('#addressDetails').modal('show');
                                                                            $('[name=dp_amount]').val(obj[0].amount)
                                                                            $('[name=dest]').val(obj[0].dest_tag)
                                                                            $('#type').html(obj[0].typ + ' Adress');
                                                                            $('#address').val(obj[0].address);
                                                                            $('#qrcode').html('<img src="'+obj[0].img_url+'" width="300">');
                                                                  }
                                                                  else {

                                                                         swal('Oops !',obj[0].msg,'error');


                                                                  }
                                                                 
                                                          }          
                                        }
                                });
                        e.preventDefault();
                  });
    });
</script>
<?php $this->view('users/body_footer')?>