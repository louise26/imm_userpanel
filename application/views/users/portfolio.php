<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#img-upload{
    width: 50%;
    position: center;       
}
</style>  

            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">MY PORTFOLIO DETAILS</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">   
                                 <div class="row">
                                        <div class="col-md-8">
                                            
                                        </div>
                                        <div class="col-md-4">

                                            <?php if($this->session->userdata('alert') !=""){ ?>
                                            <div class="alert alert-<?=$this->session->userdata('type')?>" id="alert">
                                                <?=$this->session->userdata('alert')?>
                                            </div>

                                            <?php } $this->session->set_userdata(['alert'=>''])?>
                                        </div>
                                </div>                                                          
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <table id="direct-income" class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Transaction #</th>
                                                    <th>Unique ID</th>
                                                    <th>Date</th>
                                                    <th>Amount</th>
                                                    <th>Remarks</th>
                                                    <th>Action</th>
                                                    <th>Convert</th>
                                                </tr>
                                                </thead>
                                            </table>

                                        </div>
                                    </div>
                                </div>

                            </div> <!-- End Row -->


                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                    <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
    

<script>
    $(document).ready(function() {
        setTimeout(function() {
                     $("#alert").slideUp()
             }, 2000);

        $('#direct-income').DataTable({
            "ajax": 'my-details',    
             dom: "Bfrtip",
            buttons: [{
                extend: "copy",
                className: "btn-success"
            }, {
                extend: "csv"
            }, {
                extend: "excel"
            }, {
                extend: "pdf"
            }, {
                extend: "print"
            }],
            "language": {
                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                  },
            responsive: !0
        });
    });
</script>

<script type="text/javascript">
  
           $('#direct-income tbody').on('click', 'tr', function () {
 
                var table = $('#direct-income').DataTable();
            var data = table.row($(this).closest('tr')).data()
                alert( data);
        });
    
</script>
<?php $this->view('users/body_footer')?>