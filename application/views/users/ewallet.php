<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#img-upload{
    width: 50%;
    position: center;       
}
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">E-WALLET SECTION</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                        <label>Note : Please be guided that minimum Withdrawal is $10</label>
                                                </div>
                                            </div>
                                            <div class="row">   
                                                    <div class="col-md-2">   
                                                    </div>
                                                    <div class="col-md-4">
                                                            <label> E-WALLET BALANCE</label>  
                                                            <h1 id="balance"> $  <?php

                                                                $balance = 0 ; 
                                                                 foreach ($ewallet_balance as $key => $value) {
                                                                     echo number_format($value->amount,2);
                                                                     $balance = $value->amount;
                                                                }
                                                         ?></h1> 
                                                    </div>
                                                    <div class="col-md-4">   
                                                         <div class="form-group">  
                                                         
                                                         <?php 

                                                                    $status = "" ;
                                                                    foreach ($stat as $key => $value) {
                                                                             $status = $value->status;
                                                                    } ?>
                                                                    <?php if( $status == 1) { ?>
                                                                            
                                                                            <div class="form-group">   
                                                                                <button class="btn btn-primary form-control" data-toggle="modal" data-target="#withdrawalModal"><span class="fa fa-paper-plane-o"> </span> WITHDRAW  </button>
                                                                                     </div>
                                                                                    <div class="form-group">   
                                                                                            <button class="btn btn-primary form-control"  data-toggle="modal" data-target="#transferModal"><span class="fa fa-paper-plane-o"> </span> TRANSFER TO R-WALLET   </button>
                                                                                    </div>
                                                                         
                                                                     <?php    } else {
                                                                                echo "E-WALLET TRANSACTION IS MADE AVAILABLE EVERY SUNDAY";
                                                                            }?> 
                                                                        
                                                    </div>
                                                    <div class="col-md-2">   </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                       <!-- sample modal content -->
                                                        <div id="withdrawalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title" id="myModalLabel">E-WALLET WITHDRAWAL</h4>
                                                                        <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <h4>Note: On every Transaction 5% service charge will be levy.</h4>
                                                                        <br>
                                                                                <div class="row">
                                                                                        <div class="form-group">
                                                                                            <label>Withdrawal Option :</label>
                                                                                        </div>
                                                                            </div> 
                                                                        <form name="withdrawalForm">
                                                                                <div class="row">    
                                                                                    <div class="col-md-3"><input type="radio" name="payment" class="form" required=""  value="Bankwire"> Bankwire</div>
                                                                                    <!--<div class="col-md-3"><input type="radio" name="payment" class="form" required="" value="Bitcoin"> Bitcoin</div>-->
                                                                                    <!--<div class="col-md-3"><input type="radio" name="payment" class="form" required="" value="Ethereum"> Ethereum</div>-->
                                                                                    <!--<div class="col-md-3"><input type="radio" name="payment" class="form" required="" value="Ripple"> Ripple</div>-->
                                                                                    <input type="hidden" name="type">
                                                                                     <input type="hidden" name="t_code" value="<?=$info['t_code']?>">
                                                                                     <input type="hidden" name="url" value="<?=site_url()?>">
                                                                                     <input type="hidden" name="balance" value="<?=number_format($balance,2)?>">
                                                                                </div>
                                                                            <div class="row"><hr></div>
                                                                           
                                                                            <div class="row" id="bank">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label>Account No. :</label>
                                                                                            <input type="text" name="acc_no" class="form-control" value="<?=$info['acc_number']?>">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Bank Name :</label>
                                                                                            <input type="text" name="bank_nm" class="form-control" value="<?=$info['bank_nm']?>">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label>Account Name :</label>
                                                                                            <input type="text" name="acc_name" class="form-control" value="<?=$info['acc_name']?>">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Branch Name :</label>
                                                                                            <input type="text" name="branch_nm" class="form-control" value="<?=$info['branch_nm']?>">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                            <label>Swift Code :</label>
                                                                                            <input type="text" name="swift_code" class="form-control" value="<?=$info['swift_code']?>">
                                                                                        </div>
                                                                                    </div>
                                                                            </div>
                                                                            <div class="row" id="eth">
                                                                                <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label>Ethereum Address :</label>
                                                                                            <input type="text" name="ethereum" class="form-control" value="<?=$info['ethereum']?>">
                                                                                    </div>
                                                                               </div>   
                                                                            </div>
                                                                            <div class="row" id="btc">
                                                                                <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label>Bitcoin Address :</label>
                                                                                            <input type="text" name="bitcoin" class="form-control" value="<?=$info['bitcoin']?>">
                                                                                    </div>
                                                                                </div>        
                                                                            </div>
                                                                            <div class="row" id="xrp">
                                                                                <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label>Ripple Address :</label>
                                                                                            <input type="text" name="ripple" class="form-control" value="<?=$info['ripple']?>">
                                                                                    </div>
                                                                                </div>  
                                                                            </div>
                                                                            <div class="row">
                                                                                        <div class="col-md-6">
                                                                                                 <div class="form-group">
                                                                                                    <label>Firstname</label>
                                                                                                    <input type="text" name="first_name" class="form-control" value="<?=$info['first_name']?>">
                                                                                                </div>   
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label>Lastname</label>
                                                                                                    <input type="text" name="last_name" class="form-control" value="<?=$info['last_name']?>">
                                                                                                </div> 
                                                                                        </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                         <div class="form-group">
                                                                                                <label>Description</label>
                                                                                                <textarea class="form-control" name="description"></textarea>
                                                                                         </div>
                                                                                </div>         
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                            <label>Amount to withdraw (USD)</label>
                                                                                            <input type="number" name="amount" class="form-control" required="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                            <label>Service Charge (USD)</label>
                                                                                            <input type="number" name="trans_charge" class="form-control" required="" readonly="">
                                                                                    </div>
                                                                                </div> 
                                                                            </div>
                                                                            <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Amount Net (USD)</label>
                                                                                            <input type="number" name="net_amount" class="form-control" required="" readonly="">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                             <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Transaction Password</label>
                                                                                            <input type="password" name="password" class="form-control" required="" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="btn_withdraw">Submit</button>
                                                                    </form>
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->

                                                          <!-- sample modal content -->
                                                        <div id="transferModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title" id="myModalLabel">E-WALLET TO R-WALLET FUND TRANSFER</h4>

                                                                    </div>
                                                                    <div class="modal-body">
                                                                         <h4>Note: On every Transaction 5% service charge will be levy.</h4>
                                                                        <br>
                                                                            <form name="transferForm">
                                                                                     <input type="hidden" name="trans_t_code" value="<?=$info['t_code']?>">
                                                                                     <input type="hidden" name="trans_url" value="<?=site_url()?>">
                                                                                     <input type="hidden" name="trans_balance" value="<?=number_format($balance,2)?>">
                                                                                     <input type="hidden" name="trans_first_name" class="form-control" value="<?=$info['first_name']?>">
                                                                                      <input type="hidden" name="trans_last_name" class="form-control" value="<?=$info['last_name']?>">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label>Recepient :</label>
                                                                                            <input type="text" name="trans_receiver" class="form-control" value="<?=$info['user_id']?>" readonly>
                                                                                    </div>
                                                                                </div>  
                                                                            </div>
                                                                            
                                                                             <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                            <label>Amount to transfer (USD)</label>
                                                                                            <input type="number" name="trans_amount" min="1" class="form-control" required="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                            <label>Service Charge (USD)</label>
                                                                                            <input type="number" name="service_charge" class="form-control" required="" readonly="">
                                                                                    </div>
                                                                                </div> 
                                                                            </div>
                                                                            <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Amount Net (USD)</label>
                                                                                            <input type="number" name="amount_net"  class="form-control" required="" readonly="">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                             <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Transaction Password</label>
                                                                                            <input type="password" name="trans_password" class="form-control" required="" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="btn_transfer">Submit</button>
                                                                    </form>
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                            </div> <!-- End Row -->
                           <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <table id="direct-income" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Transaction No</th>
                                                        <th>Requested Amount</th>
                                                        <th>Service Charge</th>
                                                        <th>Amount Net  </th>
                                                        <th>Request date </th>
                                                        <th>Admin Response </th>
                                                        <th>Response Date</th>
                                                        <th>Withdrawal Mode</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
        <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <!--Custom script-->
        <script src="<?=base_url()?>assets/pages/ewallet.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

       <script type="text/javascript">
           $(function(){
                   var trans_net  = 0,trans_total=0;
            $('[name=trans_amount]').focusout(function() {

                    trans_net     =  parseFloat($(this).val() * (5/100));
                    trans_total   =  parseFloat($(this).val() - (trans_net))
                      
                    $('[name=service_charge]').val(trans_net.toFixed(2));
                    $('[name=amount_net]').val(trans_total);          
              });

                     $('[name=transferForm]').submit(function(e){
                            $('#btn_transfer').attr('disabled',true);
                               $('#btn_transfer').html('<span class="fa fa-spin fa-spinner"> </span> Transferring .. ');
                                  $.ajax({
                                  type: "POST",
                                  url: 'transfer',
                                  data:   {  
                                              'imm_token'       :   $('#token').val(),
                                              't_code'          :   $('[name=trans_t_code]').val(),
                                              'user'            :   $('[name=trans_receiver]').val(),
                                              'amount'          :   $('[name=trans_amount]').val(),
                                              'password'        :   $('[name=trans_password]').val(),
                                              'balance'         :   $('[name=trans_balance]').val(),
                                              'service_charge'  :   $('[name=service_charge]').val(),
                                              'net_amount'      :   $('[name=amount_net]').val(),
                                              'first_name'      :   $('[name=trans_first_name]').val(),
                                              'last_name'       :   $('[name=trans_last_name]').val(),
                                         },
                                  cache: false,
                                  success: function(data){
                                        var obj = JSON.parse(data);
                                          //console.log(obj.title);
                                                      $('#transferForm').modal('hide');
                                                      swal(obj.title, obj.msg, obj.status);
                                                      $('#btn_transfer').attr('disabled',false);
                                                      $('#btn_transfer').html('<span>Submit</span')
                                                      $('[name=trans_amount]').val('');
                                                      $('[name=trans_password]').val('');

                                  }

                                });

                        e.preventDefault();
                  });
           });
       </script>
<?php $this->view('users/body_footer')?>