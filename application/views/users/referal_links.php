<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
      <style type="text/css">
                    .btn-file {
                      position: relative;
                      overflow: hidden;
                  }
                  .btn-file input[type=file] {
                      position: absolute;
                      top: 0;
                      right: 0;
                      min-width: 100%;
                      min-height: 100%;
                      font-size: 100px;
                      text-align: right;
                      filter: alpha(opacity=0);
                      opacity: 0;
                      outline: none;
                      background: white;
                      cursor: inherit;
                      display: block;
                  }
                  #img-upload{
                      width: 50%;
                      position: center;       
                  }
                   canvas {
                          width: 40%;
                          height: 40%;
                  }
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?php echo $info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">REFERRAL LINKS SECTION</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container"> 
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h4>Your referral link</h4>
                                                 <h5><a  href="https://immtradersclub.com/member/register/referral/<?=$info['user_id']?>">https://immtradersclub.com/member/register/referral/<?=$info['user_id']?></a> </h5>  
                                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SHARE ON TWEETER</h3>
                                        </div>
                                        <div class="panel-body">
                                                <center>
                                                    <a href="http://twitter.com/share?url=https://immtradersclub.com/member/register/referral/<?=$info['user_id']?>" target="_blank" class="btn btn-default" >
                                                    
                                                      <span class="fa fa-twitter-square fa-2x"> SHARE ON TWITTER</span>
                                                      
                                                    </a>
                                                </center>
                                           </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="panel panel-color panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SHARE ON FACEBOOK</h3>
                                        </div>
                                        <div class="panel-body">
                                          <center>
                                           <a href="http://www.facebook.com/sharer.php?u=https://immtradersclub.com/member/register/referral/<?=$info['user_id']?>" target="_blank" class="btn btn-primary">
                                              
                                              <span class="fa fa-facebook-square fa-2x"> SHARE ON FACEBOOK</span>
                                          </a>
                                          </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="panel panel-color panel-danger">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">SHARE ON GOOGLE PLUS</h3>
                                        </div>
                                        <div class="panel-body">
                                            <center>
                                              <a href="https://plusone.google.com/_/+1/confirm?hl=en&url=https://immtradersclub.com/member/register/referral/<?=$info['user_id']?>" target="_blank" class="btn btn-danger ">
                                                     <span class="fa fa-google-plus-square fa-2x"> SHARE ON GOOGLE PLUS</span>
                                                  </a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div> <!-- End Row -->
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.qrcode.min.js"></script>
        <script>
            $(document).ready(function() {
                      $('[name=transForm]').submit(function(e){
                                    $('#btn_transfer').attr('disabled',true);
                                    $('#btn_transfer').html('<span class="fa fa-spin fa-spinner"> </span> Transferring .. ');

                                          $.ajax({
                                          type: "POST",
                                          url: 'rwallet-transfer',
                                          data:   {},
                                          cache: false,
                                          success: function(data){
                                                var obj = JSON.parse(data);      
                                                  swal(obj.title, obj.msg, obj.status);
                                                  $('#btn_transfer').attr('disabled',false);
                                                  $('#btn_transfer').html('<span class="fa fa-paper-plane-o"> </span>Transfer')
                                          }
                                        });
                              //  e.preventDefault();
                          });
            });
        </script>

<script type="text/javascript">
    $('#btn_create').click(function(){
            $("#btn_create").attr("disabled", true);
            $("#btn_create").html("Getting address <span class='fa fa-spinner fa-pulse'></span>");

          //alert('Click');
            var pubk      = "" ;
            var prvkey    = "" ;
            var mn        =  "" ;
         $.getJSON("http://checker.immcoin.io/api/create-wallet", function(result){      
                        pubk   = result.publickey ;
                        prvkey = result.privateKey ; 
                        mn     = result.mnemonic;

                        $.ajax({
                                url: "create-wallet",
                                type: "POST",
                                data: {
                                      publickey : pubk,
                                      privateKey : prvkey,
                                      mnemonic :  mn 
                                },
                                timeout: 5000,
                                complete: function() {
                                    console.log('process complete');
                                    $("#btn_create").attr("disabled", false);
                                    $("#btn_create").html("GET ADDRESS");
                                },
                                success: function(data) {
                                        console.log(data);
                                        alert('You have successfully created your wallet');
                                        location.reload();
                               },
                                error: function() {
                                  console.log('process error');
                                },
                          });
            });
    })
</script>
<script type="text/javascript">
  $(function(){   
      $('#qrcode').qrcode($('[name=addr]').val());
  }) ;
</script>
<?php $this->view('users/body_footer')?>