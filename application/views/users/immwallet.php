<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
                      position: relative;
                      overflow: hidden;
                  }
                  .btn-file input[type=file] {
                      position: absolute;
                      top: 0;
                      right: 0;
                      min-width: 100%;
                      min-height: 100%;
                      font-size: 100px;
                      text-align: right;
                      filter: alpha(opacity=0);
                      opacity: 0;
                      outline: none;
                      background: white;
                      cursor: inherit;
                      display: block;
                  }
                  #img-upload{
                      width: 50%;
                      position: center;       
                  }
                   canvas {

          width: 40%;
          height: 40%;
      }
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">IMC-WALLET SECTION</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container"> 
                                  <div class="row">
                                        <div class="col-md-12">
                                            <?php 

                                                                  $rbalance = 0 ;
                                                                   $address = "";
                                                                   $is_synced = 0 ;
                                                             foreach ($imc_address as $key => $value) {
                                                                $address = $value->public_key;
                                                                $is_synced = $value->is_synced;
                                                             }
                                                          ?> 
                                        <?php if($address ==""){ ?>                  
                                            <div class="alert alert-warning alert-dismissible fade in">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h4>Note :</h4>
                                                <p>If you don't have wallet address yet, click the <button class="btn btn-primary" >CREATE WALLET</button> button below to generate your wallet address.</p>
                                                <p>Please be guided that private key is still locked, it will be activated after the ICO ends.</p>
                                                   
                                            </div>
                                        </div>
                                        <?php }?>
                                  </div>                                                          
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row">   
                                                    <div class="col-md-4">   
                                                         <label> IMM COIN BALANCE</label>  
                                                            <h1 id="balance"> 
                                                                
                                                     <?php 
                                                   

                                                      if($address !="") {
                                                          foreach ($rwallet_balance as $key => $value) {   $rbalance  = $value->amount;?>
                                                      
                                                               <p> <?= number_format($value->amount,2)?> IMC</p>
                                                               
                                                           <?php   } } else {?> 

                                                                    0.00 IMC

                                                             <?php }?>
                                                        
                                                        </h1> 
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Wallet Address</label>
                                                        <br> 
                                                       <input type="hidden" name="imm_token" value="<?=$this->security->get_csrf_hash();?>">

                                                        <?php if($address =='') { ?>


                                                            <button class="btn btn-primary" id="btn_create">CREATE WALLET</button>

                                                        <?php } else { ?>
                                                            <h6 style="color:#fff"> <a href="https://etherscan.io/token/0xe42ba5558b00d2e6109cc60412d5d4c9473fe998?a=<?=$address?>"   target="_blank"><?=$address?></a></h6>
                                                             <div class="form-group">
                                                               
                                                             </div>
                                                             <input type="hidden" name="addr" value="<?=$address?>">
                                                             <input type="hidden" name="balance" value="<?=$rbalance?>">
                                                              <input type="hidden" name="email" value="<?=$email?>">
                                                              
                                                                
                                                             
                                                                 <div id="qrcode"></div>

                                                               
                                                          

                                                        <?php }?>  
                                                        
                                                        <br>
                                                        <?php if($is_synced==0){ ?>
                                                            <button class="btn btn-primary" id="btn_sync">Sync my IMC BALANCE to Ethereum Blockchain</button>
                                                        
                                                        <?php }?>
                                                    </div>
                                                    <div class="col-md-4"> 
                                                           <label>Wallet Private Key</label>
                                                           <br>
                                                             <?php if($address =='') { ?>

                                                             <?php } else { ?>  
                                                                   <button class="btn btn-primary">LOCKED</button>
                                                         <?php }?>

                                                    </div>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                          
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.qrcode.min.js"></script>
<script>
    $(document).ready(function() {
          

          
                  
                  
                  
                   $('#btn_sync').click(function(){
                            $('#btn_sync').attr('disabled',true);
                            $('#btn_sync').html('<span class="fa fa-spin fa-spinner"> </span> Requesting .. ');

                                  $.ajax({
                                  type: "POST",
                                  url: 'https://immtradersclub.com/member/sync-request',
                                  data:   { 
                                                balance    :   $('[name=balance]').val(),
                                                address    :   $('[name=addr]').val(),
                                                imm_token  :   $('[name=imm_token]').val(),
                                                email     :   $('[name=email]').val()
                                      
                                      
                                  },
                                  cache: false,
                                  success: function(data){
                                        var obj = JSON.parse(data);
                                                 
                                                      swal(obj.title, obj.msg, obj.status);
                                                      //$('#btn_transfer').attr('disabled',false);
                                                      $('#btn_sync').remove()
                                                  

                                  }

                                });
                    
                  });


    });
</script>

<script type="text/javascript">
    $('#btn_create').click(function(){
            $("#btn_create").attr("disabled", true);
                        $("#btn_create").html("Getting address <span class='fa fa-spinner fa-pulse'></span>");

    
            var pubk = "" ;
            var prvkey = "" ;
            var mn    =  "" ;
            
            jQuery.support.cors = true;
         $.getJSON("https://immtradersclub.com/member/create-wallet", function(result){      
                                               pubk   = result.publickey ;
                                               prvkey = result.privateKey ; 
                                               mn     = result.mnemonic;

                        $.ajax({
                            url: "create-wallet",
                            type: "POST",
                      
                            data: {
                                  publickey : pubk,
                                  privateKey : prvkey,
                                  mnemonic :  mn ,
                                  imm_token  : $('[name=imm_token]').val(),
                            },
                            timeout: 5000,
                            complete: function() {
                                console.log('process complete');
                                $("#btn_create").attr("disabled", false);
                                $("#btn_create").html("GET ADDRESS");
                                
                            },
                            success: function(data) {
                                   
                                   
                
                                     swal({title: "Creat Success", text: "Your have successfully created your wallet!", type: "success"},
                                               function(){ 
                                                    location.reload();
                                                   
                                               }
                                            );
            
                                   
                           },
                            error: function() {
                              console.log('process error');
                            },
                          });
            });


    })
</script>
<script type="text/javascript">
  $(function(){
      
      $('#qrcode').qrcode($('[name=addr]').val());

  }) ;
</script>

<?php $this->view('users/body_footer')?>