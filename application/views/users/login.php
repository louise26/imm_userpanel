<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="iMM-Traders" name="description" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>iMM-Traders | Login</title>
        <link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.ico">
        <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" type="text/css">
          
    </head>
    <body>
        <!-- Begin page -->
        
       
        <div class="accountbg"></div>
        <div class="wrapper-page">
            
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-body">
                    <h3 class="text-center m-t-0 m-b-15">
                        <a href="index.html" class=""><img src="<?=base_url()?>assets/images/flogo.png"  alt="logo-img"></a>
                    </h3>
                    <h4 class="text-muted text-center m-t-0"><b>Sign In</b></h4>
                  
                      <?php
                         if( ! isset( $on_hold_message ) )
                                        {
                                            if( isset( $login_error_mesg ) )
                                            {
                                                echo '
                                                    <div style="border:1px solid red;">
                                                        <p>
                                                            Login Error #' . $this->authentication->login_errors_count . '/' . config_item('max_allowed_attempts') . ': Invalid Username, Email Address, or Password.
                                                        </p>
                                                        <p>
                                                            Username, email address and password are all case sensitive.
                                                        </p>
                                                    </div>
                                                ';
                                            }

                                            if( $this->input->get(AUTH_LOGOUT_PARAM) )
                                            {
                                                echo '
                                                    <div style="border:1px solid green">
                                                        <p>
                                                            You have successfully logged out.
                                                        </p>
                                                    </div>
                                                ';
                                            }
                         echo form_open( $login_url, ['class' => 'form-horizontal m-t-20','id'=>'submit'] ); 
                         ?>
                    <!--<form class="form-horizontal m-t-20" id="loginform">-->

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required=""  name="login_string" id="login_string" placeholder="Username / Email"  maxlength="255">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                              
                                <input type="password" name="login_pass" id="login_pass" placeholder="Password" class="form-control password" <?php 
                                    if( config_item('max_chars_for_password') > 0 )
                                        echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
                                ?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-primary">
                                    <?php
                                    if( config_item('allow_remember_me') )
                                    {
                                  ?>
                                    <input id="checkbox-signup" type="checkbox"  name="remember_me">
                                    <label for="checkbox-signup">
                                        Remember me
                                    </label>
                                    <?php
                                }
                              ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center m-t-40">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit" id="submit_button">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-7">
                                <a href="<?=site_url()?>password/recovery" class="text-muted"><i class="fa fa-lock m-r-5"></i> Reset Password?</a>
                                <br>
                                 <a href="https://immtradersclub.com/member/recover/password" class="text-muted"><i class="fa fa-lock m-r-5"></i>Alternate Reset Link</a>
                            </div>
                            <div class="col-sm-5 text-right">
                                <a href="https://immtradersclub.com/member/register" class="text-muted">Create an account</a>
                            </div>
                        </div>
                    </form>
                        <?php

                            }
                            else
                            {
                                // EXCESSIVE LOGIN ATTEMPTS ERROR MESSAGE
                                echo '
                                    <div style="border:1px solid red;">
                                        <p>
                                            Excessive Login Attempts
                                        </p>
                                        <p>
                                            You have exceeded the maximum number of failed login<br />
                                            attempts that this website will allow.
                                        <p>
                                        <p>
                                            Your access to login and account recovery has been blocked for ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes.
                                        </p>
                                        <p>
                                            Please use the <a href="/examples/recover">Account Recovery</a> after ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes has passed,<br />
                                            or contact us if you require assistance gaining access to your account.
                                        </p>
                                    </div>
                                ';
                            }
                            ?>
                </div>
            </div>
        </div>
        <!-- jQuery  -->
        <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/js/modernizr.min.js"></script>
        <script src="<?=base_url()?>assets/js/detect.js"></script>
        <script src="<?=base_url()?>assets/js/fastclick.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.blockUI.js"></script>
        <script src="<?=base_url()?>assets/js/waves.js"></script>
        <script src="<?=base_url()?>assets/js/wow.min.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?=base_url()?>assets/js/app.js"></script>
      
        <script type="text/javascript">
                    $('#submit').submit(function(){
                              //  e.preventDefault();
                                $('#btn_submit').attr('disabled',true);
                                $('#btn_submit').html("<span class='fa fa-spinner'>Redirecting .. Please wait</span>");
                       
                    });
        </script>
        <script type="text/javascript">
      // $("#submit_button").on("click",function(){
      //       $('#submit_button').attr('disabled',true);
      //       $('#login_string').attr('disabled',true);
      //       $('#login_pass').attr('disabled',true);
      //       $('#submit_button').html('<span class="fa fa-spin fa-spinner"></span>SIGNING IN . . ');
            
      // });

     

        $('form').on('submit',function(){
                  $('#submit_button').attr('disabled',true)
                $('#submit_button').html('<span class="fa fa-spin fa-spinner"> </span> Establishing connection ... Please wait')

                
        });
</script>


        

    </body>
</html>