<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
      <style type="text/css">
                    .btn-file {
                      position: relative;
                      overflow: hidden;
                  }
                  .btn-file input[type=file] {
                      position: absolute;
                      top: 0;
                      right: 0;
                      min-width: 100%;
                      min-height: 100%;
                      font-size: 100px;
                      text-align: right;
                      filter: alpha(opacity=0);
                      opacity: 0;
                      outline: none;
                      background: white;
                      cursor: inherit;
                      display: block;
                  }
                  #img-upload{
                      width: 50%;
                      position: center;       
                  }
                   canvas {
                          width: 40%;
                          height: 40%;
                  }
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?php echo $info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">Contact Support</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container"> 
                                <div class="row">
                                <div class="col-md-4">
                                  </div>
                                   <div class="col-md-4">
                                        <div class="panel panel-color panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">SEND US A MESSAGE</h3>
                                            </div>
                                            <div class="panel-body">
                                                  <form name="sendForm">
                                                    <div class="form-group">
                                                        <label>Department</label>
                                                        <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                                                        <input type="hidden" name="email" value="<?=$info['username']?>">
                                                        <select class="form-control" required="" name="department">
                                                                <option value="">--Select Department--</option>
                                                                <option value="Accounting">Accounting</option>
                                                                <option value="Customer Support">Customer Support</option>
                                                                <option value="Technical">Technical</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                       <label>Subject</label>
                                                       <input type="text" name="subject" class="form-control" required="">
                                                    </div>
                                                    <div class="form-group">
                                                          <label>Message</label>
                                                          <textarea class="form-control" rows="10" required="" name="message"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                          <button type="submit" id="btn_send" class="btn btn-primary form-control">Send</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    
                                  </div>
                                   <div class="col-md-4">
                                  </div>
                                </div>
                            </div>
                            </div> <!-- End Row -->
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.qrcode.min.js"></script>
        <script>
            $(document).ready(function() {
                      $('[name=sendForm]').submit(function(e){
                                    $('#btn_send').attr('disabled',true);
                                    $('#btn_send').html('<span class="fa fa-spin fa-spinner"> </span> Sending .. ');

                                          $.ajax({
                                          type: "POST",
                                          url: 'contact-support/send-message',
                                          data:   {
                                                  'imm_token' : $('#token').val(),
                                                  'email'   : $('[name=email]').val(),
                                                  'subject' : $('[name=subject]').val(),
                                                  'department' : $('[name=department]').val(),
                                                  'message'   : $('[name=message]').val()

                                          },
                                          cache: false,
                                          success: function(data){
                                                var obj = JSON.parse(data);      
                                                  swal(obj.title, obj.msg, obj.status);
                                                
                                                 $('[name=email]').val('');
                                                 $('[name=subject]').val('');
                                                 $('[name=department]').val('');
                                                 $('[name=message]').val('');
                                                 $('#btn_send').attr('disabled',false);
                                                 $('#btn_send').html('<span class="fa fa-paper-plane-o"> </span>Submit');
                                          }
                                        });
                               e.preventDefault();
                          });
            });
        </script>


<?php $this->view('users/body_footer')?>