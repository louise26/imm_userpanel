<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
     <title>iMM-Traders | Reset</title>
     <link rel="stylesheet" href="<?=base_url()?>assets/css/intlInputPhone.min.css">
        <link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.ico">
        <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <style>
    body { font-family:'Roboto'; background-color:#2a323c;
        }
        .popover-content{
            color: red;
        }
        #user_name {

            color: orange;
        }
        p {
            color: orange;
        }
              
    </style>
</head>
<body>
        <div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-body">
                     <h3 class="text-center m-t-0 m-b-15">
                        <a href="" class=""><img src="<?=base_url()?>assets/images/flogo.png"  alt="logo-img"></a>
                    </h3>
                    <h4 class="text-muted text-center m-t-0"><b>Reset Password</b></h4>

                  
                            <?php echo validation_errors(); ?>
                            <p class="text-warning"><?php echo $this->session->flashdata('msg') ?></p>
             
                 <form class="form-horizontal m-t-20" action="<?=site_url()?>password/recover" method="POST">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="hidden" id="token" name="imm_token" value="<?=$this->security->get_csrf_hash();?>">
                                <input class="form-control" type="text" required="" placeholder="Userid" name="userid">
                                <label id="user_name"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="email" required="" placeholder="Enter you Username/Email" name="email">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Enter  New Password" name="passwd">
                            </div>
                        </div>
                         
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Enter your Old Transaction Password" name="tcode">
                            </div>
                        </div>
                        

                        <div class="form-group text-center m-t-40">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit" id="btn_reg">Reset</button>
                            </div>
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12 text-center">
                                <a href="https://immtradersclub.com/member/login" class="btn btn-primary btn-block btn-lg waves-effect waves-light">Login</a>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
   

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="<?=base_url()?>assets/js/intlInputPhone.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script> 
   
    
</body>
</html>