<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
                      position: relative;
                      overflow: hidden;
                  }
                  .btn-file input[type=file] {
                      position: absolute;
                      top: 0;
                      right: 0;
                      min-width: 100%;
                      min-height: 100%;
                      font-size: 100px;
                      text-align: right;
                      filter: alpha(opacity=0);
                      opacity: 0;
                      outline: none;
                      background: white;
                      cursor: inherit;
                      display: block;
                  }
                  #img-upload{
                      width: 50%;
                      position: center;       
                  }
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?php echo $info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">R-WALLET SECTION</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">   
                        
                                         <div class="row">
                                        <div class="col-md-12">
                                            <?php if($this->session->flashdata('msg') !=""){ ?>
                                        

                                                    <div class="alert alert-success" id="mxg"> <?= $this->session->flashdata('msg') ?> </div>
                                                    
                                            <?php } ?>
                                            
                                        </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                       
                                            <div class="row">   
                                                    <div class="col-md-2">   
                                                    </div>
                                                    <div class="col-md-4">
                                                            <label> R-WALLET BALANCE</label>  
                                                            <h1 id="balance"> $
                                                     <?php 
                                                     $rbalance = 0 ;
                                                    foreach ($rwallet_balance as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                         $rbalance  = $value->amount;
                                                       }?>
                                                        </h1> 
                                                    </div>
                                                    <div class="col-md-4"> 
                                                    <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                                                            <?= form_open('wallet/user-tarnsferrwallet',['name'=>'transForm'])?>
                                                                <div class="form-group">   
                                                                        <label> RECEIVER</label>
                                                                        <input type="text" name="receiver" class="form-control" placeholder="Enter user id" required="">
                                                                        <span id="user_name"  class="text-info"></span> 
                                                                        <input type="hidden" name="reciever_id">
                                                                </div>
                                                                <div class="form-group">   
                                                                        <label> AMOUNT</label>
                                                                        <input type="number" step="any" min="10" name="amount" class="form-control" required="">
                                                                </div>
                                                                <div class="form-group">   
                                                                        <label> TRANSACTION PASSWORD</label>
                                                                        <input type="password" name="password" class="form-control" >
                                                                </div>
                                                                <div class="form-group">   
                                                                        <button type="submit" id="btn_transfer" class="btn btn-primary form-control"><span class="fa fa-paper-plane-o"> </span> TRANSFER   </button>
                                                                </div>
                                                            </form>  
                                                    </div>
                                                    <div class="col-md-2">   </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
<script>
    $(document).ready(function() {
            
           // $('#btn_transfer').attr('disabled',true);
                    var net = 0,total = 0;

            
            //seearch receiver
            $('[name=receiver]').focusout(function() {

                               $.ajax({
                                  type: "POST",
                                  url: 'search-user',
                                  data:   {  
                                                'imm_token' : $('#token').val(),
                                              'user'         :   $(this).val(),
                
                                         },
                                  cache: false,
                                  success: function(data){
                                        var obj = JSON.parse(data);
                                          //console.log(obj.title);
                                          console.log(obj[0].email);
                                          $('#user_name').html('<p>'+obj[0].email+'( ' + obj[0].user_id+ ' )</p>')
                                          
                                          
                                          $('[name=recieverid]').val(obj[0].user_id);

                                          $('[name=receiver_name]').val(obj[0].name);

                                          if(obj[0].name =="") {
                                                 $('#btn_transfer').attr('disabled',true);
                                          }
                                        else {

                                            $('#btn_transfer').attr('disabled',false);
                                        }                     
                                  }

                            });

              });

              $('[name=transForm]').submit(function(){
                            $('#btn_transfer').attr('disabled',true);
                            $('#btn_transfer').html('<span class="fa fa-spin fa-spinner"> </span> Transferring .. ');

                  });


    });
</script>

<?php $this->view('users/body_footer')?>