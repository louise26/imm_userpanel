<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
                      position: relative;
                      overflow: hidden;
                  }
                  .btn-file input[type=file] {
                      position: absolute;
                      top: 0;
                      right: 0;
                      min-width: 100%;
                      min-height: 100%;
                      font-size: 100px;
                      text-align: right;
                      filter: alpha(opacity=0);
                      opacity: 0;
                      outline: none;
                      background: white;
                      cursor: inherit;
                      display: block;
                  }
                  #img-upload{
                      width: 50%;
                      position: center;       
                  }
    </style>  

    
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">DEPOSIT FUND SECTION</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row">   
                                                   <div class="col-md-4">  
                                                   </div>
                                                    <div class="col-md-4">
                                                     <div id="viewCart">
                                                       <span id="viewTitle">Finish & Pay</span><br>
                                                       <div id="payAmt"><b>Amount Due: <?php echo $amount; ?> BTC</b><br>
                                                          Send payment to the below address to complete your request. <br><br>
                                                   <img src="https://chart.googleapis.com/chart?chs=125x125&cht=qr&chl=<?php echo $address; ?>" width="70%"> 
                                               <br>
                                                <div class="form-group"> 

                                                   <input type="text" id="payBox" class="form-control" name="address" value="<?php echo $address ?>" onclick="this.select();" readonly>
                                                </div>
                                                <input type="hidden" name="imm_token" value="<?=$this->security->get_csrf_hash();?>">
                                                <input type="hidden" name="user_id" value="<?=$user_id?>" >
                                                 <input type="hidden" name="email" value="<?=$email?>" >
                                                <input type="hidden" name="amount" value="<?=$amount?>" >
                                           <br><div class="form-group">  
                                                      <span  id="messages"><i  class="fa fa-spin fa-spinner"></i> Waiting for payment</span>
                                              </div>
                                              <div class="form-group">
                                                 <a href="<?=site_url()?>account/funds" class="btn btn-primary" id="done"> DONE</a> 
                                              </div>
                                           </div>
                                         </div>  
                                                   </div>
                                                    <div class="col-md-4"> 
                                                    </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
        <script>
            $(function(){
                
               $('#done').hide(); 
            });
        </script>
  <script>
 
   var specAdd = $('#payBox').val();

  var btcs = new WebSocket('wss://ws.blockchain.info/inv');

  btcs.onopen = function()
    {
    btcs.send( JSON.stringify( {"op":"addr_sub", "addr":"<?php echo $address; ?>"} ) );
    };

  btcs.onmessage = function(onmsg)
  {
    var response = JSON.parse(onmsg.data);
    var getOuts = response.x.out;
    var countOuts = getOuts.length; 
    console.log(response);
    console.log(getOuts);
    
    //   swal({
    //             position: 'top-end',
    //             type: 'success',
    //             title: 'Payment has been successfully sent. And is waiting for confimration',
    //             showConfirmButton: false,
    //             timer: 3000
    //         });
    //          $('#messages').html('<span class="fa fa-spin fa-spinner"> </span> Waiting for confirmations');
    
    
    if(countOuts > 0) {
         for(i = 0; i < countOuts; i++)
    {
      //check every output to see if it matches specified address
      var outAdd = response.x.out[i].addr;
     
         if (outAdd == specAdd )
         {
         var amount = response.x.out[i].value;
         var calAmount = amount / 100000000;
         console.log(outAdd);
         if(calAmount > 0) {
              $('#messages').html("Received " + calAmount + " BTC");
              $('#done').show(); 
              
               console.log("Received " + calAmount + " BTC");
               
               
         }
         else {
                $('#messages').html('<span class="fa fa-spin fa-spinner"> </span> Waiting for confirmations');
                console.log('Waiting for confirmations');
         }
      
        // swal({
        //         position: 'top-end',
        //         type: 'success',
        //         title: 'Payment Submiited',
        //         text: 'Your payment has been submitted.',
        //         showConfirmButton: false,
        //         timer: 3000
        //     });
            
            swal({title: "Payment Success", text: "Your payment has been submitted!", type: "success"},
               function(){ 
                    $.ajax({
                                          type: "POST",
                                          url: 'https://immtradersclub.com/member/account/sendnotif',
                                          data:   {

                                                  'imm_token'  : $('[name=imm_token]').val(),
                                                  'user_id' : $('[name=user_id]').val(),
                                                  'amount' : $('[name=amount]').val(),
                                                  'email' : $('[name=email]').val(),
                                                  'address'   : $('[name=address]').val(),

                                          },
                                          cache: false,
                                          success: function(data){
                                                //var obj = JSON.parse(data);  
                                                console.log(data);
                                                window.location.href="https://immtradersclub.com/member/account/funds";
                                                 
                                          }
                                        });
                   
               }
            );
            
           var snd = new  Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");  
             snd.play();
           
             }
             
                                
         
    } 
        
    }
    
    else {
        
        
        console.log('Waiting for payments');
    }
    
   
  }</script>


<?php $this->view('users/body_footer')?>