<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#img-upload{
    width: 50%;
    position: center;       
}
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">COIN PURCHASES</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                           <div class="col-md-6">  
                                                <div class="form-group">   
                                                <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                                                    <button class="btn btn-info form-control" data-toggle="modal" href="#buyIMC">  BUY COIN  </button>
                                                </div>  
                                           </div>
                                           <div class="col-md-6">   
                                                <div class="form-group">   
                                                     <button class="btn btn-danger form-control" data-toggle="modal" href="#sellCoin">  SELL COIN  </button>
                                                </div> 
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                            <div class="row">  
                            <div class="col-lg-12">
                                                    <ul class="nav nav-tabs navtab-bg nav-justified">
                                                        <li class="active">
                                                            <a href="#home1" data-toggle="tab" aria-expanded="true">
                                                                <span class="visible-xs"><i class="fa fa-home"></i></span>
                                                                <span class="hidden-xs">BUY COINS HISTORY</span>
                                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#profile1" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-user"></i></span>
                                                                <span class="hidden-xs">SELL COIN HISTORY</span>
                                                            </a>
                                                        </li>
                                                        
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="home1">
                                                             <table id="purchases" class="table table-striped table-bordered">
                                                                 <thead>
                                                                 <tr>
                                                                        <th>#</th>
                                                                        <th>Transaction #</th>
                                                                        <th>Date</th>
                                                                        <th>Amount</th>
                                                                        <th>Currency Rate</th>
                                                                        <th>Coins Received</th>
                                                                        <th>Coin Type</th>
                                                                        <th>Payment Status</th>
                                                                    </tr>
                                                                    </thead>
                                                              </table>
                                                        </div>
                                                        <div class="tab-pane" id="profile1">
                                                            <table id="sell" class="table table-striped table-bordered">
                                                                 <thead>
                                                                 <tr>
                                                                        <th>#</th>
                                                                        <th>Transaction #</th>
                                                                        <th>Date</th>
                                                                        <th>Amount</th>
                                                                        <th>Currency Rate</th>
                                                                        <th>Coins Received</th>
                                                                        <th>Coin Type</th>
                                                                        <th>Payment Status</th>
                                                                    </tr>
                                                                    </thead>
                                                              </table>
                                                        </div>
                                                        
                                                    </div>
                                                </div> 

                            </div>


                            <div class="row">   


                                                          <!-- sample modal content -->
                                                        <div id="buyIMC" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title" id="myModalLabel">COIN PURCHASE</h4>

                                                                    </div>
                                                                    <div class="modal-body">
                                                                         
                                                                            <form name="buyForm">
                                                                                   
                                                                           
                                                                            
                                                                             <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Amount  (USD)</label>
                                                                                            <input type="number" name="amount" min="1" class="form-control" required="">
                                                                                    </div>
                                                                                </div>
                                                
                                                                            </div>
                                                                            <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Coin Type</label>
                                                                                            <select class="form-control" name="coin_type">            
                                                                                                    <option value="IMM">    IMM COIN</option>
                                                                                            </select>
                                                                                           
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Transaction Type</label>
                                                                                            <select class="form-control">            
                                                                                                    <option value="wallet"> Wallet</option>
                                                                                            </select>
                                                                                           
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                             <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Transaction Password</label>
                                                                                            <input type="password" name="password" class="form-control" required="" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="btn_buy">Buy</button>
                                                                    </form>
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->


                                                              <!-- sample modal content -->
                                                        <div id="sellCoin" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title" id="myModalLabel">SELL COIN</h4>

                                                                    </div>
                                                                    <div class="modal-body">
                                                                         
                                                                            <form name="sellForm">
                                                                                   
                                                                           
                                                                            
                                                                             <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Number of coins</label>
                                                                                            <input type="number" name="sell_amount"  step="any" class="form-control" required="">
                                                                                    </div>
                                                                                </div>
                                                
                                                                            </div>
                                                                            <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Coin Type</label>
                                                                                            <select class="form-control" name="sell_coin">            
                                                                                                    <option value="XRP">    Ripple</option>
                                                                                                    <option value="ETH">    Ethereum</option>
                                                                                                    <option value="LTC">    Litecoin</option>
                                                                                            </select>
                                                                                           
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                             <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Transaction Password</label>
                                                                                            <input type="password" name="trans_password" class="form-control" required="" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="btn_sell">Submit</button>
                                                                    </form>
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->

                            </div>
                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->

                    <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>

          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
    

<script>
    $(document).ready(function() {
        $('#purchases').DataTable({
            "ajax": 'purchases/historty',    
             dom: "Bfrtip",
            buttons: [{
                extend: "copy",
                className: "btn-success"
            }, {
                extend: "csv"
            }, {
                extend: "excel"
            }, {
                extend: "pdf"
            }, {
                extend: "print"
            }],
            "language": {
                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                  },
            responsive: !0
        });

         $('#sell').DataTable({
            "ajax": 'purchases/sellhistorty',    
             dom: "Bfrtip",
            buttons: [{
                extend: "copy",
                className: "btn-success"
            }, {
                extend: "csv"
            }, {
                extend: "excel"
            }, {
                extend: "pdf"
            }, {
                extend: "print"
            }],
            "language": {
                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                  },
            responsive: !0
        });
    });
</script>

<script type="text/javascript">
      $('[name=buyForm]').submit(function(e){
                            $('#btn_buy').attr('disabled',true);
                            $('#btn_buy').html('<span class="fa fa-spin fa-spinner"> </span> Processing .. ');

                                  $.ajax({
                                  type: "POST",
                                  url: 'buy',
                                  data:   {    
                                                
                                              'imm_token'       :   $('#token').val(),
                                              'password'        :   $('[name=password]').val(),
                                              'coin_type'       :   $('[name=coin_type]').val(),
                                              'amount'          :   $('[name=amount]').val(),    
                                         },
                                  cache: false,
                                  success: function(data){


                                        console.log(data);
                                        var obj = JSON.parse(data);
                                              
                                                      swal(obj[0].title, obj[0].msg, obj[0].status);
                                                      $('#btn_buy').attr('disabled',false);
                                                      $('#btn_buy').html('<span class=""> </span>Buy');
                                                      $('[name=amount]').val('');
                                                      $('[name=password]').val('');
                                  }

                                });
                        e.preventDefault();
                  });



       $('[name=sellForm]').submit(function(e){
                            $('#btn_sell').attr('disabled',true);
                            $('#btn_sell').html('<span class="fa fa-spin fa-spinner"> </span> Processing .. ');

                                  $.ajax({
                                  type: "POST",
                                  url: 'sell',
                                  data:   {    
                                            'imm_token'         :   $('#token').val(),
                                              'password'        :   $('[name=trans_password]').val(),
                                              'coin_type'       :   $('[name=sell_coin]').val(),
                                              'amount'          :   $('[name=sell_amount]').val(),    
                                         },
                                  cache: false,
                                  success: function(data){


                                        console.log(data);
                                        var obj = JSON.parse(data);
                                            
                                                      swal(obj[0].title, obj[0].msg, obj[0].status);
                                                      $('#btn_sell').attr('disabled',false);
                                                      $('#btn_sell').html('<span class=""> </span>Sell');
                                                      $('[name=sell_amount]').val('');
                                                      $('[name=trans_password]').val('');
                                  }

                                });
                        e.preventDefault();
                  });

</script>

<?php $this->view('users/body_footer')?>