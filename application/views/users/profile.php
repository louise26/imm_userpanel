<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 50%;
    position: center;       
}
</style>
              
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?php echo $info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                          <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">PROFILE SETTINGS</h4>

                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <ul class="nav nav-tabs navtab-bg nav-justified">
                                                        <li class="active">
                                                            <a href="#home" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-home"></i></span>
                                                                <span class="hidden-xs">Profile</span>
                                                            </a>
                                                        </li>
                                                         <li class="">
                                                            <a href="#settings" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                                                <span class="hidden-xs">Profile Photo</span>
                                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#profile" data-toggle="tab" aria-expanded="true">
                                                                <span class="visible-xs"><i class="fa fa-user"></i></span>
                                                                <span class="hidden-xs">Bank Details</span>
                                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#messages" data-toggle="tab" aria-expanded="false">
                                                                <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                                                <span class="hidden-xs">Wallet Details</span>
                                                            </a>
                                                        </li>  
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="home" >
                                                            <div class="panel  panel-primary">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">Profile Details</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <form id="formProfile">
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <input type="hidden" name="" id="base_url" value="<?=site_url()?>">
                                                                                        <input type="hidden" name="" id="userd" value="<?=$info['id']?>">
                                                                                        <label>Firstname</label>
                                                                                        <div>
                                                                                            <input type="text" class="form-control"  placeholder="Firstname" id="fname" <?=$info['fname']=='' ? '' : 'readonly'?>>
                                                                                        </div>
                                                                                </div>       
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Lastname</label>
                                                                                        <div>
                                                                                            <input type="text" class="form-control" required=""  placeholder="Lastname" id="lname" <?=$info['lname']=='' ? '' : 'readonly'?>>
                                                                                        </div>
                                                                                </div>    
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Email</label>
                                                                                        <div>
                                                                                            <input type="email" class="form-control" required="" <?=$info['username']=='' ? '' : 'readonly'?>  placeholder="Lastname" id="email">
                                                                                        </div>
                                                                                </div>     
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Old Password</label>
                                                                                        <div>
                                                                                            <input type="password" class="form-control" required="" id="oldpasswd"  placeholder="Old Password">
                                                                                        </div>
                                                                                </div>      
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Password</label>
                                                                                        <div>
                                                                                            <input type="password" class="form-control" required="" id="passwd"  placeholder="Password">
                                                                                        </div>
                                                                                </div>      
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Confirm Password</label>
                                                                                        <div>
                                                                                            <input type="password" class="form-control" required="" id="confirmpasswd"   data-parsley-equalto="#passwd" placeholder="Confirm Password">
                                                                                        </div>
                                                                                </div>          
                                                                            </div>
                                                                            </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Old Transaction Password</label>
                                                                                        <div>
                                                                                            <input type="password" class="form-control" id="oldtranspasswd" required=""  >
                                                                                        </div>
                                                                                </div>        
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Transaction Password</label>
                                                                                        <div>
                                                                                            <input type="password" class="form-control" id="transpasswd" required=""  >
                                                                                        </div>
                                                                                </div>
                                                                           </div>
                                                                            <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Confirm Transaction Password</label>
                                                                                        <div>
                                                                                            <input type="password" class="form-control" id="ctranspasswd" required="" >
                                                                                        </div>
                                                                                </div>          
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-sm-12">
                                                                                        <div class="form-group">
                                                                                                <label>Address</label>
                                                                                                <textarea class="form-control" id="address"></textarea>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-sm-4">
                                                                                        <div class="form-group">
                                                                                        <label>Country</label>
                                                                                        <div>
                                                                                            <select class="form-control" name="country" id="country"  required="" <?=$info['country']=='' ? '' : 'disabled'?>>
                                                                                                  <option>Select Country</option>
                                                                                                  <option value="Philippines">Philippines</option>
                                                                                                  <option value="Singapore">Singapore</option>
                                                                                                  <option value="Hong Kong">Hong Kong</option>
                                                                                                  <option value="China">China</option>
                                                                                                  <option value="Malaysia">Malaysia</option>
                                                                                                  <option value="Myanmar">Myanmar</option>
                                                                                                  <option value="Indonesia">Indonesia</option>
                                                                                                  <option value="Thailand">Thailand</option>
                                                                                                  <option value="Vietnam">Vietnam</option>
                                                                                                  <option disabled="disabled">---------------------------</option>
                                                                                                  <option value="United States">United States</option> 
                                                                                                  <option value="United Kingdom">United Kingdom</option> 
                                                                                                  <option value="Afghanistan">Afghanistan</option>
                                                                                                  <option value="Albania">Albania</option> 
                                                                                                  <option value="Algeria">Algeria</option> 
                                                                                                  <option value="American Samoa">American Samoa</option> 
                                                                                                  <option value="Andorra">Andorra</option> 
                                                                                                  <option value="Angola">Angola</option> 
                                                                                                  <option value="Anguilla">Anguilla</option> 
                                                                                                  <option value="Antarctica">Antarctica</option> 
                                                                                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                                                                                  <option value="Argentina">Argentina</option> 
                                                                                                  <option value="Armenia">Armenia</option> 
                                                                                                  <option value="Aruba">Aruba</option> 
                                                                                                  <option value="Australia">Australia</option> 
                                                                                                  <option value="Austria">Austria</option> 
                                                                                                  <option value="Azerbaijan">Azerbaijan</option>
                                                                                                  <option value="Bahamas">Bahamas</option> 
                                                                                                  <option value="Bahrain">Bahrain</option> 
                                                                                                  <option value="Bangladesh">Bangladesh</option> 
                                                                                                  <option value="Barbados">Barbados</option> 
                                                                                                  <option value="Belarus">Belarus</option> 
                                                                                                  <option value="Belgium">Belgium</option> 
                                                                                                  <option value="Belize">Belize</option> 
                                                                                                  <option value="Benin">Benin</option> 
                                                                                                  <option value="Bermuda">Bermuda</option> 
                                                                                                  <option value="Bhutan">Bhutan</option> 
                                                                                                  <option value="Bolivia">Bolivia</option> 
                                                                                                  <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
                                                                                                  <option value="Botswana">Botswana</option> 
                                                                                                  <option value="Bouvet Island">Bouvet Island</option> 
                                                                                                  <option value="Brazil">Brazil</option> 
                                                                                                  <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
                                                                                                  <option value="Brunei Darussalam">Brunei Darussalam</option> 
                                                                                                  <option value="Bulgaria">Bulgaria</option> 
                                                                                                  <option value="Burkina Faso">Burkina Faso</option> 
                                                                                                  <option value="Burundi">Burundi</option> 
                                                                                                  <option value="Cambodia">Cambodia</option> 
                                                                                                  <option value="Cameroon">Cameroon</option> 
                                                                                                  <option value="Canada">Canada</option> 
                                                                                                  <option value="Cape Verde">Cape Verde</option> 
                                                                                                  <option value="Cayman Islands">Cayman Islands</option> 
                                                                                                  <option value="Central African Republic">Central African Republic</option> 
                                                                                                  <option value="Chad">Chad</option> 
                                                                                                  <option value="Chile">Chile</option>
                                                                                                  <option value="Christmas Island">Christmas Island</option>
                                                                                                  <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
                                                                                                  <option value="Colombia">Colombia</option> 
                                                                                                  <option value="Comoros">Comoros</option> 
                                                                                                  <option value="Congo">Congo</option> 
                                                                                                  <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
                                                                                                  <option value="Cook Islands">Cook Islands</option> 
                                                                                                  <option value="Costa Rica">Costa Rica</option> 
                                                                                                  <option value="Cote D'ivoire">Cote D'ivoire</option> 
                                                                                                  <option value="Croatia">Croatia</option> 
                                                                                                  <option value="Cuba">Cuba</option> 
                                                                                                  <option value="Cyprus">Cyprus</option> 
                                                                                                  <option value="Czech Republic">Czech Republic</option> 
                                                                                                  <option value="Denmark">Denmark</option> 
                                                                                                  <option value="Djibouti">Djibouti</option> 
                                                                                                  <option value="Dominica">Dominica</option> 
                                                                                                  <option value="Dominican Republic">Dominican Republic</option> 
                                                                                                  <option value="Ecuador">Ecuador</option> 
                                                                                                  <option value="Egypt">Egypt</option> 
                                                                                                  <option value="El Salvador">El Salvador</option> 
                                                                                                  <option value="Equatorial Guinea">Equatorial Guinea</option> 
                                                                                                  <option value="Eritrea">Eritrea</option> 
                                                                                                  <option value="Estonia">Estonia</option> 
                                                                                                  <option value="Ethiopia">Ethiopia</option> 
                                                                                                  <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
                                                                                                  <option value="Faroe Islands">Faroe Islands</option> 
                                                                                                  <option value="Fiji">Fiji</option> 
                                                                                                  <option value="Finland">Finland</option> 
                                                                                                  <option value="France">France</option> 
                                                                                                  <option value="French Guiana">French Guiana</option> 
                                                                                                  <option value="French Polynesia">French Polynesia</option> 
                                                                                                  <option value="French Southern Territories">French Southern Territories</option> 
                                                                                                  <option value="Gabon">Gabon</option> 
                                                                                                  <option value="Gambia">Gambia</option> 
                                                                                                  <option value="Georgia">Georgia</option> 
                                                                                                  <option value="Germany">Germany</option> 
                                                                                                  <option value="Ghana">Ghana</option> 
                                                                                                  <option value="Gibraltar">Gibraltar</option> 
                                                                                                  <option value="Greece">Greece</option> 
                                                                                                  <option value="Greenland">Greenland</option> 
                                                                                                  <option value="Grenada">Grenada</option> 
                                                                                                  <option value="Guadeloupe">Guadeloupe</option> 
                                                                                                  <option value="Guam">Guam</option> 
                                                                                                  <option value="Guatemala">Guatemala</option> 
                                                                                                  <option value="Guinea">Guinea</option> 
                                                                                                  <option value="Guinea-bissau">Guinea-bissau</option> 
                                                                                                  <option value="Guyana">Guyana</option> 
                                                                                                  <option value="Haiti">Haiti</option> 
                                                                                                  <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
                                                                                                  <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
                                                                                                  <option value="Honduras">Honduras</option>
                                                                                                  <option value="Hungary">Hungary</option>
                                                                                                  <option value="Iceland">Iceland</option> 
                                                                                                  <option value="India">India</option> 
                                                                                                  <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                                                                  <option value="Iraq">Iraq</option> 
                                                                                                  <option value="Ireland">Ireland</option> 
                                                                                                  <option value="Israel">Israel</option> 
                                                                                                  <option value="Italy">Italy</option> 
                                                                                                  <option value="Jamaica">Jamaica</option> 
                                                                                                  <option value="Japan">Japan</option> 
                                                                                                  <option value="Jordan">Jordan</option> 
                                                                                                  <option value="Kazakhstan">Kazakhstan</option> 
                                                                                                  <option value="Kenya">Kenya</option> 
                                                                                                  <option value="Kiribati">Kiribati</option> 
                                                                                                  <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
                                                                                                  <option value="Korea, Republic of">Korea, Republic of</option> 
                                                                                                  <option value="Kuwait">Kuwait</option> 
                                                                                                  <option value="Kyrgyzstan">Kyrgyzstan</option> 
                                                                                                  <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
                                                                                                  <option value="Latvia">Latvia</option> 
                                                                                                  <option value="Lebanon">Lebanon</option> 
                                                                                                  <option value="Lesotho">Lesotho</option> 
                                                                                                  <option value="Liberia">Liberia</option> 
                                                                                                  <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
                                                                                                  <option value="Liechtenstein">Liechtenstein</option> 
                                                                                                  <option value="Lithuania">Lithuania</option> 
                                                                                                  <option value="Luxembourg">Luxembourg</option> 
                                                                                                  <option value="Macao">Macao</option> 
                                                                                                  <option value="Macedonia">Macedonia</option> 
                                                                                                  <option value="Madagascar">Madagascar</option> 
                                                                                                  <option value="Malawi">Malawi</option>
                                                                                                  <option value="Maldives">Maldives</option>
                                                                                                  <option value="Mali">Mali</option> 
                                                                                                  <option value="Malta">Malta</option> 
                                                                                                  <option value="Marshall Islands">Marshall Islands</option> 
                                                                                                  <option value="Martinique">Martinique</option> 
                                                                                                  <option value="Mauritania">Mauritania</option> 
                                                                                                  <option value="Mauritius">Mauritius</option> 
                                                                                                  <option value="Mayotte">Mayotte</option> 
                                                                                                  <option value="Mexico">Mexico</option> 
                                                                                                  <option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
                                                                                                  <option value="Moldova, Republic of">Moldova, Republic of</option> 
                                                                                                  <option value="Monaco">Monaco</option> 
                                                                                                  <option value="Mongolia">Mongolia</option> 
                                                                                                  <option value="Montserrat">Montserrat</option> 
                                                                                                  <option value="Morocco">Morocco</option> 
                                                                                                  <option value="Mozambique">Mozambique</option>
                                                                                                  <option value="Namibia">Namibia</option>
                                                                                                  <option value="Nauru">Nauru</option> 
                                                                                                  <option value="Nepal">Nepal</option> 
                                                                                                  <option value="Netherlands">Netherlands</option> 
                                                                                                  <option value="Netherlands Antilles">Netherlands Antilles</option> 
                                                                                                  <option value="New Caledonia">New Caledonia</option> 
                                                                                                  <option value="New Zealand">New Zealand</option> 
                                                                                                  <option value="Nicaragua">Nicaragua</option> 
                                                                                                  <option value="Niger">Niger</option>
                                                                                                  <option value="Nigeria">Nigeria</option>
                                                                                                  <option value="Niue">Niue</option> 
                                                                                                  <option value="Norfolk Island">Norfolk Island</option> 
                                                                                                  <option value="Northern Mariana Islands">Northern Mariana Islands</option> 
                                                                                                  <option value="Norway">Norway</option> 
                                                                                                  <option value="Oman">Oman</option> 
                                                                                                  <option value="Pakistan">Pakistan</option> 
                                                                                                  <option value="Palau">Palau</option> 
                                                                                                  <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
                                                                                                  <option value="Panama">Panama</option> 
                                                                                                  <option value="Papua New Guinea">Papua New Guinea</option> 
                                                                                                  <option value="Paraguay">Paraguay</option> 
                                                                                                  <option value="Peru">Peru</option> 
                                                                                                  <option value="Philippines">Philippines</option> 
                                                                                                  <option value="Pitcairn">Pitcairn</option> 
                                                                                                  <option value="Poland">Poland</option> 
                                                                                                  <option value="Portugal">Portugal</option> 
                                                                                                  <option value="Puerto Rico">Puerto Rico</option> 
                                                                                                  <option value="Qatar">Qatar</option> 
                                                                                                  <option value="Reunion">Reunion</option> 
                                                                                                  <option value="Romania">Romania</option> 
                                                                                                  <option value="Russian Federation">Russian Federation</option> 
                                                                                                  <option value="Rwanda">Rwanda</option> 
                                                                                                  <option value="Saint Helena">Saint Helena</option> 
                                                                                                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
                                                                                                  <option value="Saint Lucia">Saint Lucia</option> 
                                                                                                  <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
                                                                                                  <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
                                                                                                  <option value="Samoa">Samoa</option> 
                                                                                                  <option value="San Marino">San Marino</option> 
                                                                                                  <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
                                                                                                  <option value="Saudi Arabia">Saudi Arabia</option> 
                                                                                                  <option value="Senegal">Senegal</option> 
                                                                                                  <option value="Serbia and Montenegro">Serbia and Montenegro</option> 
                                                                                                  <option value="Seychelles">Seychelles</option> 
                                                                                                  <option value="Sierra Leone">Sierra Leone</option>
                                                                                                  <option value="Slovakia">Slovakia</option>
                                                                                                  <option value="Slovenia">Slovenia</option> 
                                                                                                  <option value="Solomon Islands">Solomon Islands</option> 
                                                                                                  <option value="Somalia">Somalia</option> 
                                                                                                  <option value="South Africa">South Africa</option> 
                                                                                                  <option value="South Georgia">South Georgia</option> 
                                                                                                  <option value="Spain">Spain</option> 
                                                                                                  <option value="Sri Lanka">Sri Lanka</option> 
                                                                                                  <option value="Sudan">Sudan</option> 
                                                                                                  <option value="Suriname">Suriname</option> 
                                                                                                  <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
                                                                                                  <option value="Swaziland">Swaziland</option> 
                                                                                                  <option value="Sweden">Sweden</option> 
                                                                                                  <option value="Switzerland">Switzerland</option> 
                                                                                                  <option value="Syrian Arab Republic">Syrian Arab Republic</option> 
                                                                                                  <option value="Taiwan, Province of China">Taiwan, Province of China</option> 
                                                                                                  <option value="Tajikistan">Tajikistan</option> 
                                                                                                  <option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
                                                                                                  <option value="Timor-leste">Timor-leste</option>
                                                                                                  <option value="Togo">Togo</option>
                                                                                                  <option value="Tokelau">Tokelau</option>
                                                                                                  <option value="Tonga">Tonga</option> 
                                                                                                  <option value="Trinidad and Tobago">Trinidad and Tobago</option> 
                                                                                                  <option value="Tunisia">Tunisia</option> 
                                                                                                  <option value="Turkey">Turkey</option> 
                                                                                                  <option value="Turkmenistan">Turkmenistan</option> 
                                                                                                  <option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
                                                                                                  <option value="Tuvalu">Tuvalu</option> 
                                                                                                  <option value="Uganda">Uganda</option> 
                                                                                                  <option value="Ukraine">Ukraine</option> 
                                                                                                  <option value="United Arab Emirates">United Arab Emirates</option> 
                                                                                                  <option value="United Kingdom">United Kingdom</option> 
                                                                                                  <option value="United States">United States</option> 
                                                                                                  <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
                                                                                                  <option value="Uruguay">Uruguay</option> 
                                                                                                  <option value="Uzbekistan">Uzbekistan</option> 
                                                                                                  <option value="Vanuatu">Vanuatu</option> 
                                                                                                  <option value="Venezuela">Venezuela</option>
                                                                                                  <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                                                                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
                                                                                                  <option value="Wallis and Futuna">Wallis and Futuna</option> 
                                                                                                  <option value="Western Sahara">Western Sahara</option> 
                                                                                                  <option value="Yemen">Yemen</option> 
                                                                                                  <option value="Zambia">Zambia</option> 
                                                                                                  <option value="Zimbabwe">Zimbabwe</option>
                                                                                                  </select>
                                                                                        </div>
                                                                                     </div>   
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                        <div class="form-group">
                                                                                                <label>State</label>
                                                                                                <input type="text" class="form-control" name="" id="state">
                                                                                        </div>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                        <div class="form-group">
                                                                                                <label>City</label>
                                                                                                <input type="text" class="form-control" name="" id="city">
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-sm-4">
                                                                                        <div class="form-group">
                                                                                            <label>Zipcode</label>
                                                                                            <input type="text" name="" class="form-control" id="zipcode">
                                                                                        </div>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Country Code</label>
                                                                                            <select class="form-control" name="ccode" id="countrycode">
                                                                                            <option value=""> Select Country Code</option>
                                                                                             <option value="93">AFGHANISTAN(AF)  +93 </option>
                                                                                             <option value="355">ALBANIA(AL)  +355 </option>
                                                                                             <option value="213">ALGERIA(DZ)  +213 </option>
                                                                                             <option value="1684">AMERICAN SAMOA(AS)  +1684 </option>
                                                                                             <option value="376">ANDORRA(AD)  +376 </option>
                                                                                             <option value="244">ANGOLA(AO)  +244 </option>
                                                                                             <option value="1264">ANGUILLA(AI)  +1264 </option>
                                                                                             <option value="672">ANTARCTICA(AQ)  +672 </option>
                                                                                             <option value="1268">ANTIGUA AND BARBUDA(AG)  +1268 </option>
                                                                                             <option value="54">ARGENTINA(AR)  +54 </option>
                                                                                             <option value="374">ARMENIA(AM)  +374 </option>
                                                                                             <option value="297">ARUBA(AW)  +297 </option>
                                                                                             <option value="61">AUSTRALIA(AU)  +61 </option>
                                                                                             <option value="43">AUSTRIA(AT)  +43 </option>
                                                                                             <option value="994">AZERBAIJAN(AZ)  +994 </option>
                                                                                             <option value="1242">BAHAMAS(BS)  +1242 </option>
                                                                                             <option value="973">BAHRAIN(BH)  +973 </option>
                                                                                             <option value="880">BANGLADESH(BD)  +880 </option>
                                                                                             <option value="1246">BARBADOS(BB)  +1246 </option>
                                                                                             <option value="375">BELARUS(BY)  +375 </option>
                                                                                             <option value="32">BELGIUM(BE)  +32 </option>
                                                                                             <option value="501">BELIZE(BZ)  +501 </option>
                                                                                             <option value="229">BENIN(BJ)  +229 </option>
                                                                                             <option value="1441">BERMUDA(BM)  +1441 </option>
                                                                                             <option value="975">BHUTAN(BT)  +975 </option>
                                                                                             <option value="591">BOLIVIA(BO)  +591 </option>
                                                                                             <option value="387">BOSNIA AND HERZEGOVINA(BA)  +387 </option>
                                                                                             <option value="267">BOTSWANA(BW)  +267 </option>
                                                                                             <option value="0">BOUVET ISLAND(BV)  +0 </option>
                                                                                             <option value="55">BRAZIL(BR)  +55 </option>
                                                                                             <option value="246">BRITISH INDIAN OCEAN TERRITORY(IO)  +246 </option>
                                                                                             <option value="673">BRUNEI DARUSSALAM(BN)  +673 </option>
                                                                                             <option value="359">BULGARIA(BG)  +359 </option>
                                                                                             <option value="226">BURKINA FASO(BF)  +226 </option>
                                                                                             <option value="257">BURUNDI(BI)  +257 </option>
                                                                                             <option value="855">CAMBODIA(KH)  +855 </option>
                                                                                             <option value="237">CAMEROON(CM)  +237 </option>
                                                                                             <option value="1">CANADA(CA)  +1 </option>
                                                                                             <option value="238">CAPE VERDE(CV)  +238 </option>
                                                                                             <option value="1345">CAYMAN ISLANDS(KY)  +1345 </option>
                                                                                             <option value="236">CENTRAL AFRICAN REPUBLIC(CF)  +236 </option>
                                                                                             <option value="235">CHAD(TD)  +235 </option>
                                                                                             <option value="56">CHILE(CL)  +56 </option>
                                                                                             <option value="86">CHINA(CN)  +86 </option>
                                                                                             <option value="61">CHRISTMAS ISLAND(CX)  +61 </option>
                                                                                             <option value="672">COCOS (KEELING) ISLANDS(CC)  +672 </option>
                                                                                             <option value="57">COLOMBIA(CO)  +57 </option>
                                                                                             <option value="269">COMOROS(KM)  +269 </option>
                                                                                             <option value="242">CONGO(CG)  +242 </option>
                                                                                             <option value="242">CONGO, THE DEMOCRATIC REPUBLIC OF THE(CD)  +242 </option>
                                                                                             <option value="682">COOK ISLANDS(CK)  +682 </option>
                                                                                             <option value="506">COSTA RICA(CR)  +506 </option>
                                                                                             <option value="225">COTE D'IVOIRE(CI)  +225 </option>
                                                                                             <option value="385">CROATIA(HR)  +385 </option>
                                                                                             <option value="53">CUBA(CU)  +53 </option>
                                                                                             <option value="357">CYPRUS(CY)  +357 </option>
                                                                                             <option value="420">CZECH REPUBLIC(CZ)  +420 </option>
                                                                                             <option value="45">DENMARK(DK)  +45 </option>
                                                                                             <option value="253">DJIBOUTI(DJ)  +253 </option>
                                                                                             <option value="1767">DOMINICA(DM)  +1767 </option>
                                                                                             <option value="1809">DOMINICAN REPUBLIC(DO)  +1809 </option>
                                                                                             <option value="593">ECUADOR(EC)  +593 </option>
                                                                                             <option value="20">EGYPT(EG)  +20 </option>
                                                                                             <option value="503">EL SALVADOR(SV)  +503 </option>
                                                                                             <option value="240">EQUATORIAL GUINEA(GQ)  +240 </option>
                                                                                             <option value="291">ERITREA(ER)  +291 </option>
                                                                                             <option value="372">ESTONIA(EE)  +372 </option>
                                                                                             <option value="251">ETHIOPIA(ET)  +251 </option>
                                                                                             <option value="500">FALKLAND ISLANDS (MALVINAS)(FK)  +500 </option>
                                                                                             <option value="298">FAROE ISLANDS(FO)  +298 </option>
                                                                                             <option value="679">FIJI(FJ)  +679 </option>
                                                                                             <option value="358">FINLAND(FI)  +358 </option>
                                                                                             <option value="33">FRANCE(FR)  +33 </option>
                                                                                             <option value="594">FRENCH GUIANA(GF)  +594 </option>
                                                                                             <option value="689">FRENCH POLYNESIA(PF)  +689 </option>
                                                                                             <option value="0">FRENCH SOUTHERN TERRITORIES(TF)  +0 </option>
                                                                                             <option value="241">GABON(GA)  +241 </option>
                                                                                             <option value="220">GAMBIA(GM)  +220 </option>
                                                                                             <option value="995">GEORGIA(GE)  +995 </option>
                                                                                             <option value="49">GERMANY(DE)  +49 </option>
                                                                                             <option value="233">GHANA(GH)  +233 </option>
                                                                                             <option value="350">GIBRALTAR(GI)  +350 </option>
                                                                                             <option value="30">GREECE(GR)  +30 </option>
                                                                                             <option value="299">GREENLAND(GL)  +299 </option>
                                                                                             <option value="1473">GRENADA(GD)  +1473 </option>
                                                                                             <option value="590">GUADELOUPE(GP)  +590 </option>
                                                                                             <option value="1671">GUAM(GU)  +1671 </option>
                                                                                             <option value="502">GUATEMALA(GT)  +502 </option>
                                                                                             <option value="224">GUINEA(GN)  +224 </option>
                                                                                             <option value="245">GUINEA-BISSAU(GW)  +245 </option>
                                                                                             <option value="592">GUYANA(GY)  +592 </option>
                                                                                             <option value="509">HAITI(HT)  +509 </option>
                                                                                             <option value="0">HEARD ISLAND AND MCDONALD ISLANDS(HM)  +0 </option>
                                                                                             <option value="39">HOLY SEE (VATICAN CITY STATE)(VA)  +39 </option>
                                                                                             <option value="504">HONDURAS(HN)  +504 </option>
                                                                                             <option value="852">HONG KONG(HK)  +852 </option>
                                                                                             <option value="36">HUNGARY(HU)  +36 </option>
                                                                                             <option value="354">ICELAND(IS)  +354 </option>
                                                                                             <option value="91">INDIA(IN)  +91 </option>
                                                                                             <option value="62">INDONESIA(ID)  +62 </option>
                                                                                             <option value="98">IRAN, ISLAMIC REPUBLIC OF(IR)  +98 </option>
                                                                                             <option value="964">IRAQ(IQ)  +964 </option>
                                                                                             <option value="353">IRELAND(IE)  +353 </option>
                                                                                             <option value="972">ISRAEL(IL)  +972 </option>
                                                                                             <option value="39">ITALY(IT)  +39 </option>
                                                                                             <option value="1876">JAMAICA(JM)  +1876 </option>
                                                                                             <option value="81">JAPAN(JP)  +81 </option>
                                                                                             <option value="962">JORDAN(JO)  +962 </option>
                                                                                             <option value="7">KAZAKHSTAN(KZ)  +7 </option>
                                                                                             <option value="254">KENYA(KE)  +254 </option>
                                                                                             <option value="686">KIRIBATI(KI)  +686 </option>
                                                                                             <option value="850">KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF(KP)  +850 </option>
                                                                                             <option value="82">KOREA, REPUBLIC OF(KR)  +82 </option>
                                                                                             <option value="965">KUWAIT(KW)  +965 </option>
                                                                                             <option value="996">KYRGYZSTAN(KG)  +996 </option>
                                                                                             <option value="856">LAO PEOPLE'S DEMOCRATIC REPUBLIC(LA)  +856 </option>
                                                                                             <option value="371">LATVIA(LV)  +371 </option>
                                                                                             <option value="961">LEBANON(LB)  +961 </option>
                                                                                             <option value="266">LESOTHO(LS)  +266 </option>
                                                                                             <option value="231">LIBERIA(LR)  +231 </option>
                                                                                             <option value="218">LIBYAN ARAB JAMAHIRIYA(LY)  +218 </option>
                                                                                             <option value="423">LIECHTENSTEIN(LI)  +423 </option>
                                                                                             <option value="370">LITHUANIA(LT)  +370 </option>
                                                                                             <option value="352">LUXEMBOURG(LU)  +352 </option>
                                                                                             <option value="853">MACAO(MO)  +853 </option>
                                                                                             <option value="389">MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF(MK)  +389 </option>
                                                                                             <option value="261">MADAGASCAR(MG)  +261 </option>
                                                                                             <option value="265">MALAWI(MW)  +265 </option>
                                                                                             <option value="60">MALAYSIA(MY)  +60 </option>
                                                                                             <option value="960">MALDIVES(MV)  +960 </option>
                                                                                             <option value="223">MALI(ML)  +223 </option>
                                                                                             <option value="356">MALTA(MT)  +356 </option>
                                                                                             <option value="692">MARSHALL ISLANDS(MH)  +692 </option>
                                                                                             <option value="596">MARTINIQUE(MQ)  +596 </option>
                                                                                             <option value="222">MAURITANIA(MR)  +222 </option>
                                                                                             <option value="230">MAURITIUS(MU)  +230 </option>
                                                                                             <option value="269">MAYOTTE(YT)  +269 </option>
                                                                                             <option value="52">MEXICO(MX)  +52 </option>
                                                                                             <option value="691">MICRONESIA, FEDERATED STATES OF(FM)  +691 </option>
                                                                                             <option value="373">MOLDOVA, REPUBLIC OF(MD)  +373 </option>
                                                                                             <option value="377">MONACO(MC)  +377 </option>
                                                                                             <option value="976">MONGOLIA(MN)  +976 </option>
                                                                                             <option value="1664">MONTSERRAT(MS)  +1664 </option>
                                                                                             <option value="212">MOROCCO(MA)  +212 </option>
                                                                                             <option value="258">MOZAMBIQUE(MZ)  +258 </option>
                                                                                             <option value="95">MYANMAR(MM)  +95 </option>
                                                                                             <option value="264">NAMIBIA(NA)  +264 </option>
                                                                                             <option value="674">NAURU(NR)  +674 </option>
                                                                                             <option value="977">NEPAL(NP)  +977 </option>
                                                                                             <option value="31">NETHERLANDS(NL)  +31 </option>
                                                                                             <option value="599">NETHERLANDS ANTILLES(AN)  +599 </option>
                                                                                             <option value="687">NEW CALEDONIA(NC)  +687 </option>
                                                                                             <option value="64">NEW ZEALAND(NZ)  +64 </option>
                                                                                             <option value="505">NICARAGUA(NI)  +505 </option>
                                                                                             <option value="227">NIGER(NE)  +227 </option>
                                                                                             <option value="234">NIGERIA(NG)  +234 </option>
                                                                                             <option value="683">NIUE(NU)  +683 </option>
                                                                                             <option value="672">NORFOLK ISLAND(NF)  +672 </option>
                                                                                             <option value="1670">NORTHERN MARIANA ISLANDS(MP)  +1670 </option>
                                                                                             <option value="47">NORWAY(NO)  +47 </option>
                                                                                             <option value="968">OMAN(OM)  +968 </option>
                                                                                             <option value="92">PAKISTAN(PK)  +92 </option>
                                                                                             <option value="680">PALAU(PW)  +680 </option>
                                                                                             <option value="970">PALESTINIAN TERRITORY, OCCUPIED(PS)  +970 </option>
                                                                                             <option value="507">PANAMA(PA)  +507 </option>
                                                                                             <option value="675">PAPUA NEW GUINEA(PG)  +675 </option>
                                                                                             <option value="595">PARAGUAY(PY)  +595 </option>
                                                                                             <option value="51">PERU(PE)  +51 </option>
                                                                                             <option value="63">PHILIPPINES(PH)  +63 </option>
                                                                                             <option value="0">PITCAIRN(PN)  +0 </option>
                                                                                             <option value="48">POLAND(PL)  +48 </option>
                                                                                             <option value="351">PORTUGAL(PT)  +351 </option>
                                                                                             <option value="1787">PUERTO RICO(PR)  +1787 </option>
                                                                                             <option value="974">QATAR(QA)  +974 </option>
                                                                                             <option value="262">REUNION(RE)  +262 </option>
                                                                                             <option value="40">ROMANIA(RO)  +40 </option>
                                                                                             <option value="70">RUSSIAN FEDERATION(RU)  +70 </option>
                                                                                             <option value="250">RWANDA(RW)  +250 </option>
                                                                                             <option value="290">SAINT HELENA(SH)  +290 </option>
                                                                                             <option value="1869">SAINT KITTS AND NEVIS(KN)  +1869 </option>
                                                                                             <option value="1758">SAINT LUCIA(LC)  +1758 </option>
                                                                                             <option value="508">SAINT PIERRE AND MIQUELON(PM)  +508 </option>
                                                                                             <option value="1784">SAINT VINCENT AND THE GRENADINES(VC)  +1784 </option>
                                                                                             <option value="684">SAMOA(WS)  +684 </option>
                                                                                             <option value="378">SAN MARINO(SM)  +378 </option>
                                                                                             <option value="239">SAO TOME AND PRINCIPE(ST)  +239 </option>
                                                                                             <option value="966">SAUDI ARABIA(SA)  +966 </option>
                                                                                             <option value="221">SENEGAL(SN)  +221 </option>
                                                                                             <option value="381">SERBIA AND MONTENEGRO(CS)  +381 </option>
                                                                                             <option value="248">SEYCHELLES(SC)  +248 </option>
                                                                                             <option value="232">SIERRA LEONE(SL)  +232 </option>
                                                                                             <option value="65">SINGAPORE(SG)  +65 </option>
                                                                                             <option value="421">SLOVAKIA(SK)  +421 </option>
                                                                                             <option value="386">SLOVENIA(SI)  +386 </option>
                                                                                             <option value="677">SOLOMON ISLANDS(SB)  +677 </option>
                                                                                             <option value="252">SOMALIA(SO)  +252 </option>
                                                                                             <option value="27">SOUTH AFRICA(ZA)  +27 </option>
                                                                                             <option value="0">SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS(GS)  +0 </option>
                                                                                             <option value="34">SPAIN(ES)  +34 </option>
                                                                                             <option value="94">SRI LANKA(LK)  +94 </option>
                                                                                             <option value="249">SUDAN(SD)  +249 </option>
                                                                                             <option value="597">SURINAME(SR)  +597 </option>
                                                                                             <option value="47">SVALBARD AND JAN MAYEN(SJ)  +47 </option>
                                                                                             <option value="268">SWAZILAND(SZ)  +268 </option>
                                                                                             <option value="46">SWEDEN(SE)  +46 </option>
                                                                                             <option value="41">SWITZERLAND(CH)  +41 </option>
                                                                                             <option value="963">SYRIAN ARAB REPUBLIC(SY)  +963 </option>
                                                                                             <option value="886">TAIWAN, PROVINCE OF CHINA(TW)  +886 </option>
                                                                                             <option value="992">TAJIKISTAN(TJ)  +992 </option>
                                                                                             <option value="255">TANZANIA, UNITED REPUBLIC OF(TZ)  +255 </option>
                                                                                             <option value="66">THAILAND(TH)  +66 </option>
                                                                                             <option value="670">TIMOR-LESTE(TL)  +670 </option>
                                                                                             <option value="228">TOGO(TG)  +228 </option>
                                                                                             <option value="690">TOKELAU(TK)  +690 </option>
                                                                                             <option value="676">TONGA(TO)  +676 </option>
                                                                                             <option value="1868">TRINIDAD AND TOBAGO(TT)  +1868 </option>
                                                                                             <option value="216">TUNISIA(TN)  +216 </option>
                                                                                             <option value="90">TURKEY(TR)  +90 </option>
                                                                                             <option value="7370">TURKMENISTAN(TM)  +7370 </option>
                                                                                             <option value="1649">TURKS AND CAICOS ISLANDS(TC)  +1649 </option>
                                                                                             <option value="688">TUVALU(TV)  +688 </option>
                                                                                             <option value="256">UGANDA(UG)  +256 </option>
                                                                                             <option value="380">UKRAINE(UA)  +380 </option>
                                                                                             <option value="971">UNITED ARAB EMIRATES(AE)  +971 </option>
                                                                                             <option value="44">UNITED KINGDOM(GB)  +44 </option>
                                                                                             <option value="1">UNITED STATES(US)  +1 </option>
                                                                                             <option value="1">UNITED STATES MINOR OUTLYING ISLANDS(UM)  +1 </option>
                                                                                             <option value="598">URUGUAY(UY)  +598 </option>
                                                                                             <option value="998">UZBEKISTAN(UZ)  +998 </option>
                                                                                             <option value="678">VANUATU(VU)  +678 </option>
                                                                                             <option value="58">VENEZUELA(VE)  +58 </option>
                                                                                             <option value="84">VIET NAM(VN)  +84 </option>
                                                                                             <option value="1284">VIRGIN ISLANDS, BRITISH(VG)  +1284 </option>
                                                                                             <option value="1340">VIRGIN ISLANDS, U.S.(VI)  +1340 </option>
                                                                                             <option value="681">WALLIS AND FUTUNA(WF)  +681 </option>
                                                                                             <option value="212">WESTERN SAHARA(EH)  +212 </option>
                                                                                             <option value="967">YEMEN(YE)  +967 </option>
                                                                                             <option value="260">ZAMBIA(ZM)  +260 </option>
                                                                                             <option value="263">ZIMBABWE(ZW)  +263 </option>
                                                                                         </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <div class="form-group">
                                                                                        <label>Contact Number</label>
                                                                                        <input type="text" class="form-control" name="" id="contact" <?=$info['contact']=='' ? '' : 'readonly'?>>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                 <div class="form-group">
                                                                                        <label>Date of Birth</label>
                                                                                     <div class="bootstrap-timepicker">
                                                                                            <input type="text" class="form-control" name="" id="birthdate" required="">
                                                                                     </div>
                                                                                 </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                        <label>Gender</label>
                                                                                        <select class="form-control" id="gender" required="">
                                                                                            <option>Select Gender</option>
                                                                                             <option value="Male">Male</option>
                                                                                             <option value="Female">Female</option>
                                                                                             <option value="Other">Other</option>
                                                                                        </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                        <label>Marital Status</label>
                                                                                        <select class="form-control" id="status" required="">
                                                                                            <option>Select Status</option>
                                                                                             <option value="Single">Single</option>
                                                                                             <option value="Married">Married</option>
                                                                                             <option value="Other">Other</option>
                                                                                        </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-sm-12">
                                                                                        <button type="submit" id="btn_update" class="form-control btn btn-primary waves-effect waves-light">Update</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                 </div>
                                                            </div>
                                                              
                                                    </div>
                                                        <div class="tab-pane" id="settings">
                                                                                 <div class="row">
                                                                                     <div class="col-sm-12">
                                                                                            <div class="panel panel-primary">
                                                                                                     <div class="panel-body">
                                                                                                            <h4 class="m-t-0 m-b-30">Profile Photo</h4>

                                                                                                                 <div class="row">
                                                                                                                            <div class="col-md-4">   
                                                                                                                            </div>
                                                                                                                   <?php echo form_open_multipart(site_url('account/updatephoto'),['id'=>'formPhoto']);?>  <input type="hidden" name="userid" id="userid" value="<?=$info['id']?>">
                                                                                                                     <div class="col-md-4">
                                                                                                                 
                                                                                                                     <?php

                                                                                                                                if($info['image_name']=="") {

                                                                                                                                
                                                                                                                      ?>
                                                                                                                        <img id='img-upload' src="<?=base_url()?>/assets/photos/no-thumbnail.png"  class="img-responsive" />

                                                                                                                    <?php } else {?>
                                                                                                                                <img id='img-upload' src="<?=base_url()?>/assets/photos/<?=$info['image_name']?>"  class="img-responsive" />
                                                                                                                    <?php }?>
                                                                                                                  
                                                                                                                    <br>

                        <div class="form-group">
                           
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Browse… <input type="file" id="file" name="image">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                            
                        </div>
                    

                                                                                            <div class="text-center m-t-15">
                                                                                                <button type="submit" id="btn_photo" class="btn btn-primary waves-effect waves-light form-control">Update</button>
                                                                                            </div>
                                                                                     </div>
                                                                                 </form>
                                                                                     <div class="col-md-4">  
                                                                                     </div>


                                                                         </div>
                                                    <!-- end row -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                        </div>
                                                        <div class="tab-pane " id="profile" >
                                                                 <div class="panel  panel-primary">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">Bank Details</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <form id="bankForm">
                                                                                <div class="row">
                                                                                    <div class="col-md-4"></div>
                                                                                    <div class="col-md-4">
                                                                                        <div class="form-group">
                                                                                            <input type="hidden" name="" value="<?=site_url()?>" id="url">
                                                                                            <label>Account Name</label>
                                                                                            <input type="text"  class="form-control" name="acc_name" id="account" <?=$info['acc_name']=='' ? '' : 'readonly'?>>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Account Number</label>
                                                                                            <input type="text"  class="form-control" name="acc_number" id="acc_number" <?=$info['acc_number']=='' ? '' : 'readonly'?>>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Bank Name</label>
                                                                                            <input type="text"  class="form-control" name="bank_name" id="bank_name" <?=$info['bank_name']=='' ? '' : 'readonly'?>>
                                                                                        </div>
                                                                                         <div class="form-group">
                                                                                            <label>Branch Name</label>
                                                                                            <input type="text"  class="form-control" name="branch_name" id="branch_name" <?=$info['branch_name']=='' ? '' : 'readonly'?>>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Ifsc / Swift Code</label>
                                                                                            <input type="text"  class="form-control" name="swift_code" id="swift_code" <?=$info['swift_code']=='' ? '' : 'readonly'?>>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                                <button type="submit" class="btn btn-primary waves-effect waves-light form-control" id="btn_bank">Update</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                     
                                                        <div class="tab-pane" id="messages">
                                                                 <div class="panel  panel-primary">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">Wallet Details</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <form id="walletForm">
                                                                                <div class="row">
                                                                                    <div class="col-md-4"></div>
                                                                                    <div class="col-md-4">
                                                                                        <div class="form-group">
                                                                                            <label>Bitcoin</label>
                                                                                            <input type="text"  class="form-control" name="btc" id="btc">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Ethereum</label>
                                                                                            <input type="text"  class="form-control" name="eth" id="eth">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Ethereum Classic</label>
                                                                                            <input type="text"  class="form-control" name="etc" id="etc">
                                                                                        </div>
                                                                                         <div class="form-group">
                                                                                            <label>Ripple</label>
                                                                                            <input type="text"  class="form-control" name="xrp" id="xrp">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label>Ripple Destination tag</label>
                                                                                            <input type="text"  class="form-control" name="dtag" id="dtag">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                                <button type="submit" class="btn btn-primary waves-effect waves-light form-control" id="btn_wallet">Update</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end row -->
                                </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                    <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <script src="<?=base_url()?>assets/plugins/timepicker/bootstrap-timepicker.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <script src="<?=base_url()?>assets/plugins/dropzone/dist/dropzone.js"></script>
        <!-- Plugins Init js -->
        <script src="<?=base_url()?>assets/pages/form-advanced.js"></script>
        <script src="<?=base_url()?>assets/pages/profile.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script> 

    <script type="text/javascript">
        $(function(){
            $('form').parsley();
                //$('#birthdate').datepicker();
            $('#formProfile').submit(function(event) {    
                // get the form data
                // there are many ways to get this data using jQuery (you can use the class or id also)
                 $('#btn_update').attr('disabled',true);
                 $('#btn_update').html('<span class="fa fa-spin fa-spinner"></span>Upating profile ... Please wait');
                var formData = {
                                'imm_token'     : $('#token').val(),
                                'fname'         : $('#fname').val(),
                                'lname'         : $('#lname').val(),
                                'email'         : $('#email').val(),
                                'passwd'        : $('#passwd').val(),
                                'oldpasswd'     : $('#oldpasswd').val(),
                                'confirmpasswd' : $('#confirmpasswd').val(),
                                'oldtranspasswd': $('#oldtranspasswd').val(),
                                'transpasswd'   : $('#transpasswd').val(),
                                'ctranspasswd'  : $('#ctranspasswd').val(),
                                'address'       : $('#address').val(),
                                'gender'        : $('#gender').val(),
                                'country'       : $('#country').val(),
                                'state'         : $('#state').val(),
                                'city'          : $('#city').val(),
                                'zipcode'       : $('#zipcode').val(),
                                'countrycode'   : $('#countrycode').val(),
                                'status'        : $('#status').val(),
                                'userd'         : $('#userd').val(),
                                'contact'       : $('#contact').val(),
                                'birthdate'     : $('#birthdate').val(),
                             };
                // process the form
                $.ajax({
                    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url         : 'updateinfo', // the url where we want to POST
                    data        : formData, // our data object
                    dataType    : 'json', // what type of data do we expect back from the server
                                encode          : true
                })
                    // using the done promise callback
                    .done(function(data) {
                             swal(data.title, data.msg, data.status);
                               $('#btn_update').attr('disabled',false);
                                $('#btn_update').html('<span>Update</span')
                        // log data to the console so we can see
                        //console.log(data.status); 
                        if(data.status =='success') {
                                 $.Profile.init();
                        }
                        // here we will handle errors and validation messages
                    });
                // stop the form from submitting the normal way and refreshing the page
                event.preventDefault();
            });
   //image preview after select
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });
        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#file").change(function(){
            readURL(this);
        });     
        }) ;
    </script>
    <!--Upload image-->
    <script type="text/javascript">
      $("body").on("click","#btn_photo",function(e){
        
           
        $("#formPhoto").ajaxForm(options);

      });

     var options = { 
       complete: function(response) 
       {
         console.log(response.responseText);
        if($.isEmptyObject(response.responseJSON.error)){
            //alert('Image Upload Successfully.');
             swal('Good job', 'Your photo has been Successfully uploaded', 'success');
    
            $(".preview").css("display","block");
            $(".preview").find("img").attr("src","/uploads/"+response.responseJSON.success);
              $('#btn_photo').attr('disabled',false);
            $('#btn_photo').html('<span></span>Update');
         }else{
           // alert('Image Upload Error.');
             swal('Oops', response.responseText, 'error');
               $('#btn_photo').attr('disabled',false);
            $('#btn_photo').html('<span></span>Update');
         }
       }
     };
</script>
<!--update bank details-->
<script type="text/javascript">
     $('#bankForm').submit(function(event) {    
                // get the form data

                // there are many ways to get this data using jQuery (you can use the class or id also)
                 $('#bankForm #btn_bank').attr('disabled',true);
                 $('#bankForm #btn_bank').html('<span class="fa fa-spin fa-spinner"></span>Upating bank details ... Please wait');
                var formData = {
                                                 'imm_token'     : $('#token').val(),
                                               'acc_name'    : $('#bankForm #account').val(),
                                               'acc_no'      : $('#bankForm #acc_number').val(),
                                               'bank_name'   : $('#bankForm #bank_name').val(),
                                               'branch_name' : $('#bankForm #branch_name').val(),
                                               'swift_code'  : $('#bankForm #swift_code').val(),
                                               'userd'       : $('#formProfile #userd').val(),
                        
                             };
                // process the form
                $.ajax({
                    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url         : 'updatebankinfo', // the url where we want to POST
                    data        : formData, // our data object
                    dataType    : 'json', // what type of data do we expect back from the server
                    encode          : true
                })
                    // using the done promise callback
                    .done(function(data) {
                             swal(data.title, data.msg, data.status);
                               $('#bankForm #btn_bank').attr('disabled',false);
                               $('#bankForm #btn_bank').html('<span>Update</span')
                        // log data to the console so we can see
                        //console.log(data.status); 
                        if(data.status =='success') {
                                 $.Profile.init();
                        }
                        // here we will handle errors and validation messages
                    });
                // stop the form from submitting the normal way and refreshing the page
                event.preventDefault();
            });     
</script>

<script type="text/javascript">
     $('#walletForm').submit(function(event) {    
                // get the form data

                // there are many ways to get this data using jQuery (you can use the class or id also)
                 $('#walletForm #btn_wallet').attr('disabled',true);
                 $('#walletForm #btn_wallet').html('<span class="fa fa-spin fa-spinner"></span>Upating wallet details ... Please wait');
                var formData = {
                                                 'imm_token'     : $('#token').val(),
                                               'btc'    : $('#walletForm #btc').val(),
                                               'eth'      : $('#walletForm #eth').val(),
                                               'etc' : $('#walletForm #etc').val(),
                                               'xrp' : $('#walletForm #xrp').val(),
                                               'dtag'  : $('#walletForm #dtag').val(),
                                               'userd'       : $('#formProfile #userd').val(),
                        
                             };
                // process the form
                $.ajax({
                    type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url         : 'updatewalletinfo', // the url where we want to POST
                    data        : formData, // our data object
                    dataType    : 'json', // what type of data do we expect back from the server
                    encode          : true
                })
                    // using the done promise callback
                    .done(function(data) {
                             swal(data.title, data.msg, data.status);
                               $('#walletForm #btn_wallet').attr('disabled',false);
                               $('#walletForm #btn_wallet').html('<span>Update</span')
                        // log data to the console so we can see
                        //console.log(data.status); 
                        if(data.status =='success') {
                                 $.Profile.init();
                        }
                        // here we will handle errors and validation messages
                    });
                // stop the form from submitting the normal way and refreshing the page
                event.preventDefault();
            });

                   
</script>



<?php $this->view('users/body_footer')?>