<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#img-upload{
    width: 50%;
    position: center;       
}
.card .card_icon,  .card .status_icon {
  /* For a more robust cross-browser implementation, see http://bit.ly/aqZnl3 */
  display: inline-block;
  vertical-align: bottom;
  height: 23px;
  width: 27px;
}

/* --- Card Icon --- */

.card .card_icon { background: transparent url('http://localhost/member1/assets/img/credit_card_sprites.png') no-repeat 30px 0; }

/* Need to support IE6? These four rules won't work, so rewrite 'em. */

.card .card_icon.visa { background-position: 0 0 !important; }

.card .card_icon.mastercard { background-position: -30px 0 !important; }

.card .card_icon.amex { background-position: -60px 0 !important; }

.card .card_icon.discover { background-position: -90px 0 !important; }

/* --- Card Status --- */

.card .status_icon { background: transparent url('http://localhost/member1/assets/img/status_sprites.png') no-repeat 33px 0; }

.card .invalid {
  color: #AD3333;
  background: #f8e7e7;
}

.card .valid {
  color: #33AD33;
  background: #e7f8e7;
}

.card .invalid .status_icon { background-position: 3px 0 !important; }

.card .valid .status_icon { background-position: -27px 0 !important; }
</style>  


 <!-- jQuery is used only for this example; it isn't required to use Stripe -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/js/bootstrap.min.js" />

    <!-- Stripe JavaScript library -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>    
    
    <script type="text/javascript">
        //set your publishable key
        Stripe.setPublishableKey('pk_live_gzQWobLB8Uis8dW13f0hzys8');
        
        //pk_test_iu1l1HPuUeXqmyB193QpHzM0
        //pk_live_gzQWobLB8Uis8dW13f0hzys8
        
        //callback to handle the response from stripe
        function stripeResponseHandler(status, response) {
            if (response.error) {
                //enable the submit button
                $('#payBtn').removeAttr("disabled");

                $('#payBtn').html('Submit Payment');
                //display the errors on the form
                // $('#payment-errors').attr('hidden', 'false');
                $('#payment-errors').addClass('alert alert-danger');
                $("#payment-errors").html(response.error.message);
            } else {
                var form$ = $("#paymentFrm");
                //get token id
                var token = response['id'];
                //insert the token into the form
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                //submit form to the server
                form$.get(0).submit();
            }
        }
        $(document).ready(function() {
            //on form submit
            $("#paymentFrm").submit(function(event) {
                //disable the submit button to prevent repeated clicks
                $('#payBtn').attr("disabled", "disabled");
                 $('#payBtn').html('<span class="fa fa-spin fa-spinner"> </span> Processing . .');
                
                //create single-use token to charge the user
                Stripe.createToken({
                    number: $('#card_num').val(),
                    cvc: $('#card-cvc').val(),
                    exp_month: $('#card-expiry-month').val(),
                    exp_year: $('#card-expiry-year').val()
                }, stripeResponseHandler);
                
                //submit from callback
                return false;
            });
        });
    </script>
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">PACKAGE PURCHASE</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        
                        <div class="container"> 
    
                                                <div class="row">   
                                                        <div class="col-md-4"> <p><php echo number_format(1000.5, 2, '.', ''); ?></p></div>
                                                        <div class="col-md-4">
                                                            
                                                                        <p style="color:#fff">Note : 7% Credit/Debit card payment processing fee may levy. </p>
                                                                        <p>Example :  <i style="color:#fff">$100</i> (amount purchase and will go to your portfolio) +  <i style="color:#fff">$7</i> (7% for 100 processing fee) = <i style="color:#fff">$107</i> (Amount will be deducted to your credit/debit card)</p>
                                                                

                                                                         <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                    <?php endif ?>
                    <div id="payment-errors"></div>  
                     
                        <?=form_open('account/pay',['id'=>'paymentFrm','enctype'=>'multipart/form-data'])?>
                         <div class="form-group">
                             <label>Amount :</label>
                            <input type="number" name="amount" class="form-control" placeholder="Amount"  required  min="10">
                        </div> 
                         <div class="form-group">
                             <label>Transaction Type :</label>
                            <select class="form-control" name="typ">
                                <option value="package">Investment Package</option>
                                 <option value="coin">IMM COIN</option>
                            </select>
                        </div> 
                        <div class="form-group">
                             <label>Name :</label>
                            <input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo set_value('name'); ?>" required>
                        </div>  
                        
                        <div class="form-group">
                             <label>Email :</label>
                            <input type="email" name="email" class="form-control" placeholder="email@you.com" value="<?php echo set_value('email'); ?>" required />
                        </div>

                         <div class="form-group">
                             <label>Card No. </label>
                             
                            <input type="number" name="card_num" id="card_num" class="form-control" placeholder="Card Number" autocomplete="off" value="<?php echo set_value('card_num'); ?>" required> 
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                 <label>Expiration </label>
                            </div>
                        </div>
                        <div class="row">
                                 
                            <div class="col-sm-8">
                                 <div class="row">
                                     
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="exp_month" maxlength="2" class="form-control" id="card-expiry-month" placeholder="MM" value="<?php echo set_value('exp_month'); ?>" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="exp_year" class="form-control" maxlength="4" id="card-expiry-year" placeholder="YYYY" required="" value="<?php echo set_value('exp_year'); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="cvc" id="card-cvc" maxlength="3" class="form-control" autocomplete="off" placeholder="CVC" value="<?php echo set_value('cvc'); ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                          <button class="btn btn-secondary" type="reset">Reset</button>
                          <button type="submit" id="payBtn" class="btn btn-success">Submit Payment</button>
                        </div>
                    </form>     

                                                        </div>
                                                        <div class="col-md-4"></div>
                                                </div>
  
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
        <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <!--Custom script-->
        <script src="<?=base_url()?>assets/pages/ewallet.js"></script>
        <script src="<?=base_url()?>assets/check/src/jquery.cardcheck.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
            <script type="text/javascript">
        $(window).on('load',function(){
                 $('#investmentModal').modal('show');
        });
    </script>
    
<script>
    jQuery(function($) {
        
        // If JavaScript is enabled, hide fallback select field
        $('.no-js').removeClass('no-js').addClass('js');
        
        // When the user focuses on the credit card input field, hide the status
        $('.card input').bind('focus', function() {
            $('.card .status').hide();
        });
        
        // When the user tabs or clicks away from the credit card input field, show the status
        $('.card input').bind('blur', function() {
            $('.card .status').show();
        });
        
        // Run jQuery.cardcheck on the input
        $('.card input').cardcheck({
            callback: function(result) {
                
                var status = (result.validLen && result.validLuhn) ? 'valid' : 'invalid',
                    message     = '',
                    types       = '';
                // Get the names of all accepted card types to use in the status message.
                for (i in result.opts.types) {
                    types += result.opts.types[i].name + ", ";
                }
                types = types.substring(0, types.length-2);
                // Set status message
                if (result.len < 1) {
                    message = 'Please provide a credit card number.';
                } else if (!result.cardClass) {
                    message = 'We accept the following types of cards: ' + types + '.';
                } else if (!result.validLen) {
                    message = 'Please check that this number matches your ' + result.cardName + ' (it appears to be the wrong number of digits.)';
                } else if (!result.validLuhn) {
                    message = 'Please check that this number matches your ' + result.cardName + ' (did you mistype a digit?)';
                } else {
                    message = 'Great, looks like a valid ' + result.cardName + '.';
                }
                // Show credit card icon
                $('.card .card_icon').removeClass().addClass('card_icon ' + result.cardClass);
                
                // Show status message
                $('.card .status').removeClass('invalid valid').addClass(status).children('.status_message').text(message);
            }
        });
    });
  </script>

<?php $this->view('users/body_footer')?>