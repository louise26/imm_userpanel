<?php $this->view('users/body_header')?>
<link href="<?=base_url()?>assets/plugins/fullcalendar/css/fullcalendar.min.css" rel="stylesheet" />
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?php echo $info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                         <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>

                    <div class="page-content-wrapper ">
                            
                        <div class="container">
                            
                            <!--Start row-->
                                       <!--Start row-->
                            <div class="row">
                                    <div class="col-sm-12">
                                    <?php
                                                $title = "";
                                                $content = "";
                                                 foreach ($announcement as $key => $value) {
                                                            $content= $value->description;
                                                            $title= $value->news_name;
                                                    } 
                                               ?>
                                                <?php
                                                    if($content ==""){
                                                                echo '<span class="pull-right" style="color:orange">NO OFFICIAL ANNOUNCEMENT YET !</span>';
                                                        }
                                                        else {

                                                     ?>
                                                <div class="panel">
                                                    <div class="panel-body user-card">
                                                        <div class="media-main">
                                                            <a class="pull-left" href="#">
                                                                <img class="thumb-lg img-circle" src="assets/images/default.ico" alt="">
                                                            </a>
                                                            <div class="info">
                                                                <h4>OFFICIAL ANNOUNCEMENT</h4>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>

                                                        <p class="text-muted info-text">

                                                           
                                                           <label><?=$title?></label>
                                                           <p><?=$content?></p>
                                                        </p>
                                                         <br>
                                                     </br>
                                                     <br>   
                                                    </div> <!-- panel-body -->
                                                </div>

                                    <?php }?>
                                    </div>
                                   
                            </div>
                                  <!--Start row-->
                            <div class="row">
                                <div class="col-sm-6 col-lg-3">
                                    <div class="panel text-center">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-muted font-light">Referrals</h4>
                                        </div>
                                        <div class="panel-body p-t-10">
                                            <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b><?php echo $total_referral?></b></h2>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-3">
                                    <div class="panel text-center">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-muted font-light">Downlines</h4>
                                        </div>
                                        <div class="panel-body p-t-10">
                                            <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b><?php echo $total_downline?></b></h2>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-3">
                                    <div class="panel text-center">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-muted font-light">Paid Downlines</h4>
                                        </div>
                                        <div class="panel-body p-t-10">
                                            <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><span id="paid">0</span></h2>    
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-3">
                                    <div class="panel text-center">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-muted font-light">Unpaid Downlines</h4>
                                        </div>
                                        <div class="panel-body p-t-10">
                                           <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-down text-danger m-r-10"></i><b id="unpaid">0</b></h2>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end row -->
                            <div class="row">
                                
                                
                                        <div class="col-sm-6 col-lg-3">
                                    <div class="panel text-center">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-muted font-light">E-WALLET BALANCE</h4>
                                        </div>
                                        <div class="panel-body p-t-10">
                                            <h2 class="m-t-0 m-b-15"><i class="mdi mdi-currency-usd text-success m-r-10"></i><b>
                                                <?php 
                                                    foreach ($ewallet_balance as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                    }
                                                ?>

                                                </b></h2>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-3">
                                    <div class="panel text-center">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-muted font-light">R-WALLET BALANCE</h4>
                                        </div>
                                        <div class="panel-body p-t-10">
                                            <h2 class="m-t-0 m-b-15"><i class="mdi mdi-currency-usd text-success m-r-10"></i><b>
                                                


                                                <?php 
                                                    foreach ($rwallet_balance as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                    }
                                                ?>
                                                    
                                            </b></h2>
                                            
                                        </div>
                                    </div>
                                </div>
                                     <div class="col-sm-6 col-lg-3">
                                    <div class="panel text-center">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-muted font-light">TOTAL INCOME</h4>
                                        </div>
                                        <div class="panel-body p-t-10">
                                            <h2 class="m-t-0 m-b-15"><i class="mdi mdi-currency-usd text-success m-r-10"></i><span id="total_income">0.00</span></h2>
                                            
                                        </div>
                                    </div>
                                </div>
                                        <div class="col-sm-6 col-lg-3">
                                        <div class="panel text-center">
                                            <div class="panel-heading">
                                                <h4 class="panel-title text-muted font-light">IMMCOIN BALANCE</h4>
                                            </div>
                                            <div class="panel-body p-t-10">
                                                <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up  text-success m-r-10"></i><b>
                                                    

                                                <?php 
                                                    foreach ($immcoin as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                    }
                                               



                                                ?>
                                                </b></h2>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <!-- end row -->
                            <div class="row">
                                     <div class="col-sm-6 col-lg-3">
                                        <div class="panel text-center">
                                            <div class="panel-heading">
                                                <h4 class="panel-title text-muted font-light">RIPPLE BALANCE</h4>
                                            </div>
                                            <div class="panel-body p-t-10">
                                                <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b>
                                                    

                                                <?php 
                                                    foreach ($xrp as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                    }
                                               



                                                ?>
                                                </b></h2>
                                                
                                            </div>
                                        </div>
                                    </div>


                                         <div class="col-sm-6 col-lg-3">
                                        <div class="panel text-center">
                                            <div class="panel-heading">
                                                <h4 class="panel-title text-muted font-light">ETHEREUM BALANCE</h4>
                                            </div>
                                            <div class="panel-body p-t-10">
                                                <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b>
                                                    

                                                <?php 
                                                    foreach ($eth as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                    }
                                               
                                              ?>
                                                </b></h2>
                                                
                                            </div>
                                        </div>
                                    </div>


                                     <div class="col-sm-6 col-lg-3">
                                        <div class="panel text-center">
                                            <div class="panel-heading">
                                                <h4 class="panel-title text-muted font-light">ETC BALANCE</h4>
                                            </div>
                                            <div class="panel-body p-t-10">
                                                <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b>    
                                                <?php 
                                                    foreach ($etc as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                    }
                                                ?>
                                                </b></h2>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-3">
                                        <div class="panel text-center">
                                            <div class="panel-heading">
                                                <h4 class="panel-title text-muted font-light">LITECOIN</h4>
                                            </div>
                                            <div class="panel-body p-t-10">
                                                <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-up text-success m-r-10"></i><b>       

                                                <?php 
                                                    foreach ($ltc as $key => $value) {
                                                         echo number_format($value->amount,2);
                                                    }
                                        
                                                ?>
                                                </b></h2>
                                                
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <!--Start row-->
                           
                            <!-- end row -->
                            <!--start row-->
                                <div class="row">
                                      <div class="col-md-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <h4 class="m-t-0">YOUR INCOME </h4> <span id="msg2" ><i class="fa fa-spin fa-spinner"> </i> Calculating earnings .. please wait</span>
                                                        <div id="morris-bar-example" style="height: 300px"></div>
                                                    </div>
                                                </div>
                                      </div>
                                      
                                </div>
                            <!--end row-->
                            <div class="row">
                                  <div class="col-sm-6">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Event Calendar</h3> <span id="msg1" ><i class="fa fa-spin fa-spinner"> </i> Loading calendar .. please wait</span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
            
                                                <div id='calendar'></div>

                                            </div>
                                            <!-- end row -->
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                             <div class="panel">
                                            <div class="panel-heading">
                                              <h4>Latest Registered Referral</h4>
                                            </div>
                                            <div class="panel-body" style="min-height:284px;">
                                                 <div class="table-responsive">
                                                        <table class="table table-hover m-b-0">
                                            <?php foreach($latest_referral as $key=>$value){?>
                                                
                                                            
                                             <tr><td> <?=date('F d, Y',strtotime($value->registration_date))?></td><td><a class="text-success">( <?php echo $value->user_id;?> )</a>&nbsp;&nbsp;<?php echo $value->first_name."".$value->last_name;?> Created a new account</td></tr>
                                               
                                               <?php }?>
                                                
                                             
                                               </tbody>
                                                        </table>
                                                    </div>
                                            </div>
                                         </div>
                                            <div class="panel">
                                        <div class="panel-body">
                                            <h4 class="m-b-30 m-t-0">Top Transactions</h4>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover m-b-0">
                                                           <?php foreach($top_history as $key=>$value){?>
                                                            <tr>
                                                                <td><i class="fa fa-check text-success"> </i> <?php echo $value->ttype;?> on <?php echo date('F d, Y',strtotime($value->receive_date));?></td>
                                                                
                                                            </tr>
                                                            <?php }?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                            </div>

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                    <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
       
         
        <script src="<?=base_url()?>assets/pages/dash_board.js"></script>

          <script src="<?=base_url()?>assets/plugins/chart.js/chart.min.js"></script>
        <script src="<?=base_url()?>assets/pages/chartjs.init.js"></script>
        
  
 
      <script src="<?=base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/moment/moment.js"></script>
        <script src='<?=base_url()?>assets/plugins/fullcalendar/js/fullcalendar.min.js'></script>
       
    
    

    <script type="text/javascript">
        var json_events ;
        $(function(){

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();


               var atay = []; 
               
            $.getJSON('calendar',function(data){
           
                                            for ( var i = 0 ; i < data.length ; i ++) {
                                                            atay.push({title :data[i].title,start :new Date(data[i].start),end :new Date(data[i].end),allDay :new Date(data[i].allDay)});
                                                                      
                                            }
                                                               
               
                         $('#calendar').fullCalendar({
                                         header: {
                                                left: 'prev,next today',
                                                center: 'title',
                                                right: 'month,basicWeek,basicDay'
                                            },
                                                 events: atay
                                    }); 
                     
                            $('#msg1').hide();
                            $('#msg2').hide();

            })

                      
        })
    </script>
   <?php $this->view('users/body_footer')?>

