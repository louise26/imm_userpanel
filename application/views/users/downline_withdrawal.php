<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 50%;
    position: center;       
}
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">DOWNLINE WITHDRAWALS</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-md-3">
                                                             
                                                           <!-- <form method="post" id="withdrawal">-->
                                                                
                                                                <?=form_open('',['id'=>'withdrawal'])?>
                                                               
                                                                  <div class="form-group">
                                                                      <label>Transaction Type</label>
                                                                      <select class="form-control" id="type">
                                                                          <option value="all">All</option>
                                                                          <option value="Bitcoin">Bitcoin</option>
                                                                          <option value="Ripple">Ripple</option>
                                                                          <option value="Bankwire">Bankwire</option>
                                                                      </select>
                                                                  </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                              <div class="form-group">
                                                                <label>From</label>
                                                                <input type="text" name="datepicker" class="form-control" id="dateFrom" required="" readonly="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                              <div class="form-group">
                                                                <label>To</label>
                                                                <input type="text" name="datepicker2" class="form-control" id="dateTo" required="" readonly="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                                 <div class="form-group">
                                                                    <br>
                                                                    <button type="submit" class="btn btn-primary" id="btn_search">Search</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                </div>
                                            
                                                <hr>
                                            <div class="row">
                                                            <table id="direct-income" class="table table-striped table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>User ID</th>
                                                            <th>Fullname</th>
                                                            <th>Amount (USD)</th>
                                                            <th>Amount (INR)</th>
                                                            <th>Transaction Type</th>
                                                            <th>Date</th>
                                                            <th>Remarks</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                            </div>
                                            

                                        </div>
                                    </div>
                                </div>

                            </div> <!-- End Row -->


                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                    <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
         <!-- Plugins Init js -->
     
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <script src="<?=base_url()?>assets/plugins/timepicker/bootstrap-timepicker.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
           <script src="<?=base_url()?>assets/pages/form-advanced.js"></script>
        
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
        
    

<script>
    $(document).ready(function() {
        $('#dateFrom').datepicker({
             autoclose: true,
             todayHighlight: true
        });
        $('#dateTo').datepicker({
             autoclose: true,
            todayHighlight: true
        });
                
            
        $('#direct-income').DataTable();

    });
</script>


<script type="text/javascript">


     $('#withdrawal').submit(function() {    
                
                 $('#btn_search').attr('disabled',true);
                 $('#btn_search').html('<span class="fa fa-spin fa-spinner"></span>Loading  ...');
                var formData = {
                                    'imm_token'  : $('[name=imm_token]').val(),
                                    'type'       : $('#type').val(),
                                    'dateFrom'   : $('#dateFrom').val(),
                                    'dateTo'     : $('#dateTo').val(),
                                                
                             };
                
                if($('#dateFrom').val() !="" && $('#dateTo').val() !=""){
                             $.ajax({                          
                                        type        : 'POST', 
                                        url         : 'downline-withdrawals-search', 
                                        data        : formData, 
                                        dataType    : 'json', 
                                        encode          : true
                                    }).done(function(data) {
                                                $('#btn_search').attr('disabled',false);
                                                $('#btn_search').html('<span>Search</span')   ;
                                                populateDataTable(data); 
                                        });
                }
                else {
                           swal('Oops !','Specify the date range first', 'warning');
                           $('#btn_search').attr('disabled',false);
                           $('#btn_search').html('<span>Search</span');
                }
               
    
                event.preventDefault();
            });

 
  function populateDataTable(data) {

   
    $("#direct-income").DataTable().destroy();
           
          $("#direct-income tbody tr").remove();

                $.each(data, function() {
                    $.each(this, function(k, v) {
                                   
                    $('#direct-income tbody').append('<tr><td>'+v.id+'</td><td>'+v.user_id+'</td><td>'+v.fullname+'</td><td>'+v.amount+'</td><td>'+v.amount_inr+'</td><td>'+v.trans_type+'</td><td>'+v.date+'</td><td>'+v.remark+'</td></tr>');

                        });
                });

      $('#direct-income').DataTable({

         dom: "Bfrtip",
            buttons: [{
                extend: "copy",
                className: "btn-success"
            }, {
                extend: "csv"
            }, {
                extend: "excel"
            }, {
                extend: "pdf"
            }, {
                extend: "print"
            }],
            "language": {
                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                  },
            responsive: !0
      });

    
   

  }

            
</script>


<?php $this->view('users/body_footer')?>