<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
                      position: relative;
                      overflow: hidden;
                  }
                  .btn-file input[type=file] {
                      position: absolute;
                      top: 0;
                      right: 0;
                      min-width: 100%;
                      min-height: 100%;
                      font-size: 100px;
                      text-align: right;
                      filter: alpha(opacity=0);
                      opacity: 0;
                      outline: none;
                      background: white;
                      cursor: inherit;
                      display: block;
                  }
                  #img-upload{
                      width: 50%;
                      position: center;       
                  }
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">CLOSE ACCOUNT CONFIRMATION</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row">   
                                                    
                                                    <div class="col-md-4">
                              
                                                    </div>
                                                    <div class="col-md-4"> 


                                                                <center><img src="<?=base_url()?>assets/images/closeaccount.png" class="img-responsive"></center>
                                                                <br>
                                                            <form name="confirmForm" action="<?=site_url()?>portfolio/close/confirm" method="post">  
                                                                 <input type="hidden"  name="imm_token" id="token" value="<?=$this->security->get_csrf_hash();?>">
                                                                <input type="hidden" name="ljid"  value="<?=$lifejacket_id?>">
                                                                <input type="hidden" name="amounts" value="<?=$this->uri->segment(4)?>">
                                                               
                                                                <div class="form-group">   
                                                                        <label> ARE YOU SURE YOU WANT TO CLOSE YOUR ACCOUNT ?</label>

                                                                       
                                                                </div>
                                                                
                                                                <div class="form-group">   
                                                                        <button type="submit" id="" class="btn btn-primary form-control"><span class="fa fa-paper-plane-o"> </span> CONFIRM   </button>
                                                                      
                                                                </div>
                                                                <div class="form-group">
                                                                       <a href="<?=site_url()?>portfolio/details" id="" class="btn btn-primary form-control"><span class="fa fa-incorrect"> </span> CANCEL   </a>
                                                                </div>
                                                            </form>  
                                                    </div>
                                                    <div class="col-md-4">   </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
          <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script>
    $(document).ready(function() {
            
            $('#btn_transfer').attr('disabled',true);
                    var net = 0,total = 0;

             $('[name=amount]').focusout(function() {

                    net     =  parseFloat($(this).val());
                    total   =  parseFloat($('#balances').val());
                      

                      if(total  > net){

                              $('#btn_transfer').attr('disabled',false);
                      }
                      else {
                          swal('OOps !', 'Not Enough balance', 'warning');
                            $('#btn_transfer').attr('disabled',true);
                      }            
              });
            $('[name=receiver]').focusout(function() {

                               $.ajax({
                                  type: "POST",
                                  url: 'search-user',
                                  data:   {  
                                            
                                              'user'         :   $(this).val(),
                
                                         },
                                  cache: false,
                                  success: function(data){
                                        var obj = JSON.parse(data);
                                          //console.log(obj.title);
                                          console.log(obj[0].email);
                                          $('#user_name').html('<p>'+obj[0].email+'</p>');

                                          $('[name=receiver_name]').val(obj[0].name);

                                          if(obj[0].name =="") {
                                                 $('#btn_transfer').attr('disabled',true);
                                          }
                                        else {

                                            $('#btn_transfer').attr('disabled',false);
                                        }                     
                                  }

                            });
              });

              $('[name=transForm]').submit(function(e){
                            $('#btn_transfer').attr('disabled',true);
                            $('#btn_transfer').html('<span class="fa fa-spin fa-spinner"> </span> Transferring .. ');

                                  $.ajax({
                                  type: "POST",
                                  url: 'rwallet-transfer',
                                  data:   {  
                                              't_code'          :   $('[name=t_code]').val(),
                                              'password'        :   $('[name=password]').val(),
                                              'sender_id'       :   $('[name=sender_id]').val(),
                                              'sender_name'     :   $('[name=sender_name]').val(),
                                              'amount'          :   $('[name=amount]').val(),
                                              'balance'         :   $('[name=balance]').val(),

                                              'receiver_id'     :   $('[name=receiver]').val(),
                                              'receiver_name'   :   $('[name=receiver_name]').val(),

                                            
                                         },
                                  cache: false,
                                  success: function(data){
                                        var obj = JSON.parse(data);     
                                                      swal(obj.title, obj.msg, obj.status);
                                                      $('#btn_transfer').attr('disabled',false);
                                                      $('#btn_transfer').html('<span class="fa fa-paper-plane-o"> </span>Transfer');
                                                      $('[name=amount]').val('');
                                                      $('[name=password]').val('');
                                                      $('[name=receiver]').val('');


                                  }

                                });
                        e.preventDefault();
                  });


    });
</script>

<?php $this->view('users/body_footer')?>