<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                
                 .ui-datepicker-today a.ui-state-highlight {
        border-color: #d3d3d3;
        background: #e6e6e6 url(/themeroller/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x;;
        color: #555555;    
    }
   .ui-datepicker-today.ui-datepicker-current-day a.ui-state-highlight {
        border-color: #aaaaaa;
        background: #ffffff url(images/ui-bg_glass_65_ffffff_1x400.png) 50% 50% repeat-x;
        color: #212121;
    }
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 50%;
    position: center;       
}
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">DOWNLINE PURCHASES </h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">    
                        
                                         <div class="row">
                                                        <div class="col-md-3">
                                                             
                                                           <!-- <form method="post" id="withdrawal">-->
                                                                
                                                                <?=form_open('reports/search-downline-purchase',['id'=>''])?>
                                                               
                                                                  <div class="form-group">
                                                                      <label>User ID</label>
                                                                      <input type="text" class="form-control" name="userid">
                                                                  </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                              <div class="form-group">
                                                                <label>From</label>
                                                                <input type="date" name="df" class="form-control"   >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                              <div class="form-group">
                                                                <label>To</label>
                                                                <input type="date" name="dt" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                                 <div class="form-group">
                                                                    <br>
                                                                    <button type="submit" class="btn btn-primary" id="btn_search">Search</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                </div>
                                                
                                                <hr>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            

                                            <table id="direct-income" class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                  
                                                    <th>Downline ID</th>
                                                    <th>Downline Username</th>
                                                    <th>Amount</th>
                                                    <th>Remark</th>
                                                    
                                                    <th>Purchase Date</th>

                                                </tr>
                                                 </thead>
                                                <tbody>
                                                
                                                    <?php $total =0; $i = 0 ; foreach($result as $result) {  $i +=1;$total =$total + $result['amount']; ?>
                                                            <tr>
                                                            <td><?=$i?></td>
                                                            <td><?=$result['user_id']?></td>
                                                            <td><?=$result['username']?></td>
                                                            <td><?='$'. $result['amount']?></td>
                                                            <td><?=$result['remark']?></td>
                                                            <td><?=$result['date']?></td>
                                                            </tr>
                                                    
                                                    <?php }?>
                                                </tbody>
                                                
                                               
                                            </table>
                                            
                                            
                                            
                                            <h2>Total :$ <?=$total?> </h2>

                                        </div>
                                    </div>
                                </div>

                            </div> <!-- End Row -->


                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                    <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
         <script src="<?=base_url()?>assets/plugins/timepicker/bootstrap-timepicker.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
         <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
    
<script>
    $(document).ready(function() {
        $('#direct-income').DataTable();
        
         
    });
</script>

<?php $this->view('users/body_footer')?>