<?php $this->view('users/body_header')?>
                <link href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#img-upload{
    width: 50%;
    position: center;       
}
</style>  
            <!-- Left Sidebar End -->
            <!-- Star right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <input type="hidden" id="user_id" value="<?=$info['user_id'] ?>" />
                         <input type="hidden" id="base_url" value="<?=site_url()?>" />
                    <div class="">
                        <div class="page-header-title">
                            <h4 class="page-title">PACKAGE PURCHASE</h4>
                        </div>
                    </div>
                    <div class="page-content-wrapper ">
                        <div class="container">                                                           

                                                <div class="row">   
                                                        <div class="col-md-4"></div>
                                                        <div class="col-md-4">
                                                            
                                                                 <form name="investmentForm">
                                                                           <input type="hidden" id="token" value="<?=$this->security->get_csrf_hash();?>">
                                                                            <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Amount   (USD)</label>
                                                                                            <input type="number" name="amount" min="10"  class="form-control" required="" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                               <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label>Payment Method</label>
                                       
                                                                                            <select class="form-control">   
                                                                                                    <option value="rwallet">Register-Wallet</option>    
                                                                                            </select>
                                                                                    </div>
                                                                                </div>  
                                                                            </div>
                                                                             <div class="row">
                                                                                    <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                            <label>Transaction Password</label>
                                                                                            <input type="password" name="password" class="form-control" required="" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">   
                                                                                    <div class="col-md-12">   
                                                                                     
                                                                                                    <button type="submit" class="btn btn-primary form-control" id="btn_purchase">   Submit </button>   
                                                                                        
                                                                                    </div>
                                                                            </div>
                                                                   
                                                                    
                                                                    </form>


                                                        </div>
                                                        <div class="col-md-4"></div>
                                                </div>
  
                        </div><!-- container -->
                    </div> <!-- Page content Wrapper -->
                </div> <!-- content -->
                <?php $this->view('users/footer')?>
            </div>
            <!-- End Right content here -->
        </div>
        <!-- END wrapper -->
        <!-- jQuery  -->
        <?php $this->view('users/scripts')?>
        <!-- Datatables-->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?=base_url()?>assets/pages/datatables.init.js"></script>
        <!--Sweet Alert-->
        <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="<?=base_url()?>assets/pages/sweet-alert.init.js"></script>
        <!--Custom script-->
        <script src="<?=base_url()?>assets/pages/ewallet.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
            <script type="text/javascript">
        $(window).on('load',function(){
                 $('#investmentModal').modal('show');
        });
    </script>
     
                  <script type="text/javascript">
                    
                        $(function(){

                                $('[name=investmentForm]').submit(function(e){
                                $('#btn_purchase').attr('disabled',true);
                                $('#btn_purchase').html('<span class="fa fa-spin fa-spinner"> </span> Processing .. ');
                                  $.ajax({
                                  type: "POST",
                                  url: 'purchase',
                                  data:   {  
                                               'imm_token'     : $('#token').val(),
                                              'amount'          :   $('[name=amount]').val(),
                                              'password'        :   $('[name=password]').val(),
                                             
                                         },
                                  cache: false,
                                  success: function(data){
                                            console.log(data);
                                        var obj = JSON.parse(data);
                                         
                                                      swal(obj[0].title, obj[0].msg, obj[0].status);
                                                      $('#btn_purchase').attr('disabled',false);
                                                      $('#btn_purchase').html('<span>Submit</span')
                                                      $('[name=amount]').val('');
                                                      $('[name=password]').val('');

                                                     
                                  }

                                });
                        e.preventDefault();
                  });

                        });

                  </script>


<?php $this->view('users/body_footer')?>