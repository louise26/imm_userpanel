<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <title>iMM-Traders | PAGE NOT FOUND</title>

        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <link href="https://immtradersclub.com/member/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="https://immtradersclub.com/member/assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="https://immtradersclub.com/member//assets/css/style.css" rel="stylesheet" type="text/css">

    </head>


    <body>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">
            <div class="ex-page-content text-center">
                <h1 class="text-white">404!</h1>
                <h2 class="text-white">Sorry, page not found</h2><br>
                 <a class="btn btn-info waves-effect waves-light" href="https://immtradersclub.com">Back to Homepage</a>
                <a class="btn btn-info waves-effect waves-light" href="https://immtradersclub.com/member/dashboard">Back to Dashboard</a>
               
            </div>
        </div>


        <!-- jQuery  -->
        <script src="https://immtradersclub.com/member/assets/js/jquery.min.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/bootstrap.min.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/modernizr.min.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/detect.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/fastclick.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/jquery.slimscroll.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/jquery.blockUI.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/waves.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/wow.min.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/jquery.nicescroll.js"></script>
        <script src="https://immtradersclub.com/member/assets/js/jquery.scrollTo.min.js"></script>

        <script src="https://immtradersclub.com/member/assets/js/app.js"></script>

    </body>
</html>